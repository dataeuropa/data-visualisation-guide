A significant proportion of the human population is not able to distinguish between certain combination of colours (8 percent of European men cannot distinguish greens from reds well, for example).

When your visualisation relies heavily on colour to be well understood, you need to make sure the colours you use are accessible to people suffering from colour blindness.

![A line chart with 3 series, coloured in red, green and brown. The series are identified through a colour legend above the chart](Pitfalls%20in%20dataviz%20colours%2043f748a84c564ae5a1b4d6678b8171d7/red-green-colourblindness2x.png)

![The same line chart as above, but viewed through the eyes of someone suffering from deuteranomaly. All colours have changed to brown, making it impossible to identify each line through the colour legend](Pitfalls%20in%20dataviz%20colours%2043f748a84c564ae5a1b4d6678b8171d7/deuteranopia.png)

A chart relying on colour to identify the lines (above) and the same chart as seen by people suffering from deuteranomaly (unable to perceive green, below). Source: Maarten Lambrechts, CC BY SA 4.0

The solution is to always test your visualisation to be colourblind proof, and to avoid some colour combinations that do not work for people suffering from certain forms of colour blindness.

Colour combinations to avoid are

- reds, greens and browns (see the charts above). This implies that using the traffic-light colour palette to signal “good” or “safe” data points with a green colour, and “bad” and “dangerous” data points with red, is not a good idea
- pink, turquoise and greys
- purples and blues

[ColorBrewer](https://colorbrewer2.org/) includes many colourblind proof palettes (make sure to check the “colorblind safe” option). A good tool to check the palette you use in your visualisation is [Viz Palette](https://projects.susielu.com/viz-palette).