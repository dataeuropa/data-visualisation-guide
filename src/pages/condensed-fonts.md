Data visualisations pack a lot of information into a limited amount of space, so they may not offer a lot of room for labels, annotations and other text elements. To accommodate for situations like this, font designers have come up with fonts with reduced widths, so more text can be fitted into the same space.

These fonts are called condensed fonts, or sometimes also “compressed” or “narrow” fonts. These fonts can be handy to load your visualisations with more text. But be aware that condensed fonts come at the cost of reduced legibility.

![ ](Typography%20and%20the%20design%20of%20text%20elements%203d739b7b83f2405290637ce3d7a4a814/condensed.png)

Examples of [condensed fonts on Google Fonts](https://fonts.google.com/?query=condensed)

An alternative to using condensed fonts is to reduce the letter spacing or “tracking” between characters. But using that technique, you will quickly run into legibility issues.

![ ](Typography%20and%20the%20design%20of%20text%20elements%203d739b7b83f2405290637ce3d7a4a814/letter-spacing2x.png)

Progressively reduced letter spacing in Roboto leads to progressively reduced legibility. Source: Maarten Lambrechts, CC BY 4.0