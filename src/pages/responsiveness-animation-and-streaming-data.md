When space is limited, and visualisations or small multiples cannot be presented next or on top of each other in one view, the visualisations can be “stacked in time” by showing them in sequence over time. A popular file format for short animations of data visualisations is the GIF file.

![3 small multiple maps showing the share of Chicago residents within 1 or 2 miles of Wal-Mart in 2005, 2010 and 2015](Responsiveness%20and%20data%20visualisation%20for%20small%20sc%20bfcc7f2b3f63483d9213104e4137aec4/walmart-npr-small-multiples.png)

On wider screens, this small series of maps are presented next to each other. Source: [npr.org](https://www.npr.org/2015/04/01/396757476/the-neighborhood-wal-mart-a-blessing-or-a-curse?t=1660058703358)

<p class='center'>
<img src='Responsiveness%20and%20data%20visualisation%20for%20small%20sc%20bfcc7f2b3f63483d9213104e4137aec4/chicago-npr-walmart.gif' alt='An animation that shows the 3 maps above in sequence' class='max-400' />
</p>

On smaller screens, the user is shown a looping gif animation. Source: [npr.org](https://www.npr.org/2015/04/01/396757476/the-neighborhood-wal-mart-a-blessing-or-a-curse?t=1660058703358) 

The reverse strategy is sometimes also used: showing an animated version of a chart on bigger screens, and a static one on smaller ones. In those cases, the static chart for smaller screens is usually a fallback, because making the animation work properly would involve much development work.

![ ](Responsiveness%20and%20data%20visualisation%20for%20small%20sc%20bfcc7f2b3f63483d9213104e4137aec4/gender-work-waterfall-wsj-full.png)

This visualisation showing the share of men and women at different levels in the workforce is animated on scroll on large screens. Source: [wsj.com](http://graphics.wsj.com/how-men-and-women-see-the-workplace-differently/)

<p class='center'>
<img src='Responsiveness%20and%20data%20visualisation%20for%20small%20sc%20bfcc7f2b3f63483d9213104e4137aec4/gender-work-waterfall-wsj-mobile.png' alt='' class='max-400' />
</p>

On smaller screens, the animated chart is replaced with a static version. Source: [wsj.com](http://graphics.wsj.com/how-men-and-women-see-the-workplace-differently/)