The simplest and easiest way to create visual hierarchy is choosing where to place elements in a design. Elements positioned at the top of a publication will be looked at before elements further down the page, and elements on the left of a line or row will be looked at before elements with the same visual weight positioned on the right (for readers with left to right languages, this is reversed for right to left languages).

<p class="center">
    <img src="Grid%20and%20arrangement%204ba513397bf6413d900e7c2944888770/Comic_reading_order.png" alt="An illustration of the reading direction, with arrows originating in the top left of the illustration and ending in the bottom right" class="max-600"/>
</p>

Illustration of the reading order, [Maplestrip](https://commons.wikimedia.org/wiki/File:Comic_reading_order.png), CC0

The hierarchy for the position of elements on a design is

top left > top right > bottom left > bottom right

The importance of the elements positioned at these locations should reflect this hierarchy/reading sequence.