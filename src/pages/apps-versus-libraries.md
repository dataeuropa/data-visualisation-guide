Data visualisation tools fall into two big categories.

On one hand there are **visualisation apps**: tools with a graphical user interface that run as a desktop app on your computer or as a web app in your browser. Apps require no coding skills, but they offer less flexibility. Most of the apps offer a fixed list of chart types, so you can’t use them to make custom and creative visualisations.

The apps mentioned here are:

- <span class='internal-link'><a href="microsoft-excel">Microsoft Excel</a></span>
- <span class='internal-link'><a href="google-sheets">Google Sheets</a></span>
- <span class='internal-link'><a href="adobe-illustrator">Adobe Illustrator</a></span>
- <span class='internal-link'><a href="datylon">Datylon</a></span>
- <span class='internal-link'><a href="business-intelligence-tools">Business intelligence tools</a></span>
- <span class='internal-link'><a href="rawgraphs">RAWGraphs</a></span>
- <span class='internal-link'><a href="datawrapper">Datawrapper</a></span>
- <span class='internal-link'><a href="flourish">Flourish</a></span>
- <span class='internal-link'><a href="qgis">QGIS</a></span>

On the other hand there are **visualisation libraries**. These are packages that integrate with a programming language and extend it with functions for producing visualisations. Some libraries take the chart template approach of the visualisation apps, but other libraries do not offer chart templates and require you to code all elements of a visualisation yourself.

The libraries mentioned here are:

- <span class='internal-link'><a href="highcharts">Highcharts</a></span>
- <span class='internal-link'><a href="ggplot2">ggplot2</a></span>
- <span class='internal-link'><a href="python-visualisation-packages">Python visualisation packages</a></span>
- <span class='internal-link'><a href="plotly">Plotly</a></span>
- <span class='internal-link'><a href="d3js">D3.js</a></span>
- <span class='internal-link'><a href="vega">Vega</a></span>
- <span class='internal-link'><a href="observable-plot">Observable Plot</a></span>
- <span class='internal-link'><a href="leaflet">Leaflet</a></span>