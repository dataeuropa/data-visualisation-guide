Data that is not directly related to people, can still be a powerfull way to connect to the audience. Whenever data has a link to the day to day lives of people, your story has an entry point into peoples interests: they will wonder how the data presented can have an impact on their daily lives.

One of the best examples of this were the charts news media published in the first weeks of the covid-19 pandemic. People realised that this new disease could have a very big impact on their lives, so the charts showing the rising number of cases drew in enourmous amounts of views on news sites and on social media.

![ ](Can%20a%20chart%20tell%20a%20story%20942e60e2613a4b43af485e6fa3dbb23c/jbm-covid-chart.jpeg)

A chart showing the rise in covid-19 cases in different countries, published by the Financial Times on 11 March 2020. Source: [@jburnmurdoch](https://twitter.com/jburnmurdoch/status/1237737352879112194) 

Apart from health data, other data that touches upon the daily lives of people also has an automatic engagement effect. Data and data visualisations about topics like personal finance (taxes, rising prices of goods and services, ...), environmental issues (air quality, pollution, ...), and transport have a direct link with the daily live and people will engage with them because of that.