An excellent way of learning about accessible HTML is by using a screen reader. So in this module, you will find instructions for using a screen reader, and links to simple HTML pages to be visited using a screen reader. For every HTML page, the underlying HTML code is also available.

Screen readers are applications that read text content out loud so that it becomes accessible to blind or visually impaired people. Most screen readers are separate applications that plug into the operating system and can read not only web pages but also the text in other applications.

The most used screen readers are [JAWS](https://www.freedomscientific.com/products/software/jaws/) (Windows only, paying application), [NVDA](https://www.nvaccess.org/) (Windows, free) and [VoiceOver](https://www.apple.com/accessibility/vision/) (built into macOS and iOS). For reasons of uniformity and simplicity however, we propose to use the [ChromeVox Screen Reader Chrome extension](https://chrome.google.com/webstore/detail/screen-reader/kgejglhpjiefppelpmljglcjbhoiplfn?hl=nl). You are of course free to use any other screen reader, but this module focusses on installing, configuring and using the ChromeVox extension.

## Getting started with ChromeVox

To get started, you should have the Chrome browser installed. So if you don’t have Chrome installed yet, you should install it first. Chrome can be downloaded from [google.com/chrome](https://www.google.com/chrome/).

To install ChromeVox, you can follow the instructions in the following video. Written instructions are provided below the video.

<iframe width="100%" height="500px" src="https://www.youtube.com/embed/_DdDQuvEtHk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

To install the extension, open [the page of the Screen Reader extension on the Chrome Web Store](https://chrome.google.com/webstore/detail/screen-reader/kgejglhpjiefppelpmljglcjbhoiplfn) in Chrome and click the “Add to Chrome” button (the page might be in a different language than English, depending on your computer and browser settings).

You enable the ChromeVox extension by clicking the extension button next to the address bar in Chrome and then click “Manage extensions”. This will navigate you to the Extensions page at chrome://extensions, where you can activate the ChromeVox extension (which is confusingly called “Screen Reader” here) by clicking the toggle. If the sound of your computer is on, you will hear the message “ChromeVox spoken feedback is ready”.

You can configure ChromeVox by clicking the Details button of the ChromeVox extension and then click the Options link. At the top of the page that opens, you can set some general options and change the voice used to read out text.

But the most important option in ChromeVox is the **ChromeVox modifier key**: this is the key, or most likely a combination of keys, that you need to press to run ChromeVox commands. If no ChromeVox modifier key is set, you can set it to the default by clicking “Reset current keymap”.

## Using ChromeVox

The video below explains how to activate and use ChromeVox.

<iframe width="100%" height="500px" src="https://www.youtube.com/embed/CRspGG8MgAM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

The ChromeVox commands used in the video are listed below.

ChromeVox modifier key (CV) + R: read the content of the page from the current position

CV + arrow down: navigate to next element

CV + arrow up: navigate to previous element

Tab: navigate to next interactive element (link or input). Note: this is not a ChromeVox command, and works also when ChromeVox is not activated.

Shift + Tab: navigate to previous interactive element

CV + Space: click a link, activate dropdown

CV + N > H: navigate to **n**ext **h**eader

CV + P > H: navigate to **p**revious **h**eader

CV + L > H: enter the **l**ist of **h**eadings on the page