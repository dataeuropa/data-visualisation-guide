[ft.com/vocabulary](https://ft.com/vocabulary)

The Visual Vocabulary is a set of chart types organised by what aspects of your data the chart type can show best.

![The full poster of the Visual Vocabulary](Data%20visualisation%20galleries%2054f97b3d69b04dbe86cbf50ba86ab8c5/visual-vocabulary.png)

Source: [ft.com/vocabulary](https://github.com/Financial-Times/chart-doctor/tree/main/visual-vocabulary)

The Visual Vocabulary was designed by the graphics desk of the Financial Times, and exists both as a poster and as [a web page](http://ft-interactive.github.io/visual-vocabulary/). The main goal of developing the Vocabulary was to have a common language to discuss the best chart type to use for charts to be added to Financial Times articles.