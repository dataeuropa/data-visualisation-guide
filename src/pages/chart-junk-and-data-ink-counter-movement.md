Lately, the pushback against Tufte’s data visualisation design philosophy and his terminology of <span class="internal-link">[chart junk](chart-junk-and-data-ink-origins)</span> has become more audible. In 2021, a team of visualisation researchers published the [Manifesto for Putting ‘Chartjunk’ in the Trash](https://altvis.github.io/papers/akbaba.pdf). According to the manifesto, the term chart junk was originally coined as a provocation on some design practices. But the term entered the data visualisation lexicon, and has been used in multiple books and scientific papers.

A critique on the use of the term is that Tufte uses a very broad definition of what is chart junk, including “harmless junk”. But by using the word “junk”, any kind of additional non-data chart element immediately is associated with a negative connotation.This makes talking about these chart elements in a neutral and scientifical way difficult. So the authors of the Manifesto propose to stop using the term “chart junk”, although they did not suggest another term or definition to replace it. 

Also recently, there seem to be some renewed interest in making more playful chart designs, integrating hand drawn and illustrative elements. One exponent of this style of data visualisation is data journalist [**Mona Chalabi**](https://monachalabi.com/). Her popular Instagram page contains many visualisations that clearly violate Tufte’s rules, but that do get a message across and are appreciated by a huge audience.

![A drawn line chart, with a seat belt representing the seat belt use rate over time, and a bar chart representing the fatalities per 100 million vehicular miles travelled](Chart%20junk%20and%20data%20ink%2045cd2a8ea3454bffa82b78d53ca414dc/mona-chalabi-cars.jpg)

Source: [@MonaChalabi](https://twitter.com/MonaChalabi/status/1462845944857350151)

![Drawings of teeth in mouths, with the number of white teeth proportional to the number of dentist consultations per year in different countries](Chart%20junk%20and%20data%20ink%2045cd2a8ea3454bffa82b78d53ca414dc/mona-chalabi-dentist.jpg)

Source: [@MonaChalabi](https://twitter.com/MonaChalabi/status/1452642962769383425)

Data illustrator Gabrielle Merite is adding to the same genre, with collage-like data visualisations.

![3D renderings of different sources and sinks of carbon (like forestry, electricity and buildings), with the height of the drawings proportional to the amount of CO2 emitted or sequestrated in Costa Rica in 2018 and 2050](Chart%20junk%20and%20data%20ink%2045cd2a8ea3454bffa82b78d53ca414dc/gabrielle-merite-costa-rica-carbon.jpg)

Source: [@Data_Soul](https://twitter.com/Data_Soul/status/1498688948314406916)

![A bar chart titled 'Hold me longer' in which each bar is drawn being hugged and its height is proportional to the pleasure rating tied to the duration of the hug](Chart%20junk%20and%20data%20ink%2045cd2a8ea3454bffa82b78d53ca414dc/hugs-gabrielle-merite.jpg)

Source: [@Data_Soul](https://twitter.com/Data_Soul/status/1465365173040914435)

![A collage like visualisation of the top 10 countries with the largest cumulative carbon emissions](Chart%20junk%20and%20data%20ink%2045cd2a8ea3454bffa82b78d53ca414dc/gabrielle-merite-emissions.jpg)

Source: [@Data_Soul](https://twitter.com/Data_Soul/status/1457770156189315075)

In a talk at the Outlier 2022 Conference, Robert Kosara (the researcher who coined the term “harmless junk”), highlighted this chart:

![A bar chart titled 'If cows were a country, they would be among the top greenhouse-gas emitters. The bar representing Cattle & Dairy is replaced by a cow](Chart%20junk%20and%20data%20ink%2045cd2a8ea3454bffa82b78d53ca414dc/mckinsey-cow.jpg)

Source: [@McKinsey](https://twitter.com/mckinsey/status/1457365985971908616)

It compares the greenhouse emissions of cattle worldwide with the ones of countries, and it represents emissions of cattle and dairy farming with... a cow.

Tuftean purists might say that because the cow is so wide, people might overestimate the value it represents, and that because it has no straight top, it is impossible to read the exact value from the cow’s shape. On top of that, the bars representing the countries are 3D bars, which have problems of their own.

But would the message be so powerful if the data was represented like below?

![The same bar chart as above, but without the cow](Chart%20junk%20and%20data%20ink%2045cd2a8ea3454bffa82b78d53ca414dc/cows-bars.png)

Source: [Robert Kosara—Outlier 2022—This Should Have Been a Bar Chart](https://www.youtube.com/watch?v=GeRgED7LWr8)

As Robert concludes his talk:

> Having some fun with a chart goes a long way in getting peoples attention and standing a chance of being remembered. We seem to think that when something is fun, it can’t also be serious, and we feel the need to strip every chart down to its bare foundations, and in that way, naturally get rid of all the fun bits as well. [...] We should all strive to present ordinary data in extraordinary ways.
> 