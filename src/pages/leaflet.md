Free, open source

[leafletjs.com](https://leafletjs.com/)

Leaflet is a Javascript library for interactive maps. It can be used to publish points, lines and polygons as well as image overlays on maps.

<iframe src='https://leafletjs.com/examples/choropleth/example.html' width='100%' height='450px' style='border: none;'></iframe>

An interactive choropleth map made with Leaflet. Source: [leafletjs.com/examples/choropleth](https://leafletjs.com/examples/choropleth/)