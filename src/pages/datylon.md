Paying, plug in for Adobe Illustrator

[datylon.com](https://www.datylon.com/)

If you want to make charts in Adobe Illustrator, the Datylon plugin is a good option. It offers a large collection of chart templates, with a lot of styling options, and integrates with the Datylon cloud platform for managing data, visualisations and custom templates.

![ ](Data%20visualisation%20design%20in%20practice%202%20tools%20208f06b06b0f4b21ad8ecf3047f02ce0/datylon-editor.png)

The Datylon chart editor interface. Source: Maarten Lambrechts, CC BY SA 4.0