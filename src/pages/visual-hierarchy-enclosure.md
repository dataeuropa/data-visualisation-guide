Looking at the visualisation below, we can also note that some elements in the chart are giving more weight by being enclosed.

![A line chart with Hungarian text, showing trends for the 1995 - 2021 period](Visual%20hierarchy%2032d60a2016ea4334ae0d7e2395559439/atlatszo-color.png)

Source: [atlatszo.hu](https://atlatszo.hu/kozugy/2022/01/27/magyarorszag-az-eu-masodik-legkorruptabb-orszaga-egyre-lejjebb-csuszunk-a-transparency-international-rangsoran/)

Firstly, the name of the medium (”Atlatszo”) in the bottom right corner is enclosed in thick red border, giving it even more visual weight. And second, the left time interval on the chart is implicitly enclosed by the rectangle containing it having a darker background. From the visual hierarchy, it seems that the left time interval is more important than the right one.

So enclosure is another mechanism to move elements up in the visual hierarchy of a visualisation: enclosed elements seem more important than other elements lacking the enclosure.

Therefore, borders and boxes should be applied consciously, and not be put onto elements that shouldn’t be highlighted.

<aside>
🔎 Can you identify and name the visual channels (<span class="internal-link"><a href="visual-hierarchy-sizing">size</a></span>, <span class="internal-link"><a href="visual-hierarchy-colour">colour</a></span>, <span class="internal-link"><a href="visual-hierarchy-z-positioning">layering</a></span>, enclosure) used to create visual hierarchy in the chart below? What are the most important elements in the chart? Would you change anything in the design of the charts to optimise their visual hierarchy?
</aside>

![A series of small multiple slope charts titled 'Covid-19 deaths before and after universal adult vaccine eligibility'](Visual%20hierarchy%2032d60a2016ea4334ae0d7e2395559439/hierarchy-exercise.png)

Source: [Why Covid Death Rates Are Rising for Some Groups](https://www.nytimes.com/interactive/2021/12/28/us/covid-deaths.html), nytimes.com