<script>
    import Reveal from '$lib/components/Reveal.svelte'
</script>

Consider the chart below.

![A line chart with dots representing measurements and fitted line between them. The title of the y axis is Heart rate (bmp) and the title of the x axis is Time axis (about 40 minutes)](Text%20annotations%204d77570c409249378ca558ae45eb0d67/no-text4x.png)

Source: [reddit.com](https://www.reddit.com/r/dataisbeautiful/comments/2o1rfe/heart_rate_bpm_during_marriage_proposal_oc/)

From the annotations in the bottom, you can read that the chart shows the heart rate of a man, in beats per minute over a 40 minutes time span. The chart is clearly showing peaks and valleys, but without more information, it is not possible to know what caused these changes in the man’s heart rate.

The importance of chart titles is explained on the <span class='internal-link'>[importance of visualisation titles](the-importance-of-visualisation-titles)</span> page. Adding the original title to the chart confirms that this can help enourmously in understanding and interpreting a visualisation.

![The same chart as above, but with the title 'Heart rate (bpm) during marriage proposal'](Text%20annotations%204d77570c409249378ca558ae45eb0d67/title-only4x.png)

Source: [reddit.com](https://www.reddit.com/r/dataisbeautiful/comments/2o1rfe/heart_rate_bpm_during_marriage_proposal_oc/)

Now you might suspect at what moment this man proposed to his partner. But a lot more can happen over a 40 minute time period. Let’s add the annotations that were part of the original visualisation:

![The same chart as above, but with annotations explaining what happened before, during and after the proposal](Text%20annotations%204d77570c409249378ca558ae45eb0d67/all-text4x.png)

Source: [reddit.com](https://www.reddit.com/r/dataisbeautiful/comments/2o1rfe/heart_rate_bpm_during_marriage_proposal_oc/)

Suddenly, the chart turns into a story: the fiancees are walking in the Colosseum (setting the geographic context of this little story to Rome, Italy), buying ice creams (so it’s probably spring or summer, feels like the sun is shining), and then walking towards the Forum Romanum, where he proposes. You can almost feel the relief after she said yes and the hapiness of the couple sitting on that nice bench they found.

This example shows the power of annotations: they provide an additional layer of information that can contextualise and explain elements in the chart that would be difficult for the reader to see or understand without the annotations. 

Annotations also work great in photography, as the following example about a much less happy topic shows.

![An annotated satellite image of the Ukraine city of Mariupol](Text%20annotations%204d77570c409249378ca558ae45eb0d67/annotated-picture.jpg)

Source: [Lauren Tierney](https://twitter.com/tierneyl/status/1503729507668312065), washingtonpost.com

Of course it is much easier to create a narrative with annotations with a topic like a marriage proposal: empathy and engagement are almost a guarantee because the data is so personal, recognisable and loaded with emotions.

But media have demonstrated that annotations are a powerful mechanism to communicate and explain messages in data.

Here is another illustration: look at the chart below, and then click on the toggle below it to see the chart with annotations added.

![A chart titled 'Rouble tumbles to record low', showing the devaluation of the Russian rouble over the 2000 - 2022 period](Text%20annotations%204d77570c409249378ca558ae45eb0d67/ruble-no-annotations2x.png)

<Reveal label="Click here to reveal the chart with annotations" content="<p>
<img src='Text%20annotations%204d77570c409249378ca558ae45eb0d67/ft-rouble-annotations.png' alt='The same chart as above but with annotations showing the timing of events in the recent history of Russia' /></p>"></Reveal>