Operating systems, websites and web browsers and their plugins are increasingly supporting dark mode styling. In dark mode, content is shown with light text on a dark background, instead of the standard dark text on a white or light background. Dark mode is more suited for use in low-light settings, and it reduces blue light exposure and fatigue from long screen times.

Data visualisation tool Datawrapper [started to offer dark mode](https://blog.datawrapper.de/dark-mode-for-embedded-visualizations/) for its embedded visualisations to their users. When the “automatic dark mode” option is enabled on a Datawrapper visualisation, the styling of the embedded visualisation will switch to dark mode whenever users have selected to prefer dark colour schemes in their browser or operating system. Note that for this to work properly, the website the visualisation is embedded in should also support dark mode.

![An area chart titled 'More than Earth can handle', with a white background and dark text](Colours,%20colour%20blindness%20and%20data%20visualisation%201bdcaf7fa57b4d92a9804910f3066592/light-mode.png)

![The same area chart, but with a dark background, white text and modified colours](Colours,%20colour%20blindness%20and%20data%20visualisation%201bdcaf7fa57b4d92a9804910f3066592/dark-mode.png)

The same chart in standard and in dark mode. Source: Datawrapper

Notice that the red and green colours (which are not optimal for colour weak and colour blind users, by the way) are not the same colours in both modes. Datawrapper changes the colours of all elements on the chart in dark mode to ensure a high enough contrast ratio with the dark background.