The **intrinsic cognitive load** is the inherent level of complexity and difficulty of the new information to be processed. In mathematics, for example, it is easier to calculate a simple sum , like 3 + 5, than it is to calculate a ratio like 3/5. And both of these are easier than solving differential equations. 

The intrinsic load of the data presented in a visualisation can be high or low depending on some of its characteristics:

- a low number of records has a lower intrinsic load than a high number of records. So a bar chart comparing just 2 values is easier to process than a bar chart with 20 values.
- a low number of dimensions has a lower intrinsic load than data with a lot of dimensions. A bar chart only shows 2 dimensions (one categorical and one numerical), so it is easier to process than a bubble chart, which can show 5 dimensions. In the bubble chart below these dimensions are 2 categorical dimensions (the country names (as tooltips) and the region each country belongs to) and 3 numerical dimensions (encoded in the x and y position of the bubbles and their size).

![A bubble chart showing 5 dimensions: income on the x axis, life expectancy on the y axis, region as colour, population as bubble size, and country name in tooltips](Choosing%20the%20right%20chart%20type%20for%20your%20story%20534c70625e194b62ad932d52825d1579/gapminder.png)

A bubble chart showing 5 dimensions. When the chart is animated over time, the visualisation even shows 6 dimensions. Source: [gapminder.org/tools](https://www.gapminder.org/tools/#$chart-type=bubbles&url=v1)

- some aspects of data can elevate its intrinsic load. For example, uncertainty in data makes it much harder to make sense of it. As a result of this higher intrinsic load, visualisations showing uncertainty are often poorly interpreted.
- Abstract data has a higher cognitive load than data that people can relate to directly. A yearly budget of 4.7 billion euros is harder to process than a monthly electricity bill of 80 euros, and 150 million kilometers (the distance between the sun and the earth) is much harder to grasp than a distance of 20 kilometers (which may be similar to the length of your daily commute).

Intrinsic load is not only associated to the data itself. The pattern in the data that you want to highlight for your message also has an intrinsic load. Simple differences between values have a lower intrinsic load than correlations between dimensions, for example.