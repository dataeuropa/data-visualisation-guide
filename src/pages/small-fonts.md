Another technique for packing more text into a small space is, of course, to reduce the font size. The readability of fonts at small font sizes is determined by a few characteristics of the font.

The **x-height** is the distance between the baseline and the mean line of the font. This is the height of the letter x (but also of the letters v,w and z).

![The word Sphinx is used to show the baseline and the x-height of serif font](Typography%20and%20the%20design%20of%20text%20elements%203d739b7b83f2405290637ce3d7a4a814/x-height2x.png)

Illustration of the x-heigh. Source: [Max Naylor](https://commons.wikimedia.org/wiki/File:Typography_Line_Terms.svg), public domain

Fonts with large x-heights have better legibility when used in small font sizes.

<p class='center'>
<img src='Typography%20and%20the%20design%20of%20text%20elements%203d739b7b83f2405290637ce3d7a4a814/merriweather-large-x-height.png' alt='A snippet of text in the Merriweather font' class='max-600' />
</p>

Merriweather has a large x height. Source: [Google Fonts](https://fonts.google.com/?query=merriweather&category=Serif)

<p class='center'>
<img src='Typography%20and%20the%20design%20of%20text%20elements%203d739b7b83f2405290637ce3d7a4a814/merriweather-large-x-height.png' alt='A snippet of text in the Merriweather font displayed very small. The text is still readable' class='max-100' />
</p>

<p class='center'>
<img src='Typography%20and%20the%20design%20of%20text%20elements%203d739b7b83f2405290637ce3d7a4a814/bellefair-small-x-height.png' alt='A snippet of text in the Bellefair font' class='max-600' />
</p>

Bellefair has small x height. Source: [Google Fonts](https://fonts.google.com/?query=bellefair&category=Serif)

<p class='center'>
<img src='Typography%20and%20the%20design%20of%20text%20elements%203d739b7b83f2405290637ce3d7a4a814/bellefair-small-x-height.png' alt='A snippet of text in the Bellefair font, displayed very small. The text is less readable' class='max-100' />
</p>

Source: [Google Fonts](https://fonts.google.com/?query=bellefair&category=Serif)

The **counters** of a font are the areas that are entirely (”closed counter”) or partially enclosed (”open counter”) by the letter forms. The letters “o” and “p”, for example, have closed counters, the letter “a” has both an open and a closed counter and the letter “c” only has an open counter.

The opening between the inside of an open counter and its outside is called an **apperture**. The size of appertures varies between fonts, with some fonts having wide openings (”open appertures”), while others have narrow openings (”closed appertures”). Fonts with open appertures are preferred for displaying text with small font sizes. Example fonts with open appertures include Lucida Grande, Trebuchet MS and Corbel.

Below are some fonts with closed appertures, which become problematic for small font sizes: distinguishing between the numbers 8 and 9 becomes very difficult.

<p class='center'>
<img src='Typography%20and%20the%20design%20of%20text%20elements%203d739b7b83f2405290637ce3d7a4a814/anton.png' alt='The number 89 in the Anton font' class='max-50' />
</p>

<p class='center'>
<img src='Typography%20and%20the%20design%20of%20text%20elements%203d739b7b83f2405290637ce3d7a4a814/bebas-neue.png' alt='The number 89 in the Bebas Neue font' class='max-50' />
</p>

<p class='center'>
<img src='Typography%20and%20the%20design%20of%20text%20elements%203d739b7b83f2405290637ce3d7a4a814/archivo-black.png' alt='The number 89 in the Archivo Black font' class='max-50' />
</p>

Source: [Google Fonts](https://fonts.google.com/)

Some fonts are designed to be used in titles and in large displays, like billboards or big screens. These fonts may have characters with very fine lines. These hairlines have good visibility when the font is used in large font sizes, but can be barely visible on small font sizes. These fonts (called “display fonts”) can be best avoided for small font sizes.

<p class='center'>
<img src='Typography%20and%20the%20design%20of%20text%20elements%203d739b7b83f2405290637ce3d7a4a814/libre-caslon-display.png' alt='The number 1234567890 in the Libre Caslon Display' class='max-600' />
</p>

<p class='center'>
<img src='Typography%20and%20the%20design%20of%20text%20elements%203d739b7b83f2405290637ce3d7a4a814/libre-caslon-display.png' alt='The number 1234567890 in the Libre Caslon Display, displayed very small. The hairlines are not very visible' class='max-100' />
</p>

Source: [Google Fonts](https://fonts.google.com/)

Similarly, the serifs of serif fonts can be hard to distinguish or even hinder the identification of characters when used in small font sizes. So sans serif fonts are more suited for these circumstances.