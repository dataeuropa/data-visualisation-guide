Understanding the results from a sample drawn from a population distribution is not very intuitive. Hypothetical outcome plots (HOPs) try to make this easier to grasp.

Instead of showing some summary statistics, like the sample mean and a confidence interval, HOPs simulate multiple samples from the same distribution, and show them in an animated way.

![An animated gif showing an hypothetical outcome plot for 2 measures A and B, expressed in parts per million](Visualising%20uncertainty%208aa5c9e663864767aaa41986a5a6f96c/hops.gif)

Source: [Hypothetical Outcome Plots: Experiencing the Uncertain](https://medium.com/hci-design-at-uw/hypothetical-outcomes-plots-experiencing-the-uncertain-b9ea60d7c740)

The animation of a HOP can give viewers an intuitive sense of the uncertainty surrounding the true trends.

The New York Times used some HOPs to simulate how the US Labor Department’s monthly jobs report numbers would look like when resampled with the same sampling error the jobs report has. The results are quite striking.

![An animation showing that the numbers in the US jobs report have a high degree of uncertainty and can vary a lot](Visualising%20uncertainty%208aa5c9e663864767aaa41986a5a6f96c/nyt-jobs-hops.gif)

Source: [How Not to Be Misled by the Jobs Report](https://www.nytimes.com/2014/05/02/upshot/how-not-to-be-misled-by-the-jobs-report.html), nytimes.com

When animated HOPs leave a trace, they can be used to build up a static image that shows the cumulative results of each frame of the HOP. An example of this technique is the animation in the [Extensive Data Shows Punishing Reach of Racism for Black Boys](https://www.nytimes.com/interactive/2018/03/19/upshot/race-class-white-and-black-men.html) article by the New York Times. It shows how black boys who grew up rich have a higher chance to end up poor than their white peers.

<video src='Visualising%20uncertainty%208aa5c9e663864767aaa41986a5a6f96c/nyt-animation-hops.mp4' width='100%' height='500px' controls/>

_Source: [Extensive Data Shows Punishing Reach of Racism for Black Boys](https://www.nytimes.com/interactive/2018/03/19/upshot/race-class-white-and-black-men.html), nytimes.com_