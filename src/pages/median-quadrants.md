Adding lines for the medians in a scatterplot can serve as a powerful storytelling mechanism: the medians for the x and y values create 4 quadrants that can be used to classify the data marks into 4 categories for x and y values (”low-low”, “low-high”, “high-low” and “high-high”).

Using the medians for x and y assures that each quadrant contains the same amount of data points: half of the points are above and below the horizontal median line for y, and half of the points are to the left and right of the vertical median line for y.

<iframe src='https://graphics.axios.com/2019-04-06-occupation-demographics/index.html' width='100%' height='700px' style='border: none;'></iframe>

_The interactive chart above uses the "medians-quadrant" visual annotation technique. Source: [The millennials who are making it](https://www.axios.com/the-oldest-and-youngest-jobs-in-the-us-millennials-d9738704-4c84-4208-8f15-8d997db170ac.html), axios.com_

![A scatter plot of footballers with their average successfull dribbles on the x axis and the proportion of successfull dribbles on the y axis. The chart is divided into quadrants using the medians for x and y](A%20deep%20dive%20into%20scatter%20plots%20447afd31ef0d4b0a887b000d2b360f95/median-scatterplot-driblab.jpg)

Source: [@driblab](https://twitter.com/driblab/status/1224387280271544322) 