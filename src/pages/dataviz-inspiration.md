[dataviz-inspiration.com](https://www.dataviz-inspiration.com/)

Dataviz Inspiration is a collection of data visualisations curated by Yan Holtz, who is also the author of [data-to-viz.com](https://www.data-to-viz.com/). With Dataviz Inspirations, Holtz aims to showcase hundreds of the most beautiful and impactful dataviz projects.

![A browser window showing the dataviz-inspiration.com home page](Data%20visualisation%20galleries%2054f97b3d69b04dbe86cbf50ba86ab8c5/dataviz-inspiration.png)

Source: [dataviz-inspiration.com](https://www.dataviz-inspiration.com)

The visualisations can be filtered based on chart type and based on the tool that was used to make the visualisation.

![A screenshot of an entry in the Dataviz Inspirations database, showing a map titled 'Russia, Gas, and the Ukraine Conflict', with a description of the context and the technique of the map](Data%20visualisation%20galleries%2054f97b3d69b04dbe86cbf50ba86ab8c5/gas-sankey-map-nytimes.png)

Source: [dataviz-inspiration.com](https://www.dataviz-inspiration.com)
