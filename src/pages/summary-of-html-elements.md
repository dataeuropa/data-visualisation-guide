Below is a summary table with all the HTML elements discussed on the <span class='internal-link'>[HTML 101](tag/html-101)</span> pages.

| Element | <div class="left">Application</div> |
| --- | --- |
| <code>html</code> | <div class="left">The containing parent element of all elements on a web page</div> |
| <code>head</code>| <div class="left">Contains meta information about the page. Its content is not displayed</div> |
| <code>title</code> | <div class="left">Is a child element of the head element. It contains text that is displayed in browser tabs, and it is read out by screen readers</div> |
| <code>body</code> | <div class="left">Contains the content of a web page</div> |
| <code>h1</code> - <code>h6</code> | <div class="left">Headings give the content of a page structure and hierarchy. They are used by screen readers to navigate the content of a web page</div> |
| <code>p</code> | <div class="left">Contains a paragraph of text</div> |
| <code>img</code> | <div class="left">Embeds an image into a web page</div> |
| <code>a</code> | <div class="left">Creates a link to an element on the page, or to another page</div> |
| <code>button</code> | <div class="left">When activated, initiates an action, like submitting inputted data or opening a dialog</div> |
| <code>input</code> | <div class="left">Collects data from users. Can have different types, like a radio button, a checkbox, a text field, …</div> |
| <code>ul</code>, <code>ol</code>, <code>li</code> | <div class="left">Unordered lists show their list items as bullet points, ordered lists show them numbered</div> |
| <code>div</code> | <div class="left">A generic element to contain other elements, used for creating layout and for styling</div> |
| <code>table</code>, <code>thead</code>, <code>tbody</code> | <div class="left">Creates a table, with a table header row and a table body</div> |
| <code>tr</code> | <div class="left">A row in a table</div> |
| <code>td</code> | <div class="left">A cell in a table row</div> |
| <code>audio</code>, <code>video</code> | <div class="left">Embeds audio and video files into a web page. Modern browsers automatically create media players to play the media</div> |
| <code>svg</code> | <div class="left">Scalable vector graphics are the vector based image format native to HTML</div> |
| <code>rect</code>, <code>circle</code>, <code>line</code>, <code>text</code> , … | <div class="left">Children of an svg element, to dray shapes and text on the svg canvas</div> |