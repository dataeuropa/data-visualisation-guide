Free, web app

[flourish.studio](https://flourish.studio/)

Flourish is a web for creating charts and other interactive content. Its template gallery not only covers charts and different kinds of maps, but also things like photo sliders and quizzes. Charts can have tooltips, but also filters and play/pause buttons for animations, and you can also make slide shows with animated visualisations.

An example of an embedded animated slide show made with Flourish:

<iframe src='https://flo.uri.sh/story/321221/embed?auto=1' width='100%' height='650px' style='border: none;'></iframe>

Visualisations can be exported as PNG, JPG and SVG and interactive visualisations can be shared with a url or embedded into other web pages (as demonstrated above). Paying accounts can download the html of interactive visualisations to be self hosted.