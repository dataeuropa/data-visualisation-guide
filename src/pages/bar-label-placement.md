Data labels are usually positioned at the end of the bars, because it is the end of the bar where the values should be read from. These labels can be placed inside or outside the bars. When they are placed inside, they can be in white and have some transparency to make them blend in into their bars.

<p class='center'>
<img src='A%20deep%20dive%20into%20bar%20charts%20047791ead2e848bdb3d0afcd1bf2bd4a/data-labels-outside2x.png' alt='A horizontal bar chart with data labels outside of the bars' class='max-400' />
</p>

<p class='center'>
<img src='A%20deep%20dive%20into%20bar%20charts%20047791ead2e848bdb3d0afcd1bf2bd4a/data-labels-inside2x.png' alt='A horizontal bar chart with data labels inside of the bars' class='max-400' />
</p>

Data labels outside and inside bars. Source: Maarten Lambrechts, CC BY SA 4.0

When the labels are placed inside the bars, a solution needs to be provided for small values that lead to labels that do not fit inside the bars.

<p class='center'>
<img src='A%20deep%20dive%20into%20bar%20charts%20047791ead2e848bdb3d0afcd1bf2bd4a/overflowing-data-label_12x.png' alt='A horizontal bar chart with labels inside of the bars, with one bar being too narrow to contain its label' class='max-400' />
</p>

Source: Maarten Lambrechts, CC BY SA 4.0

A common solution is to position the data labels inside the bars, and outside of the bars in the case they do not fit.

<p class='center'>
<img src='A%20deep%20dive%20into%20bar%20charts%20047791ead2e848bdb3d0afcd1bf2bd4a/data-label-overflow-outside2x.png' alt='A horizontal bar chart with data labels inside of the bars, but outside of them when the bars are too narrow to contain the label' class='max-400' />
</p>

Source: Maarten Lambrechts, CC BY SA 4.0