<p class='center'>
<img src='Data%20story%20genres%20and%20structures%20854bd72307ad4dbda8a777a86347f3df/comicstrip.png' alt='A schematic representation of comic, with a page divided into squares and rectangles' class='max-200' />
</p>

Source: [Narrative Visualization: Telling Stories with Data](https://cpb-us-e1.wpmucdn.com/sites.northwestern.edu/dist/3/3481/files/2015/02/Narrative_Visualization.pdf)

A comic strip is related to a <span class='internal-link'>[partitioned poster](partitioned-poster-narrative-visualisation)</span>, but the panels have a much stricter layout, so that the reading order is much more linear.

Recently, the genre of the data comics has emerged as a study area in visualisation research. Publications on the topic as well as a gallery with examples can be found at [datacomics.github.io](http://datacomics.github.io). See also the  <span class='internal-link'>[Combining with other media: data comics](combining-with-other-media-data-comics)</span> page.

![ ](Data%20story%20genres%20and%20structures%20854bd72307ad4dbda8a777a86347f3df/data-comic-time-use-matt-hong.png)

A data comic on the average time use of Americans. Source: [Matt Hong](https://medium.com/@MattIanHong/how-americans-spend-each-day-a-data-comic-51817f910dd7)