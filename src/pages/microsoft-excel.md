Paying, desktop app

[microsoft.com/en-us/microsoft-365/excel](https://www.microsoft.com/en-us/microsoft-365/excel)

Because it is so ubiquitous in the business world and outside of it, Excel is one of the most popular tools to produce data visualisations in the world. The interface for making charts is based on a gallery of chart types, but Excel users have been very creative in making many kinds of charts in Excel that are not part of the gallery offered by Excel.

<p class='center'>
<img src='Data%20visualisation%20design%20in%20practice%202%20tools%20208f06b06b0f4b21ad8ecf3047f02ce0/excel-charts.png' alt='Screenshot of the available chart types in Microsoft Excel' class='max-600' />
</p>

Available chart types in Excel. Source: Maarten Lambrechts, CC BY SA 4.0

Excel’s chart output is mainly static, but because Excel is so flexible, it can also be used to produce interactive charts and dashboards. However, the use of these is limited to Excel itself: you have to have Excel installed to open the file containing the interactive charts.