In a graphic sequence text blocks move over a fixed visual element in the background, which is replaced with a new graphic with each new text block.

![A four panel illustration of how graphic sequences work, with the text 'Scrolling... switches the graphic'](Scrollytelling%200ae4533947224ed3b08305e4c650ce0d/scrollytelling-graphic-sequence.png)

Source: Oesch, Jonas, Manuel Roth, and Adina Renner. “Scrolling into the Newsroom: A Vocabulary for Scrollytelling Techniques in Visual Online Articles.” Information Design Journal. Forthcoming 2022

Graphic sequences are often used for showing and highlighting information on a visualisation step by step. They can also work well with illustrations and photographs.

![A screenshot from A room, a bar and a classroom: how the coronavirus is spread through the air, showing a drawing of 6 masked people in a room](Patterns%20for%20data%20driven%20stories%2034fe0220a7d64297ae6ccf534303e18f/elpais-covid-spread.png)

Source: [A room, a bar and a classroom: how the coronavirus is spread through the air](https://elpais.com/especiales/coronavirus-covid-19/a-room-a-bar-and-a-class-how-the-coronavirus-is-spread-through-the-air/), elpais.com

![A screenshot from The race to save the river Ganges, showing a map of the river](scrollytelling-ganges-reuters.jpg)

Source: [The race to save the river Ganges](https://graphics.reuters.com/INDIA-RIVER/010081TW39P/index.html), reuters.com

In graphic sequences, the transition between the subsequent graphics can be direct or can be faded. The graphic can use the full viewport or can have smaller dimensions. In the latter case, it is usually centered in the middle of the screen.