The below shows how you can get access to the data of a dynamically generated visualisation on a web page using the developer tools of your browser.

<iframe width="100%" height="500" src="https://www.youtube.com/embed/0S4aWcm5Zsg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

<aside>
🔎 Can you get access to the data from the visualisation on <a href="https://www.ons.gov.uk/peoplepopulationandcommunity/leisureandtourism/timeseries/gmax/ott">this page</a>, following the instructions in the video? To do so, you can follow these steps:

- open the developer tools in your browser (`Tools > Browser tools > Web Developer Tools` in Firefox, `View > Developer > Developer Tools` in Chrome or `Settings and more > More tools > Developer tools` in Edge. You can also click right on a page and select ‘Inspect’)
- Go to the Network tab and select Fetch/XHR or XHR
- Reload the page
- See what requests were made and what data was received in response

</aside>