Typography can be optimised for people suffering from visual impairments or dyslexia.

A first basic rule is to avoid any unusual or artistic font, like handwritten fonts. They are not designed for legibility and can easily generate reading difficulties.

Fonts should also have more than one character case, so avoid uppercase only fonts, for example. Uppercase text is harder to read than normal cased text, and text decoration like italics also reduce legibility.

Some fonts (especially sans serif ones) use basically indistinguishable characters for the number 1, normal cased l and capital I. They do this for stylistical reasons, but this obviously creates a lower legibility. Using a font that has distinctively different characters for these characters is advised.

<p class='center'>
<img src='Accessibility%207a31e0024d4d4023ba9ec30ba05cf2f3/alata-font.png' alt='The characters 1, l and I in the Alata font are very similar' class='max-600' />
</p>

Source: [Google Fonts](https://fonts.google.com/)

<p class='center'>
<img src='Accessibility%207a31e0024d4d4023ba9ec30ba05cf2f3/hammersmith-font.png' alt='The characters 1, l and I in the Hammersmith font are very similar' class='max-600' />
</p>

Source: [Google Fonts](https://fonts.google.com/)

Young children, but also some adults, mirror the letter combinations d-b and p-q. Using a font that does not use mirroring characters for these combinations can help them reading these correctly.

![The letters pq and db in the Roboto and Roboto Serif fonts](Accessibility%207a31e0024d4d4023ba9ec30ba05cf2f3/mirroring-fonts.png)

Roboto uses mirrored characters, while Roboto Serif does not. Source: [Google Fonts](https://fonts.google.com/?preview.text=pq%20db&preview.text_type=custom&category=Sans+Serif&query=Roboto)

Other problematic letter combinations for people with visual impairments are o, c, e and a. These can be hard to distinguish in fonts that have small openings (called apertures) of the c character, which can appear to be closed and ressemble an o, while the e and the a can also appear to be closed and ressemble the number 8.

![The letter ocea in the Source Sans Pro font on the left and in Oswald font on the right](Accessibility%207a31e0024d4d4023ba9ec30ba05cf2f3/source-sans-oswald.png)

Source Sans Pro has wider appertures than Oswald. People with visual impairments might have difficulties with the o, c, e and a in Oswald. Source: [Google Fonts](https://fonts.google.com/)

Reducing letter spacing is a technique to fit more text into a small space, but comes at a legibility cost and should therefore be used carefully. But fonts have their own characteristic letter spacing, with some using a wider spacing then others. 