[datavizproject.com](https://datavizproject.com/)

The Dataviz Project is a data visualisation collection with more than 160 chart types.

![A web browser showing the datavizproject.com homepage](Data%20visualisation%20galleries%2054f97b3d69b04dbe86cbf50ba86ab8c5/dataviz-project-overview.png)

Source: Maarten Lambrechts, CC BY SA 4.0

The collection can be filtered by

- family: charts, diagrams, maps, …
- input: the type of data that is required to make the visualisation
- function: comparison, correlation, distribution, …
- shape: circular, grid, …

Each chart type has its own dedicated page, with a description of the chart type, and lists published examples of the chart type.

![A web browser showing the page about alluvial diagrams, showing examples and a description of the chart](Data%20visualisation%20galleries%2054f97b3d69b04dbe86cbf50ba86ab8c5/dataviz-project-detail.png)

Source: Maarten Lambrechts, CC BY SA 4.0