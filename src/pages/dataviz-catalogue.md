[datavizcatalogue.com](http://datavizcatalogue.com/)

The Dataviz Collection catalogue contains around 60 chart types, which can be searched by function.

![A browser window showing the datavizcatalogue.com homepage. The page shows a grid of chart type icons](Data%20visualisation%20galleries%2054f97b3d69b04dbe86cbf50ba86ab8c5/dataviz-catalogue.png)

Source: [datavizcatalogue.com](http://datavizcatalogue.co)

Compared to the other chart type galleries discussed here, the Dataviz Catalogue is unique because for each chart type it contains:

- a little “A guide to…” video
- a list of links pointing to tools that can be used to produce the chart type.
    
    ![A screenshot of the page about histograms on datavizcatalogue.com, showing a description, an example and a list of functions of histograms](Data%20visualisation%20galleries%2054f97b3d69b04dbe86cbf50ba86ab8c5/datavizcatalogue-histogram.png)
    
    Source: [datavizcatalogue.com](https://datavizcatalogue.com/methods/histogram.html)
    

The Dataviz Catalogue also contains [a page with resources](https://datavizcatalogue.com/resources.html), and is available in multiple languages.