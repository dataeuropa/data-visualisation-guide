Below you can find some guidelines on the use of colour in data visualisation. Where relevant, the motivation behind the guideline is also given.

**The default chart background should be white.** A white background is the easiest to work with and ensures the highest contrast between the chart elements and the background. Off white backgrounds can give a distinctive feel to visualisations, but require more saturated and bright colours to guarantee enough contrast. “Dark mode” visualisations, with a black or dark background, can work too, but require all colours to be reversed: text should be white, or another bright colour, the light grey of supporting visualisation elements like axes and grids should turn into a darker grey, and colour scales should be reversed, with dark colours representing low values and light colours representing high values.

**Make grey our best friend.** The <span class='internal-link'>[visual hierarchy](tag/visual-hierarchy)</span> of a visualisation determines where readers will look at first in a visualisation and colour is an important element in the visual hierarchy. Saturated and bright colours move elements up in the visual hierarchy and let them attract more attention. So use them sparingly, and reserve them only for the most important data points and annotations. Except for data and text, all elements in your visualisations could be shades of grey.

**Don’t use too many colours.** If everything has a colour, nothing stands out and there is no visual hierarchy. Randomly applying colour can confuse readers, as they might try to see meaning in colours which is not there. [Here are 10 strategies to reduce the number of colours to use](https://blog.datawrapper.de/10-ways-to-use-fewer-colors-in-your-data-visualizations/).

**Use colours deliberately.** Think about what colours would make sense, take into account <span class='internal-link'>[colour connotations](colour-connotations)</span> and go beyond the default colour palette of the tool you are using. Avoid cliche colour connotations and stereotypes like pink for women and blue for men.

**Use colours consistently.** If you assign meaning to a colour in your visualisation design, make sure not to change the meaning or assign other colours to the same meaning. You will confuse readers otherwise.

**Consider colour harmonies when picking colours.** Colour harmonies can set the mood and create balanced palettes. 

**Use perceptually uniform colour palettes when encoding numbers in colour.** Use continuous scales for more detail, discrete scales for a clearer picture and diverging scales for values above and below a meaningful central value.

**Remember that colour is a weak channel for decoding data.** If you want readers to get access to the data values, provide other or additional encodings, chart types or tooltips.

**Perform a colour blindness test on the colours you use.** Red - green colour combinations are popular for indicating “good” and “bad”, but a significant portion of the population will have difficulties telling them apart.

**Get it right in black and white.** If you suspect your visualisations will be printed in black and white, make sure the visualisation still makes sense without colours.

**Small elements on a chart need more saturated colours than big ones.** This applies to data marks like dots and symbols, but also to text.