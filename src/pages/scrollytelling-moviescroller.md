In a moviescroller the scrolling controls the advancement of a video or the movement of the camera in a 3D-rendered graphic.

![A four panel illustration of how moviescrollers work, with the text 'Scrolling... advances the video'](Scrollytelling%200ae4533947224ed3b08305e4c650ce0d/scrollytelling-moviescroller.png)

Source: Oesch, Jonas, Manuel Roth, and Adina Renner. “Scrolling into the Newsroom: A Vocabulary for Scrollytelling Techniques in Visual Online Articles.” Information Design Journal. Forthcoming 2022

The New York Times article “How a Massive Bomb Came Together in Beirut’s Port contains 2 moviescrollers: the first one starts with aerial imagery of the aftermath of the explosion, but then transitions into a moviescroller of 3D rendering of the scene before the explosion. The second moviescroller explains what is going on in a video of the explosion.

![Screenshot of How a Massive Bomb Came Together in Beirut's Port (Published 2020)](scrollytelling-beirut-nyt.jpg)

Source: [How a Massive Bomb Came Together in Beirut's Port (Published 2020)](https://www.nytimes.com/interactive/2020/09/09/world/middleeast/beirut-explosion.html)

Another piece by the New York Times, on gymnast Sunisa Lee, also uses moviescrollers:

![Screenshot of Sunisa Lee Is Unmatched on Uneven Bars and Wants All-Around Glory](scrollytelling-lee-nyt.jpg)

[Sunisa Lee Is Unmatched on Uneven Bars and Wants All-Around Glory](https://www.nytimes.com/interactive/2021/sports/olympics/suni-lee-gymnastics.html)