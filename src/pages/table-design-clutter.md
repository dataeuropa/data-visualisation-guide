Traditionally, table designs make extensive use of visually heavy grids (in part because many tools will apply them by default. But grids can make scanning tables harder, and when cell items are properly aligned, they are implying the grid lines with the white space between the columns.

The same applies to the outside border of the table, which can also be safely removed.

Faint grid lines can help distinguish between the header and the data rows of the table, or between the rows of data themselves. A popular alternative is to use striping: alternating the colour of subsequent rows in the table.