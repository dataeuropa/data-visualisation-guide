Consider the following chart:

![A line chart titled 'Income growth of the poorest 10% vs income growth of the richest 10%', showing trajectories for the US, the UK, France and Norway](How%20to%20introduce%20less%20common%20chart%20types%2054daaa496c1540519f56f72f5ba88962/connected-scatter-vox-roser.webp)

Source: [The US lags far behind its peers on “inclusive” economic growth](https://www.vox.com/the-big-idea/2017/2/3/14491964/growth-inequality-comparative-us-europe), vox.com

There is a lot going on in this chart: there are a lot of different visual and text annotations, both axes use a logarithmic scale and there is a lot of text in the chart description, in the axis titles and in the notes at the bottom of the chart. But the biggest issue is probably that this it not a classical line chart: the chart shows evolution over time, but it has no time axis.

This chart type is a [connected scatterplot](http://steveharoz.com/research/connected_scatterplot): a scatterplot in which dots are connected by lines in chronological order. It can be a good chart type for showing how two numerical dimensions evolve over time together, but it is not very common.

That is the reason why the authors of the chart above decided to introduce the chart step by step.

In the first step, they use one single data point (France in 2010) to explain what the axes and the diagonal lines mean:

![The same chart as above, but with only the values for France in 2010 shown as a point](How%20to%20introduce%20less%20common%20chart%20types%2054daaa496c1540519f56f72f5ba88962/connected-scatter-vox-roser-intro1.webp)

Source: [The US lags far behind its peers on “inclusive” economic growth](https://www.vox.com/the-big-idea/2017/2/3/14491964/growth-inequality-comparative-us-europe), vox.com

Then they reveal France’s trajectory over the 1978 to 2010 period, showing how the chart works, and with arrows and annotations they highlight what it means if a trajectory moves to the top left and bottom right corners of the chart.

![The same chart as above, but with the trajectory for France and some arrows and annotations](How%20to%20introduce%20less%20common%20chart%20types%2054daaa496c1540519f56f72f5ba88962/connected-scatter-vox-roser-intro2.webp)

Source: [The US lags far behind its peers on “inclusive” economic growth](https://www.vox.com/the-big-idea/2017/2/3/14491964/growth-inequality-comparative-us-europe), vox.com

After this, they add other countries to the chart, whose trajectories are discussed in the surrounding text of the article.

![The same chart as above, but with the trajectory of Norway added](How%20to%20introduce%20less%20common%20chart%20types%2054daaa496c1540519f56f72f5ba88962/connected-scatter-vox-roser-intro3.webp)

Source: [The US lags far behind its peers on “inclusive” economic growth](https://www.vox.com/the-big-idea/2017/2/3/14491964/growth-inequality-comparative-us-europe), vox.com

Only after this step, the final chart is revealed.

![The same chart as above, but with the trajectories for the US and the UK added](How%20to%20introduce%20less%20common%20chart%20types%2054daaa496c1540519f56f72f5ba88962/connected-scatter-vox-roser%201.webp)

Source: [The US lags far behind its peers on “inclusive” economic growth](https://www.vox.com/the-big-idea/2017/2/3/14491964/growth-inequality-comparative-us-europe), vox.com

So, to lower the cognitive load of this unfamiliar chart type, the chart authors **reveal the complexity of the chart in small chunks**, which is much easier to digest (see the <span class='internal-link'>[cognitive load concepts for storytelling](cognitive-load-concepts-for-storytelling)</span> page) . On top of that, they use very **clear annotations** to indicate what lines and directions means on the chart.

Here is another weird looking chart you are probably not familiar with:

![A chart showing polling scores of the Greens party in Germany](How%20to%20introduce%20less%20common%20chart%20types%2054daaa496c1540519f56f72f5ba88962/german-elections-datawrapper.png)

Source: [Which German party is the most unlucky when it comes to election dates?](https://blog.datawrapper.de/german-party-polls-vs-election-results/), blog.datawrapper.de

It is a chart showing the polling scores of the German Greens party, and how much the polling results where above or below the next election result of the party. The chart can’t be interpreted without this information, and explaining in words how to read it is much less effective than including a visual legend, as the legend below proves.

![A magnification of the chart above, with annotations added to explain how to read the chart](How%20to%20introduce%20less%20common%20chart%20types%2054daaa496c1540519f56f72f5ba88962/german-election-legend-datawrapper.png)

Source: [Which German party is the most unlucky when it comes to election dates?](https://blog.datawrapper.de/german-party-polls-vs-election-results/), blog.datawrapper.de

So another strategy of explaining how a chart works is including a small version of the chart and explain how it works by adding annotations.

Horizon charts are another example of chart types that definitely need some explanation for the reader. Here is an example of horizon chart published in a newspaper.

![A horizon chart showing trends in the stock prices of some 70 stocks](Choosing%20the%20right%20chart%20type%20for%20your%20story%20534c70625e194b62ad932d52825d1579/horizon-chart-ruys.png)

Source: [Frédérik Ruys](https://www.vizualism.nl/winnaar-infographicjaarprijs-2009/) for Financieele Dagblad

Notice the small “How the chart works” section (”Hoe het werkt” in Dutch) in the bottom right corner. This is not so much explaining how the chart should be read, but rather **how it is constructed**. Only the last little panel shows how the horizons should be read. Frédérik Ruys, the author of the chart, also made a little **animation** to show how horizon charts are constructed.

![An animation that explains how a horizon chart is constructed and how it should be read](How%20to%20introduce%20less%20common%20chart%20types%2054daaa496c1540519f56f72f5ba88962/horizonchart2.gif)

Source: [vizualism.nl](https://www.vizualism.nl/winnaar-infographicjaarprijs-2009/)