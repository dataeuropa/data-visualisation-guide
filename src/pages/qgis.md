Free, desktop app

[qgis.org](https://qgis.org/)

QGIS is a mature open source and free desktop Geographical Information System (GIS). It can be used to manage, process and model geographic data and works both with raster and vector geodata. Its visualisation and map styling capabilities are extensive and the QGIS community also publishes plugins for making things like animations and 3D visualisations.

![A screenshot of the QGIS interface showing a map of regions of the EU in shades of green and pink](Data%20visualisation%20design%20in%20practice%202%20tools%20208f06b06b0f4b21ad8ecf3047f02ce0/QGIS.png)

The QGIS interface. Source: Maarten Lambrechts, CC BY SA 4.0