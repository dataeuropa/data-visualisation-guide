<p class='center'>
<img src='Data%20story%20genres%20and%20structures%20854bd72307ad4dbda8a777a86347f3df/slideshow.png' alt='A small illustration of a slide show' class='max-200' />
</p>

Source: [Narrative Visualization: Telling Stories with Data](https://cpb-us-e1.wpmucdn.com/sites.northwestern.edu/dist/3/3481/files/2015/02/Narrative_Visualization.pdf)

Slide shows are sequences of visualisations, each with a some explanatory text. The slides are put one after the other to build up the story, and transitions between slides can be faded or animated.

![A screenshot showing lines of monthly average temperatures for the period of January to December. The navigation shows that the user is currently on slide 7 of 9](Data%20story%20genres%20and%20structures%20854bd72307ad4dbda8a777a86347f3df/belgium-heating-up.png)

How Belgium’s heating up explains climate change visible in Belgian meteorological data. Source: [Maarten Lambrechts](http://www.maartenlambrechts.be/vis/warm2015/)

Media used to publish a lot of slide shows with buttons and other controls that users needed to interact with to advance the story. But analytics showed that only a small share of users used these controls, and that “users just want to scroll”. So today, slide shows have become rare in media. They are mostly replaced with traditional magazine style formats, with text and visualisations alternating, or with <span class='internal-link'>[scrollytelling](tag/scrollytelling)</span> formats.