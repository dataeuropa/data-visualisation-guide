Columns in the data that are not relevant for your analysis or visualisation purposes can be deleted (make sure you have a copy of the original data before doing this). For data in the wide format (see the<span class='internal-link'>[Wide versus long data](wide-versus-long-data)</span> page), you can just drop the columns you don’t need.

Deleting columns from data in the long format may require aggregation. For example, if you are only interested in the average employment rate over the last five years for the EU member states, you need to group the records in the following table by country, and calculate the average for the Value column for each group of records.

| Country | Year | Value |
| --- | --- | --- |
| Belgium | 2017 | 69,8 |
| Belgium | 2018 | 71 |
| Belgium | 2019 | 71,8 |
| Belgium | 2020 | 71,5 |
| Belgium | 2021 | 71,9 |
| Bulgaria | 2017 | 71,4 |
| Bulgaria | 2018 | 72,4 |
| Bulgaria | 2019 | 75 |
| Bulgaria | 2020 | 73,4 |
| Bulgaria | 2021 | 73,2 |
| Czechia | 2017 | 78,4 |
| Czechia | 2018 | 79,8 |
| Czechia | 2019 | 80,2 |
| Czechia | 2020 | 79,6 |
| Czechia | 2021 | 79,8 |
| Denmark | 2017 | 77,8 |
| Denmark | 2018 | 78,7 |
| Denmark | 2019 | 79,4 |
| Denmark | 2020 | 78,8 |
| Denmark | 2021 | 79,8 |