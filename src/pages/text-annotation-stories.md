Sometimes a story based on data lends itself well to a story format that almost exclusively relies on annotations. These stories use a visualisation as the canvas, and the annotations to guide the reader through the visualisation and explain what is going on in the data.

An example of this is the visualisation contained in the article titled [Bailout costs will be a burden for years](https://www.ft.com/content/b823371a-76e6-11e7-90c0-90a9d1bc9691), by the Financial Times. The bulk of the article consists of an annotated chart, only below this visualisation does the article contain some paragraphs of additional text.

Annotations on this chart include

- a “how to read” section at the top
- country names on the left and data values on the right (those are not really annotations, but category and data labels, see further)
- quotes about the country bailouts directly in the visualisation
- an explanatory annotation to explain the magnified portion of the chart

![A horizontal stacked bar chart with bars of varying heights showing countries' GDP and the cumulative impact of bailout since 2007 on Gross Public Debt and other support, with many quotes and annotations](Patterns%20for%20data%20driven%20stories%2034fe0220a7d64297ae6ccf534303e18f/crisis-annotation-chart-FR.webp)

Source: [FT.com](https://www.ft.com/content/b823371a-76e6-11e7-90c0-90a9d1bc9691)

Another classic in this genre is the [connected scatterplot](http://steveharoz.com/research/connected_scatterplot/) below, showing how the distance driven per capita and the traffic accident fatalities in the United States have evolved over time. The story of the chart is told by the text annotating the line chart. Because the chart is not a traditional line chart, with time not running from left to right on the x axis, and people might not understand immediately how the chart should be read, the way to read the chart is explained with

- axes titles in bold, with arrows pointing at the higher end of the axes
- duplicated axes titles left and right
- integrated smaller versions of the chart in the chart annotations, highlighting the period the annotation is referring to
- labels for each year
- repeated arrows showing the chronological order of the data points

![A connected scatterplot with vehicle miles driven per capita on the x axis and auto fatatlities per 100.000 people on the y axis. A series of 7 text annotations explain the patterns in the chart](Text%20annotations%204d77570c409249378ca558ae45eb0d67/driving-shifts-into-reverse-annotated-connected-scatterplot.png)

Source: [Driving Safety, in Fits and Starts](https://archive.nytimes.com/www.nytimes.com/interactive/2012/09/17/science/driving-safety-in-fits-and-starts.html), nytimes.com

The visualisation below, titled “Which countries do refugees come to the UK from”, uses long explanatory annotations to explain the patterns visible on the visualisation.

![A streamgraph with 11 annotations giving context to the visualisation](Text%20annotations%204d77570c409249378ca558ae45eb0d67/annotated-streamgraph.png)

Source: [Asylum Statistics Research Briefing](https://researchbriefings.files.parliament.uk/documents/SN01403/SN01403.pdf), House of Commons Library

Another example of an annotated timeline telling a very clear story, is [A Timeline of Earth’s Average Temperature](https://xkcd.com/1732/), by web comic artist XKCD.

Below is an example of an annotated chart that was published in a scientific journal, to illustrate that the use of annotations and annotation stories are not limited to media and journalism.

![A heatmap of the male-female mortality ratio in England and Wales from 1905 to 2013. 4 annotations explain the patterns in the chart](Text%20annotations%204d77570c409249378ca558ae45eb0d67/jonas-scholey-annotations.jpg)

Source: [@jschoeley](https://twitter.com/jschoeley/status/1493512903701508099), [Visualizing compositional data on the Lexis surface](https://www.demographic-research.org/volumes/vol36/21/)