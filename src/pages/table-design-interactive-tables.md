Consider the two interactive tables below. They don’t offer the scannability of printed tables (you can’t see all records in the table at once, you would have to use the pagination to go through all rows of the tables), but they offer something more powerful: a search box. With a search box, it is even more easy to find a row of interest.

<iframe title="The most visited art museums in the world" aria-label="Table" id="datawrapper-chart-bboxO" src="https://datawrapper.dwcdn.net/bboxO/1/" scrolling="no" frameborder="0" style="border: none;" width="100%" height="605" data-external="1"></iframe>

<iframe title="Life expectancy in all countries increased since 1960, but with a different pace" aria-label="Table" id="datawrapper-chart-NUn3X" src="https://datawrapper.dwcdn.net/NUn3X/1/" scrolling="no" frameborder="0" style="border: none;" width="100%" height="582" data-external="1"></iframe>

Interactive tables can also offer user defined sorting, so that users can choose which column to sort the table on, in which direction (ascending or descending).

More advanced tables could have filters to filter the displayed rows and can have a fixed header row or a fixed first column with names or id’s that always stay into view.