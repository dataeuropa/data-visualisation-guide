Visual annotations can also be used to indicate values that provide information about the distribution of the data values visualised. 

A line on a bar chart can indicate the median or mean of the values of all bars, for example. This can help the reader in assessing whether the value of a bar can be considered “low” or “high”.

![Source: Maarten Lambrechts, CC-BY 4.0](Visual%20annotations%20589ebcc4e0024634956566d2e144385f/barchart-average2x.png)

Source: Maarten Lambrechts, CC-BY 4.0

Adding lines for the medians in a scatterplot can serve as a powerful storytelling mechanism: the medians for the x and y values create 4 quadrants that can be used to classify the data marks into 4 categories for x and y values (”low-low”, “low-high”, “high-low” and “high-high”).

<iframe src='https://graphics.axios.com/2019-04-06-occupation-demographics/index.html' width='100%' height='700px' style='border: none;'></iframe>

This interactive chart uses the “medians-quadrant” visual annotation technique. Notice how the chart makes use of multiple highlighting annotations: the bubbles in each quadrant have 4 different colours , with the ones on the left highlighted and the ones on the right dehighligted, the labelled bubbles have subtle outlines, and when hovered, bubbles get a strong outline. Source: [The millennials who are making it](https://www.axios.com/the-oldest-and-youngest-jobs-in-the-us-millennials-d9738704-4c84-4208-8f15-8d997db170ac.html), axios.com

![A scattter plot of footballers with successful dribbles on the x axis and proportion of successfull dribbles on the y axis. 2 lines are added for the medians of x and y](Visual%20annotations%20589ebcc4e0024634956566d2e144385f/median-scatterplot-driblab.jpg)

Source: [@driblab](https://twitter.com/driblab/status/1224387280271544322) 

Visual annotations can also be used to highlight the range or spread in data values by highlighting the distance between minimum and maximum values.

![A dot plot showing the employment rates for EU regions](Visual%20annotations%20589ebcc4e0024634956566d2e144385f/range-annotations-eurostat.png)

The range of values for the regions in each country are highlighted with a grey rectangle in the background. Notice how the minimum and maximum values are also highligted with added data labels. Source: [Regions and Cities Illustrated](https://ec.europa.eu/eurostat/cache/RCI/#?vis=nuts2.labourmarket&lang=en), Eurostat

When differences between two time series are relevant for the message of a chart, it makes a lot of sense to highlight them by applying some colour to the area between. Scottisch data visualisation pioneer William Playfair, applied this technique in the first line charts ever made:

![A line chart titled 'Exports and Imports to and from Denmark & Norway from 1700 to 1780'](Visual%20annotations%20589ebcc4e0024634956566d2e144385f/Playfair_TimeSeries.png)

Highlighted time series differences in the first line chart ever produced. Source: [The Commercial and Political Atlas, William Playfair](https://commons.wikimedia.org/wiki/File:Playfair_TimeSeries.png), public domain

Colour legends can be used to give additional information on the distribution of values, like showing the highest and lowest values.  They can even be turned into histograms to show the distribution of the values in the data set.

![A choropleth map showing the share of people udner 65 without insurance in US counties. The colour legend of the map is also a histogram showing the number of counties in each bucket of the colour scale](Visual%20annotations%20589ebcc4e0024634956566d2e144385f/colour-legend-histogram-npr.png)

Source: [Maps Show A Dramatic Rise In Health Insurance Coverage Under ACA](https://www.npr.org/sections/health-shots/2017/04/14/522956939/maps-show-a-dramatic-rise-health-in-insurance-coverage-under-aca), npr.org