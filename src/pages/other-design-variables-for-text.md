Apart from the font and typography, there are some other characteristics of text on visualisations that you should keep in mind.

First of all: **avoid non-horizontal text** as much as possible. Vertical or rotated text is very hard to read. Vertical or rotated text is very common in bar charts with vertical bars and long labels. 

![A vertical bar chart showing the greenhouse gas emissions of EU member states, with vertical country labels](Typography%20and%20the%20design%20of%20text%20elements%203d739b7b83f2405290637ce3d7a4a814/vertical-text-eurostat.png)

Source: [Eurostat](https://ec.europa.eu/eurostat/web/products-eurostat-news/-/DDN-20220215-1)

But this problem is easily overcome by rotating the chart 90 degrees, to end up with horizontal bars and horizontal text.

![The same chart as above, but rotated 90 degrees, so the country labels are horizontal and readable](Typography%20and%20the%20design%20of%20text%20elements%203d739b7b83f2405290637ce3d7a4a814/vertical-text-eurostat-rotated.png)

Source: Eurostat

Multiline text elements should be **aligned left**. Right aligned or center aligned text can sometimes be argumented for. But never justify longer text, especially not with narrow text columns: this will create holes in your text, and will look messy.

<p class='center'>
<img src='Typography%20and%20the%20design%20of%20text%20elements%203d739b7b83f2405290637ce3d7a4a814/unjustified-justify_32x.png' alt='A snippet of text with justified alignment. The last two words of the snippet are on a single line, with one word on the left and the other one on the right' class='max-400' />
</p>

Justified text leads to irregular spacing between words. Source: Maarten Lambrechts, CC BY 4.0

We’ve discussed letter spacing as a technique to make text more condensed. But letter spacing can also be used the other way around, to let a word (or a couple of words) occupy more space. You could use a bigger font size to do this, but sometimes you want words to occupy a bigger space without giving them too much visual weight. In that case increasing the letter spacing is a good option.

This technique is very common in cartography, for example for labelling countries, oceans and mountain ranges on maps.

![A map of the central part of South America, showing country names, cities and ecoregions](Typography%20and%20the%20design%20of%20text%20elements%203d739b7b83f2405290637ce3d7a4a814/equal-earth-letter-spacing.png)

The names of the ecoregions (”Mato Grosso Plateau”, “Andes”, “Gran Chaco”, ...) use an increased letter spacing. This allows them to occupy a bigger space while still not coming to the foreground. Source: [equal-earth.com](https://www.equal-earth.com/), public domain

This technique of increasing the letter spacing can be used to label bigger areas in a visualisation, like the quadrants of a scatter plot, or the areas in a stacked area chart.

The formatting of numbers and dates is part of the design of text elements, but this is covered by the <span class='internal-link'>[number formatting](number-formatting)</span> <span class='internal-link'>[date formatting](date-formatting)</span> pages.