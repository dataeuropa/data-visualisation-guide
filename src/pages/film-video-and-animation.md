<p class='center'>
<img src='Data%20story%20genres%20and%20structures%20854bd72307ad4dbda8a777a86347f3df/filmvideoanimation.png' alt='' class='max-200' />
</p>

Source: [A small illustration of a film negative showing a setting sun](https://cpb-us-e1.wpmucdn.com/sites.northwestern.edu/dist/3/3481/files/2015/02/Narrative_Visualization.pdf)

Video can be a powerful format for data driven storytelling. But it is a separate discipline, requiring specalised skills, like producing voice overs and video editing.

A recent nice example of using data visualisation in video is [Pandemic Polarization](https://www.youtube.com/watch?v=sv0dQfRRrEQ&ab_channel=Vox) by Vox.

<iframe width="100%" height="450" src="https://www.youtube.com/embed/sv0dQfRRrEQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

It uses a slideshow format in the beginning, revealing new charts and data or annotating visualisations already in view. This illustrates that genres can be used in a hybrid way. A video can use slides shows, but also comic strips or flow charts, while a slide show can use annotated charts but could also contain video, for example. The storytelling genres are not mutually exclusive.

Notice that later in the video above, traditional video formats like interviews and testimonials are mixed with the visualisations. You can find more examples of video combined with data visualisation on the <span class='internal-link'>[Combining with other media: video](combining-with-other-media-video)</span> page.