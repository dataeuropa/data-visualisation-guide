A **density plot** is a continuous and smoothed version of a histogram. The biggest advantage is that its shape does not depend on the number of buckets like the histogram does, because density plots don’t use buckets.

![3 density plots showing the bill length distribution of 3 species of penguin, plotted on 3 different base lines](Visualising%20distributions%2024ffe2f7bda24dc6b496de328dc7df6a/penguin-slabs.png)

Source: Maarten Lambrechts, CC-BY-SA 4.0

Because density plots can be plotted on a single base line, they make comparing distributions easier than histograms.

![The same density plots as above, but plotted on a single base line, so they are overlapping](Visualising%20distributions%2024ffe2f7bda24dc6b496de328dc7df6a/penguin-densities-overlaid.png)

Source: Maarten Lambrechts, CC-BY-SA 4.0

When a density plot is mirrored and a box plot is overlaid, the result is often called a **violin plot** (which usually has a vertical orientation), or an **eye plot** (usually horizontal). 

![3 eye plots showing the distribution of the bill lengths of the 3 species of penguin](Visualising%20distributions%2024ffe2f7bda24dc6b496de328dc7df6a/penguin-eyes.png)

An eye plot. Source: Maarten Lambrechts, CC BY SA 4.0

Another popular combination are density plots together with dot plots, or density plots together with jittered data points. This combination is called a **raincloud plot**.

![3 raincloud plots showing the distribution of the bill lengths of the 3 species of penguin](Visualising%20distributions%2024ffe2f7bda24dc6b496de328dc7df6a/penguin-rainclouds.png)

Source: Maarten Lambrechts, CC BY SA 4.0