<p class='center'>
<img src='Data%20story%20genres%20and%20structures%20854bd72307ad4dbda8a777a86347f3df/partitionedposter.png' alt='A small illustration of poster showing a sillhouette of a person and 3 little charts' class='max-200' />
</p>

Source: [Narrative Visualization: Telling Stories with Data](https://cpb-us-e1.wpmucdn.com/sites.northwestern.edu/dist/3/3481/files/2015/02/Narrative_Visualization.pdf)

Partitioned posters can contain very limited or even no data visualisations at all when they illustrate concepts and ideas instead of data. They used to be very popular and were often called “infographics”.

![ ](Data%20story%20genres%20and%20structures%20854bd72307ad4dbda8a777a86347f3df/lunge-feeding-jonathan-corum.jpg)

Part of newspaper page explaining how whales dive and feed themselves. Data visualisation, illustration and text are combined with series of panels. Source: [nytimes.com](https://archive.nytimes.com/www.nytimes.com/imagepages/2007/12/10/science/20071211_WHALE_GRAPHIC.html)

