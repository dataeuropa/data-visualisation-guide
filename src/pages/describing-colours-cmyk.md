You might know CMYK from the ink cartridges in your printer. CMYK stands for **C**yan, **M**agenta, **Y**ellow and **K**ey (black).

<p class='center'>
<img src='Colour%20the%20basics%20a90e331756d2497aa8b2b3ce26b9e3de/1024px-CMYK_subtractive_color_mixing.svg.png' alt='A yellow, a magenta and a cyan circle plotted on top of each other. Overlap between the yellow and magenta circles results in red, between yellow and cyan in green, between magenta and cyan blue, and where all three overlap black' class='max-600' />
</p>

Source: [SharkD](https://commons.wikimedia.org/wiki/File:CMYK_subtractive_color_mixing.svg), CC0

The CMYK model is a subtractive model and can be best understood from the process of printing ink with CMYK colours on a (white) piece of paper: where cyan, magenta and yellow ink overlap, no light (or almost none) is reflected, resulting in black. Where for example cyan and yellow overlap, magenta is “subtracted” from the reflected white light, resulting in a green colour.

Printers do not print full blocks of solid cyan, magenta and yellow colours. If they would, printers would only be able to print 7 colours (the ones in the image above). Instead, printers print tiny dots of ink, so that for example pink can be produced by small dots of magenta with enough white space between the dots. Because the dots are so small, we perceive a solid pink colour.

This technique of printing small dots of ink is called halftoning. By varying the size of the dots for each color, printers can produce many different colours. In the image below, you can see a CMYK print under a microscope. You can clearly see the dots of the CMYK colours.

![A CMYK print viewed under a microscope](Colour%20the%20basics%20a90e331756d2497aa8b2b3ce26b9e3de/CMYK_under_a_microscope.jpg)

Source: [Psihedelisto](https://commons.wikimedia.org/wiki/File:CMYK_under_a_microscope.jpg), CC0

The image below is the same image as above, but displayed much smaller. It is impossible to spot the individual dots of ink, and the image seems to contain solid, light and darker teal tints.

<p class='center'>
<img src='Colour%20the%20basics%20a90e331756d2497aa8b2b3ce26b9e3de/CMYK_under_a_microscope%201.jpg' alt='The same CMYK print as above, but with a zoomed out view' class='max-50' />
</p>

Source: [Psihedelisto](https://commons.wikimedia.org/wiki/File:CMYK_under_a_microscope.jpg), CC0

The image below illustrates how printers can produce different colours by using halftoning. Different dot sizes for different ink colours determine the resulting perceived colour. Note the use of black ink dots, which is more efficient than combining cyan, magenta and yellow to produce black. Rotating the grid of dots for the ink colours produces better print results.

![3 examples of how differently sized dots of the CMYK colours produce different final colours](Colour%20the%20basics%20a90e331756d2497aa8b2b3ce26b9e3de/2560px-Halftoningcolor.svg.png)

Source: [en.wikipedia.org/wiki/File:Halftoningcolor.svg](https://en.wikipedia.org/wiki/File:Halftoningcolor.svg), public domain