For an overview of pitfalls to avoid in ethical, visual representation of data, see the pages on <span class='internal-link'>[Pitfalls in dataviz: scales](tag/pitfalls-in-dataviz-scales)</span>, <span class='internal-link'>[Pitfalls in dataviz: colours](tag/pitfalls-in-dataviz-colours)</span> and <span class='internal-link'>[Pitfalls in dataviz: chart types](tag/pitfalls-in-dataviz-chart-types)</span>. These pitfalls are listed here again for reference. So, ethical data visualisation also means

- not <span class='internal-link'>[breaking scales](breaking-scales)</span>
- choosing an appropriate number to <span class='internal-link'>[start your y axis in line scales](scales-in-line-charts)</span>
- choosing an appropriate <span class='internal-link'>[width to height ratio](width-to-height-ratio)</span>
- not <span class='internal-link'>[distorting proportions](respecting-proportions)</span>
- preserving <span class='internal-link'>[the same scale for comparisons](preserving-scales-for-comparisons)</span>
- avoiding <span class='internal-link'>[rainbow colour scales](endrainbow)</span> and using <span class='internal-link'>[perceptually uniform colour scales](perceptual-uniformity)</span>
- using <span class='internal-link'>[appropriate chart types](tag/pitfalls-in-dataviz-chart-types)</span> for your data and message
- avoiding <span class='internal-link'>[double y axis charts](line-charts-double-y-axes)</span>
- choosing an appropriate <span class='internal-link'>[line interpolation](line-charts-line-interpolations)</span> for line charts
- being careful with <span class='internal-link'>[stacked area and stacked bar charts](stacked-charts)</span>
- being careful no to <span class='internal-link'>[suggest causality when there is only correlation](correlation-is-not-causation)</span>

These pitfalls should be avoided to produce undistorted, meaningful and truthful visualisations to communicate data.

When any chance in misinterpretation arises, chart authors should guide the reader in a way that visualisations are clear, and can only be interpreted in the correctly. This can be done by making good use of labels, axis titles, legends and annotations.