Trying to tell a story with data visualisation is similar to how comic strips use sequences of frames to tell a story. Researchers noted this and [started to study the field of data comics](https://datacomics.github.io/), which combines both fields. Below you can find some examples.

![A spread of the data comic 'Open borders'](Combining%20visualisations%20with%20other%20media%203195c842e70245d394d6c9e019d7165e/open-borders-data-comic.png)

Source: [Open borders : the science and ethics of immigration](https://datacomics.github.io/comicfiles/openBorders.pdf), Bryn Caplan and Zach Weinersmith

![A data comic by XKCD titled 'How big a change is that?'](Combining%20visualisations%20with%20other%20media%203195c842e70245d394d6c9e019d7165e/4_5_degrees_2x.png)

Source: [xkcd.com/1379](https://xkcd.com/1379/)

![A data comic showing the time use of an average American titled 'A day in the life of Americans'](Combining%20visualisations%20with%20other%20media%203195c842e70245d394d6c9e019d7165e/data-comic-matt-hong-time-use.png)

Source: [A day in the life of Americans: a data comic](https://medium.com/@MattIanHong/how-americans-spend-each-day-a-data-comic-51817f910dd7), Matt Hong

![A data comic about trends in productivity and wages data](Combining%20visualisations%20with%20other%20media%203195c842e70245d394d6c9e019d7165e/decoupling-comic-willikinwolf.jpg)

Source: [@WillikinWolf](https://twitter.com/WillikinWolf/status/1176006515968241665)