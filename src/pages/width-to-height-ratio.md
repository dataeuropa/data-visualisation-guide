A wide chart layout, with a high width to height ratio, will flatten trends in time series.  A high layout, with a low width to height ratio, will do the reverse and stress or even dramatise trends in the data.

![A line chart with a flat and wide layout](Pitfalls%20in%20dataviz%20scales%20and%20proportions%20c55dba398451424aa684d319018f8380/linechart-wide2x.png)

A wide and flat layout suggest slow trends or even flat lines. Source: Maarten Lambrechts, CC BY 4.0

<p class='center'>
<img src='Pitfalls%20in%20dataviz%20scales%20and%20proportions%20c55dba398451424aa684d319018f8380/linechart-narrow2x.png' alt='The same line chart as above, but with a narrow and high layout' class='max-200' />
</p>

Narrow and high designs exaggerate trends. Source: Maarten Lambrechts, CC BY 4.0

So in a sense, changing the width to height ratio of a line chart has the same effect as truncating (or extending) the y axis. So what is a “good”, “correct” or “honest” aspect ratio for line charts? One often cited rule, called “banking to 45 degrees”, says that the average slope of the lines on a chart should be 45 degrees. So you could try to aim for that.