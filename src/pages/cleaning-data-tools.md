In the video below, a few tools for working with data are demonstrated. These tools are:

- [Tabula](https://tabula.technology/)
- Microsoft Excel
- [Google Sheets](https://www.google.com/sheets)
- [Open Refine](https://openrefine.org/)

<iframe width="100%" height="450" src="https://www.youtube.com/embed/DrbfRi2N-tE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

<aside>
🔎 As an exercise for after watching the video: download and install Tabula, and try to extract the data out of the tables of the example PDF file used in the video. <a href="https://ec.europa.eu/eurostat/documents/2995521/14636032/4-13052022-AP-EN.pdf/856c2d2a-adc3-25ee-66d5-f4aacc90c5b5?t=1652362973659">This PDF file can be found here]</a>.

</aside>