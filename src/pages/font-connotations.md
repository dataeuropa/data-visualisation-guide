Just like <span class='internal-link'>[colours have connotations](colour-connotations)</span>, fonts can evoke feelings too: some will feel serious, others playful, some will feel old and others modern and some will feel technical and others might even evoke feelings of horror and fear.

<p class='center'>
<img src='Typography%20and%20the%20design%20of%20text%20elements%203d739b7b83f2405290637ce3d7a4a814/im-fell-dw-pica-font.png' alt='A snippet of text in the IM Fell DW Pica font' class='max-600' />
</p>

[IM Fell DW Pica](https://fonts.google.com/specimen/IM+Fell+DW+Pica) has an authentic and old look, because it resembles text printed on old printing presses.

<p class='center'>
<img src='Typography%20and%20the%20design%20of%20text%20elements%203d739b7b83f2405290637ce3d7a4a814/exo-font.png' alt='A snippet of text in the Exo font' class='max-600' />
</p>

[Exo](https://fonts.google.com/specimen/Exo) is a “sans serif typeface that tries to convey a technological/futuristic feeling”

<p class='center'>
<img src='Typography%20and%20the%20design%20of%20text%20elements%203d739b7b83f2405290637ce3d7a4a814/creepster-font.png' alt='The word ARACHNOPHOBIA in the Creepster font' class='max-600' />
</p>

[Creepster](https://fonts.google.com/specimen/Creepster) is a “fright-filled font, perfect for all of your grisly graphic needs”

For serious publications, it is advisable to stay away from fonts that are too playful and from fonts that evoke undesired emotions.