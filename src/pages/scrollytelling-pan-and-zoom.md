In the pan and zoom scrollytelling technique the scroller controls which section of the visual element is visible. Scrolling causes the the visual to zoom in and out or move the visual horizontally (this is called panning).

![A four panel illustration of how pan and zoom scrollytelling work, with the text 'Scrolling... zooms into the map... and moves around](Scrollytelling%200ae4533947224ed3b08305e4c650ce0d/scrollytelling-pan-zoom.png)

Source: Oesch, Jonas, Manuel Roth, and Adina Renner. “Scrolling into the Newsroom: A Vocabulary for Scrollytelling Techniques in Visual Online Articles.” Information Design Journal. Forthcoming 2022

This technique is mostly used with maps as the zoomable and pannable graphic. In between the zooming and panning triggered by scrolling, the user might have control over the zooming and panning herself, or not.

The map in “The Great Flood of 2019” only uses panning:

![Screenshot of maps showing flooded areas in the US](scrollytelling-floods-nyt.jpg)

Source: [The Great Flood of 2019: A Complete Picture of a Slow-Motion Disaster (Published 2019)](https://www.nytimes.com/interactive/2019/09/11/us/midwest-flooding.html)

“Is the world full or empty?” uses a globe as the graphic and let’s you fly from place to place while scrolling:

![Screenshot of a snippet of text about Manilla and satellite imagery of the city](scrollytelling-world-full-arcgis.jpg)

Source: [Is the world full or empty?](https://storymaps.arcgis.com/stories/b4380439bc8c4293b36a4f9772c665ba)

“How Russia’s mistakes and Ukrainian resistance altered Putin’s war” also rotates and tilts the map when zooming in:

![Screenshot of a black and white map zoomed in on the city of Summy](scrollytelling-ukraine-ft.jpg)

Source: [How Russia's mistakes and Ukrainian resistance altered Putin's war](https://ig.ft.com/russias-war-in-ukraine-mapped/)