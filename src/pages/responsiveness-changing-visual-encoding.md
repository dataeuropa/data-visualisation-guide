After just removing a visualisation, the most radical way to adapt a visualisation for smaller screens is to change its visual encoding, and using another chart type.

In order to show time series with annotations on smaller screens, NPR changed the chart type from a line chart to a heatmap in the following example.

![A line chart with 3 lines showing the number of times the words phone, smartphone, or mobile were mentioned in quarterly earnings calls of Yahoo, Google and Microsoft between 2006 and 2016](Responsiveness%20and%20data%20visualisation%20for%20small%20sc%20bfcc7f2b3f63483d9213104e4137aec4/hbr-yahoo-desktop.png)

Source: [hbr.org](https://hbr.org/resources/html/infographics/2016/06/was-yahoo-late-to-mobile/index.html)

<p class='center'>
<img src='Responsiveness%20and%20data%20visualisation%20for%20small%20sc%20bfcc7f2b3f63483d9213104e4137aec4/hbr-yahoo-mobile.png' alt='' class='max-400' />
</p>

Source: [hbr.org](https://hbr.org/resources/html/infographics/2016/06/was-yahoo-late-to-mobile/index.html)

From an accessibility standpoint, showing a table with the data contained in a visualisation is a good idea. It can also be a good fallback on mobile screens, as demonstrated by this map by the New York Times:

![A map of the US with arrows showing the increase in homicide rates in 2015](Responsiveness%20and%20data%20visualisation%20for%20small%20sc%20bfcc7f2b3f63483d9213104e4137aec4/murder-rates-nyt-desktop.png)

Source: [nytimes.com](https://www.nytimes.com/interactive/2016/09/08/us/us-murder-rates.html)

<p class='center'>
<img src='Responsiveness%20and%20data%20visualisation%20for%20small%20sc%20bfcc7f2b3f63483d9213104e4137aec4/murder-rates-nyt-mobile.png' alt='A smaller map with numbers, which are labelled with a table below the map' class='max-400' />
</p>

Source: [nytimes.com](https://www.nytimes.com/interactive/2016/09/08/us/us-murder-rates.html)