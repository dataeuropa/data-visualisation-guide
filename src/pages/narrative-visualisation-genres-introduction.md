In a study executed in 2010, 2 researchers collected examples of data driven and visual news stories in order to detect common patterns in these stories and to describe different genres for these kind of news stories. Although [the study](https://cpb-us-e1.wpmucdn.com/sites.northwestern.edu/dist/3/3481/files/2015/02/Narrative_Visualization.pdf) is already quite old given the rapid development of tools and techniques for online articles (<span class='internal-link'>[scrollytelling](tag/scrollytelling)</span> wasn’t invented yet at the time, for example), some useful insights can be drawn from the study and its conclusions.

These are the genres the researchers identified:

- Narrative visualisation: <span class='internal-link'>[Magazine style](magazine-style-narrative-visualisation)</span>
- Narrative visualisation: <span class='internal-link'>[Annotated chart](annotated-chart-narrative-visualisation)</span>
- Narrative visualisation: <span class='internal-link'>[Partitioned poster](partitioned-poster-narrative-visualisation)</span>
- Narrative visualisation: <span class='internal-link'>[Flow chart](flow-chart-narrative-visualisation)</span>
- Narrative visualisation: <span class='internal-link'>[Comic strip](comic-strip-narrative-visualisation)</span>
- Narrative visualisation: <span class='internal-link'>[Slide show](slide-show-narrative-visualisation)</span>
- Narrative visualisation: <span class='internal-link'>[Slide show](film-video-and-animation)</span>