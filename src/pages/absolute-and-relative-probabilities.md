In April 2019 many media reported on a study that found people eating red and processed meat on a daily basis had an increased risk on developing colorectal cancer.

![Screenshot of a web browser showing an article on cnn.com titled 'Eating just one slice of bacon a day linked to higher risk of colorectal cancer, says study'](More%20pitfalls%20in%20statistics%20980beb010f984cd49c83ec8dab6dae6e/cnn-bacon.png)

Source: [cnn.com](https://edition.cnn.com/2019/04/17/health/colorectal-cancer-risk-red-processed-meat-study-intl/index.html)

This is what the researchers found:

> People who ate 76 grams of red and processed meat per day had a 20% higher chance of developing colorectal cancer compared to others, who ate about 21 grams a day.
> 

A 20% higher chance to get cancer seems considerable. So should you better stop eating red and processed meat? Let’s take a closer look at the numbers.

Of 10.000 people in the study who ate 21 grams of red or processed meat each day, 
40 developed colorectal cancer. This is 40/10.000 = 0,4%. Among those who ate 76 gram per day, 48 did, which is 0,48%. So eating more red or processed meat each day indeed means a 20% increase in the risk on cancer. But the increase is also just a 0,08 percentage point increase. A significant increase of a low probability is still a low probability.

But that doesn’t mean that relative increases in small probabilities are never important. The following news article is an example of that.

![Screenshot of a web browser showing an article on cnbc.com titled 'Covid variant first found in the U.K. appears to be 64% more deadly than earlier strains, study finds'](More%20pitfalls%20in%20statistics%20980beb010f984cd49c83ec8dab6dae6e/cnbc-covid.png)

Source: [cnbc.com](https://www.cnbc.com/2021/03/11/covid-variant-in-the-uk-appears-to-be-64percent-more-deadly-than-other-strains-study-finds.html#:~:text=Covid%20variant%20first%20found%20in,than%20earlier%20strains%2C%20study%20finds&text=Researchers%20in%20the%20U.K.%20analyzed,with%20other%20previously%20circulating%20strains.)

These are the numbers the article is based on:

> The researchers said B.1.1.7 led to 227 deaths in a sample of 54,906 patients. That compares with 141 deaths in roughly the same number of patients who were infected with other strains.
> 

141 deaths out of 54.906 patients is 0,26%, 227 out of 54.906 = 0,41%. The chance of dying from the new Covid variant is only 0,15 percentage point higher than the old variant. But this does not mean that the increased risk is unsignificant. The new Covid strain resulted in 86 more deaths, which means a lot of pain and mourning for a lot of people, which is obviously not something you can just ignore.