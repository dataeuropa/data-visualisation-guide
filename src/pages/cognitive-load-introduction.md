In <span class='internal-link'>[Data communication](data-communication)</span> a distinction is made between exploratory data visualisation (visualisations an analyst makes for herself to get a better understanding of data) and explanatory data visualisation. 

In explanatory data visualisation, visualisation is used as a means to transfer information and knowledge from one person to another. The visualisation creator gained some insights from a data set, and packs that insight into a visualisation so that other people can unpack the message and add the new insight to their mental model they use to make sense of the world.

That is how **storytelling** is related to data visualisation. Stories are told to transfer knowledge and insights about the world between people. Through stories we understand the world we live in.

But there is a third related field that may be more relevant for data visualisation, and that is the field of **education**. Educators also pack chunks of knowledge into different formats for their students to unpack and add to the knowledge they already have.

Education and psychology researchers have long studied how students consume and pick up new information. One concept that is particularly useful for delivering a message with data visualisation is **cognitive load**.

## Cognitive load?

A useful definition of cognitive load, is the following taken from [Cognitive Load as a Guide: 12 Spectrums to Improve Your Data Visualizations](https://nightingaledvs.com/cognitive-load-as-a-guide-12-spectrums-to-improve-your-data-visualizations), by Eva Sibinga and Erin Waldron: 

> Cognitive load describes the amount of working memory someone uses when they take in new information and transform it into their long-term memory. In simpler terms, cognitive load helps us consider how easy or difficult it is for someone to make sense of something new.
> 

From this definition you can see how important cognitive load is in education. The more mental work students have to put in, the harder it will be for them to understand and memorise new information. And the same applies to data visualisation: as a data visualisation designer, you want to make it as easy as possible for your audience to understand a chart and remember its main message.

Cognitive load is broken down into three types, and all are relevant for data visualisation:

- the <span class='internal-link'>[intrinsic load](intrinsic-cognitive-load)</span>
- the <span class='internal-link'>[extraneous load](extraneous-cognitive-load)</span>
- the <span class='internal-link'>[germane load](germane-cognitive-load)</span>