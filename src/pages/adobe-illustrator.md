Paying, desktop app

[How to create graphs in Illustrator](https://helpx.adobe.com/illustrator/using/graphs.html)

Illustrator is the vector image editor tool in Adobe’s collection of design tools. It is a very flexible tool, but the built in charting tools are old and limited in functionality. Illustrator is widely used in data visualisation to apply finishing touches to vector images generated with other tools, and to create custom and creative data visualisations (check the work of [Federica Fragapane](https://www.behance.net/FedericaFragapane) for examples).

![Screenshot of the Adobe Illustrator Graph tool, showing a very basic bar chart with one bar in black, and a data table with the value of 1 in a single cell](Data%20visualisation%20design%20in%20practice%202%20tools%20208f06b06b0f4b21ad8ecf3047f02ce0/illustrator-chart-tool.png)

Illustrator’s interface for making a bar chart. Source: Maarten Lambrechts, CC BY SA 4.0