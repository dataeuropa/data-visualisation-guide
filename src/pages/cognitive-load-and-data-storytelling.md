The working memory of the brain determines how much cognitive load we can handle when we are exposed to new information.

![An illustration of the working memory capacity, represented by a vertical bar](Reducing%20the%20cognitive%20load%2070a9cb0c4c8c4f45b66f63a749d9f40e/working-memory-doug-schepers.png)

Source: [Designing Data for Cognitive Load](https://www.youtube.com/watch?v=pkbU05dN6dg&t=674s&ab_channel=DataVisualizationSociety), Doug Schepers

So added up, the intrinsic, extraneous and germane load should not exceed the capacity of your audience’s working memory. If they do, your audience will get overwhelmed and confused: the information presented to them is too much, and they will simply give up.

![An illustration of the intrinsic load and extraneous load together exceeding the working memory capacity, leading to confusion and being overwhelmed](Reducing%20the%20cognitive%20load%2070a9cb0c4c8c4f45b66f63a749d9f40e/cognitive-overload-doug-schepers.png)

Source: [Designing Data for Cognitive Load](https://www.youtube.com/watch?v=pkbU05dN6dg&t=674s&ab_channel=DataVisualizationSociety), Doug Schepers

To avoid that, the different types of cognitive load need to balanced, so:

- high intrinsic load data and complex data driven messages should be presented with minimal extraneous load
- high intrinsic load data and complex data driven messages should be presented only to people for which this does not mean a high germane load (experts)
- low intrinsic load data or messages can tolerate higher extraneous loads.

Cognitive load that is too low also exist: when the cognitive load of educational material, or of a data driven message, is too low, your audience can get bored and switch off too.

![An illustration of the intrinsic load and extraneous load together being smaller than the working memory capacity, leading to boredom and disengagement](Reducing%20the%20cognitive%20load%2070a9cb0c4c8c4f45b66f63a749d9f40e/cognitive-underload-doug-schepers.png)

Source: [Designing Data for Cognitive Load](https://www.youtube.com/watch?v=pkbU05dN6dg&t=674s&ab_channel=DataVisualizationSociety), Doug Schepers

So “simplify as much as possible” might not be the best strategy, you need to challenge your audience (a little bit) to keep them engaged.