Next to position (<span class="internal-link">[in the x and y direction](visual-hierarchy-x-y-positioning)</span>, and <span class="internal-link">[in the z dimension](visual-hierarchy-z-positioning)</span>), sizing design elements is another obvious ways of bringing visual hierarchy into your designs: bigger things seem more important than smaller ones, and will draw more importance.

When the contrast in size is large enough, the size effect will compete with, or even overrule the positioning hierarchy, and the reading sequence will be changed. For example, a big and striking visualisation will be looked at first, even when it is not positioned in the top left, where most readers will usually start looking.

![A map showing land use in the US. The title is on the right and reads 'How America Uses Its Land'](Visual%20hierarchy%2032d60a2016ea4334ae0d7e2395559439/size-overruling-hierarchy.png)

In this layout, the graphic is very big in size and competes for the highest place in the visual hierarchy with the title, which occupies the most important location in the reading order but is much smaller in size. Source: [Alex Varlamov](https://public.tableau.com/app/profile/alexandervar/viz/HowAmericaUsesItsLand/USAMap)

In multi-visualisation designs, like dashboards, you could for example choose to position the key visualisation in the centre of the dashboard. As long as the contrast in size is big enough, it will be the part of the dashboard that people will be looking at first.

Within a visualisation, the sizing of text creates a clear visual hierarchy. Take a moment to look at the streamgraph below.

![A streamgraph showing streams for the names Michelangelo, Donatello, Raphael and Leonardo. The font size of the names is proportional to the size of the streams](Visual%20hierarchy%2032d60a2016ea4334ae0d7e2395559439/streamgraph-labels.png)

Source: [github.com/curran/d3-area-label](https://github.com/curran/d3-area-label)

What jumps into your eye first when you look at the graph? It probably is the “Leonardo” label, which is much bigger in size than the other labels.

This is the reason why the hierarchy in the text chart elements of a visualisation should be reflected in the sizing of the font. So the font size should go from large to small in the following order:

> Title > Sub title > annotations > data and category labels > axis labels > small print, like credits and sources
> 

![A line chart in which the different elements of the chart anatomy are annotated](Visual%20hierarchy%2032d60a2016ea4334ae0d7e2395559439/Chart-annotations2x.png)

Source: Maarten Lambrechts, CC BY 4.0

