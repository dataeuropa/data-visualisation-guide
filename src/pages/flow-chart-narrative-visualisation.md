<p class='center'>
<img src='Data%20story%20genres%20and%20structures%20854bd72307ad4dbda8a777a86347f3df/flowchart.png' alt='' class='max-200' />
</p>

Source: [A small illustration showing a drawing with a lot of different arrows](https://cpb-us-e1.wpmucdn.com/sites.northwestern.edu/dist/3/3481/files/2015/02/Narrative_Visualization.pdf)

Flow charts visualise processes, sequences of events and relationships between features.  They might not be driven by (numerical) data, but they can tell stories that are hard to tell using only text in a visual way.

![A schematic drawing explaining how the Covid-19 pandemic caused labor shortage, component shortages, shortage of durable goods and strained supply chains. Prepandemic situations are labelled in red, and the drawing has a lot of arrows](Data%20story%20genres%20and%20structures%20854bd72307ad4dbda8a777a86347f3df/flowchart-nytimes.png)

Source: [How the Supply Chain Crisis Unfolded](https://www.nytimes.com/interactive/2021/12/05/business/economy/supply-chain.html), nytimes.com