An important distinction in visualisation tools is the one between tools aimed at exploratory data visualisation and tools developed for making explanatory and publication ready visualisations.

Tools for **data analysis and exploratory data visualisation** integrate with data manipulation processes and allow you to quickly generate different views on the data. The design of the visualisations is of lesser importance in these tools.

Tools for **explanatory data visualisations** are more focussed on representing data in a style and quality ready for publication. They may require you to prepare your data in another tool, but the styling options are generally more extensive.

The output of a tool can be static or interactive. Static output can consist of <span class='internal-link'>[bitmap images](bitmap-images)</span> (JPG and PNG files) and <span class='internal-link'>[vector images](vector-images)</span> (usually SVG or PDF files), interactive output can consist of a web page or an embed code. Some tools can generate both static and interactive output, and interactivity ranges from full fledged dashboards to mostly static charts that only offer things like tooltips.