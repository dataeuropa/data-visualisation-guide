[xeno.graphics](https://xeno.graphics/)

Xenographics is a collection of weird but (sometimes) useful charts, curated by Maarten Lambrechts. The aim of the collection is to show novel, innovative and experimental visualisations that can be inspirational for visualising specific data sets.

![A browser window displaying the xeno.graphics homepage](Data%20visualisation%20galleries%2054f97b3d69b04dbe86cbf50ba86ab8c5/xenographics.png)

Source: [xeno.graphics](https://xeno.graphics)

Each visualisation in the collection has a small description and a link to the source.

![An UpSet plot as an example of a xenographic](Data%20visualisation%20galleries%2054f97b3d69b04dbe86cbf50ba86ab8c5/upset-plot-xenographics.png)

Source: [xeno.graphics](https://xeno.graphics/upset-plot/)