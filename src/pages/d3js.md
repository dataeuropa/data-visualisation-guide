Free, open source

[d3js.org](https://d3js.org/)

D3 is a JavaScript library for generating visualisations based on data in the browser. It does not offer chart templates, but instead consists of a collection of modules which each contain helper functions to perform actions like loading and parsing data, adding and removing elements to HTML documents, animating transitions, work with geographic data and scales to compute things like the position and colours of visual elements.

![ ](Data%20visualisation%20design%20in%20practice%202%20tools%20208f06b06b0f4b21ad8ecf3047f02ce0/d3-header.png)

The header image of D3js.org. Source: [d3js.org](https://d3js.org/)

D3 is very versatile and allows for creating custom and creative visualisations. It also forms the foundation of many visualisation tools, including some listed on this page. But D3’s power and flexibility comes at the cost of a relatively steep learning curve.