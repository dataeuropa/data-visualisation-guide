The design concepts of the grid and reading sequence have clear implications for the design of data visualisations. Here are some ground rules.

Whenever you are using a layout or design tool to lay out elements, turn on the "**Show the grid**" option to position and size your elements. This applies both to positioning and sizing charts in a publication, as to positioning and sizing the building blocks of a visualisation. Turning on the “Snap to grid” option makes using <span class="internal-link">[the grid](the-grid)</span> much easier.

Align the **title** of your chart in the **top left** of your visualisation, to respect <span class="internal-link">[the reading order](arrangement-and-reading-direction)</span>. The least important information should go in the bottom and could be right aligned to put it into the bottom-right corner

To align a visualisation in a publication, you can use between many **anchor points** (the left edge of the y axis labels, the left edge of the plotting area, the left edge of the chart area). Choose the one that feels right and creates the best balance.

**Align axis labels** both horizontally and vertically in a meaningful way

Use an **invisible grid** and invisible boxes and frames. This means you shouldn’t put a frame around your visualisation when you embed it. The plotting area doesn’t need a border either (if needed, you could give it a subtle background colour). In many cases, you can even remove the axis lines on a visualisation.

Use the <span class="internal-link">[break the grid](breaking-the-grid)</span> technique to draw attention to a data visualisation or to individual data points or outliers.

Charts with a cartesian coordinate system, with an X and a Y axis, have a natural grid built into it. It shouldn’t be too visibly present (or should even be invisible), but you can use it to align chart elements like annotations.

Alignment and the grid are very important in table design. This is the topic of the <span class="internal-link">[Table design](tag/table-design)</span> pages.

When laying out pages or when publications contain multiple visualisations, you need to consider the **reading sequence**, running from top-left to bottom-right.