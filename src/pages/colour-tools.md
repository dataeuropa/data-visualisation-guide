There are a lot of tools available for working with colours. Here is a list with some of the most useful for data visualisation.

## Online tools

The [Adobe Color Wheel](https://color.adobe.com/create/color-wheel) lets you pick 5-colour palettes in various colour harmonies. Colours can be adjusted in different colour spaces.

[](https://color.adobe.com/create/color-wheel)

The [Google Colour Picker](https://www.google.com/search?q=%23123456) lets you pick and adjust colours in different colour spaces. Also easy for converting colours from one colour space to another.

The [Data Color Picker](https://learnui.design/tools/data-color-picker.html) form Learn UI Design creates equidistant categorical, discrete and diverging colour palettes.

[ColorBrewer](https://colorbrewer2.org/) has a set of colour palettes with scientifically tested efficacy.

The [Chroma.js Color Palette Helper](https://vis4.net/palettes/#/9%7Cs%7Cf3ff82,007c92,1e4160%7Cffffe0,ff005e,93003a%7C1%7C0) is an excellent tool to generate custom perceptually uniform colour palettes.

[Viz Palette](https://projects.susielu.com/viz-palette) gives you some visualization previews for the colours you pick and signals colour blindness related issues with your palettes.

Tools for checking <span class='internal-link'>[colour contrast](colour-contrast)</span> include [color.review](https://color.review/), [contrastchecker.com](https://contrastchecker.com/) and the [APCA Contrast Calculator](https://www.myndex.com/APCA/).

## Desktop tools

The built in [Digital Colour Meter](https://support.apple.com/en-mide/guide/digital-color-meter/welcome/mac) app on Mac computers allows you to identify and copy colours on your screen to your clipboard.

![Screenshot of the Digital Colour Meter](Colour%20use%20in%20data%20visualisation%20acd08b9e488e4cd9bd518e063a86f6b7/colour-meter.png)

Source: [Digital Colour Meter](https://support.apple.com/en-mide/guide/digital-color-meter/welcome/mac)

[Sim Daltonism](https://apps.apple.com/us/app/sim-daltonism/id693112260?mt=12) is a Mac app that overlays a window over your screen to simulate different kinds of colour blindness.

![Screenshot of the Sim Daltonism window over a picture of an air balloon with different colours](Colour%20use%20in%20data%20visualisation%20acd08b9e488e4cd9bd518e063a86f6b7/simdaltonism.webp)

Source: [Sim Daltonism](https://apps.apple.com/us/app/sim-daltonism/id693112260?mt=12)

[Color Oracle](https://colororacle.org) is a colour blindness simulator for Windows, Mac and Linux.rg)

![An animated gif showing the interface of Color Oracle](Colour%20use%20in%20data%20visualisation%20acd08b9e488e4cd9bd518e063a86f6b7/colororacle.gif)

Source: [Color Oracle](https://colororacle.org)