Adding annotations to your visualisations requires enough space to place them. In responsive visualisations, annotations can become problematic on smaller screens, and they could be left out.

But visualisation tool Datawrapper offers a nice solution to this problem: annotations can be integrated in a visualisation when there is enough space, and the fallback solution on smaller screens puts the annotations below the visualisation and uses numbering to connect the annotations to the visualisation.

This is an example of a map with integrated annotations:

![Source: [blog.datawrapper.de](https://blog.datawrapper.de/better-more-responsive-annotations-in-datawrapper-data-visualizations/)](Text%20annotations%204d77570c409249378ca558ae45eb0d67/datawrapper-annotations-big.png)

Source: [blog.datawrapper.de](https://blog.datawrapper.de/better-more-responsive-annotations-in-datawrapper-data-visualizations/)

And this is the version on smaller screens:

<p class='center'>
<img src='Text%20annotations%204d77570c409249378ca558ae45eb0d67/datawrapper_annotations-small.png' alt='Source: [blog.datawrapper.de](https://blog.datawrapper.de/better-more-responsive-annotations-in-datawrapper-data-visualizations/)' class='max-400' />
</p>

Source: [blog.datawrapper.de](https://blog.datawrapper.de/better-more-responsive-annotations-in-datawrapper-data-visualizations/)

More on the responsive annotations can be found on <span class='internal-link'>[Responsiveness: adapting annotations, axes and legends](responsiveness-adapting-annotations-axes-and-legends)</span>.