[data-to-viz.com](https://www.data-to-viz.com/)

The Data to Viz gallery contains some 40 chart types, which can be browsed as a simple grid list, or as a hierarchy based on the data types needed to construct the charts.

![A browser window showing the Data to Viz homepage](Data%20visualisation%20galleries%2054f97b3d69b04dbe86cbf50ba86ab8c5/data-to-viz-home.png)

Source: Maarten Lambrechts, CC BY SA 4.0

![A diagram on Data to Viz, showing a hierarchy of chart types](Data%20visualisation%20galleries%2054f97b3d69b04dbe86cbf50ba86ab8c5/data-to-viz-classification.png)

Source: data-to-viz.com

Unique to Data to Viz is that each chart type in the collections not only has  a short description, but also a list with common mistakes associated with each chart type.

<p class='center'>
<img src='Data%20visualisation%20galleries%2054f97b3d69b04dbe86cbf50ba86ab8c5/data-to-viz-boxplot.png' alt='A screenshot of the page about boxplots on Data to Viz, with sections named About, Common Mistakes and Code' class='max-600' />
</p>

Source: data-to-viz.com

All the caveats are also collected on [a dedicated page](https://www.data-to-viz.com/caveats.html). But even more interesting is that Data to Viz is actually a portal to 3 mirror sites: each chart type has links to its sister page on

- [r-graph-gallery.com](https://r-graph-gallery.com/), which contains sample code to make the visualisation in R
- [python-graph-gallery](https://www.python-graph-gallery.com/), with Python sample code to produce the chart type
- [d3-graph-gallery](https://d3-graph-gallery.com/), with javascript and D3.js code to make the visualisation

![A screenshot of r-graph-gallery.com showing a density plot and the R code to generate it](Data%20visualisation%20galleries%2054f97b3d69b04dbe86cbf50ba86ab8c5/r-graph-gallery-density-plot.png)

R code snippet to generate a density plot. Source: [r-graphs-gallery.com](https://r-graph-gallery.com/21-distribution-plot-using-ggplot2.html)