The content of this and the other <span class='internal-link'>[pages about scrollytelling](tag/scrollytelling)</span> is based on the paper “Scrolling into the newsroom: A vocabulary for scrollytelling techniques in visual online articles”, written by members of the graphics team at the Neue Zurcher Zeiting, and to be published in the Information Design Journal (reference: *Oesch, Jonas, Manuel Roth, and Adina Renner. “Scrolling into the Newsroom: A Vocabulary for Scrollytelling Techniques in Visual Online Articles.” Information Design Journal. Forthcoming 2022.)*

You can watch the video below to see all the scrollytelling examples in action, or you can read the text and click through to check the examples yourself.

<iframe width="100%" height="450" src="https://www.youtube.com/embed/v2LkaxaVH74" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

The paper has a handy definitition of what scrollytelling is:

> Scrollytelling refers to a storytelling format in which visual and textual elements appear or change as the reader scrolls through an online article. It is more clearly defined in the negative: when readers scroll, something other than the conventional movement of a document through the viewport happens.
> 

Scrollytelling usually involves a fixed or updating visual that updates as text blocks scroll by.

![ ](Scrollytelling%200ae4533947224ed3b08305e4c650ce0d/scrollytelling.gif)

An animation illustrating scrollytelling. Source: [Russell Goldenberg](https://github.com/russellgoldenberg/scrollama)

But other techniques also exist. The paper authors analysed 50 scrollytelling articles, and identified 5 kinds of scrollytelling.

## Five scrollytelling techniques

Based on an analysis of the properties of the scrollers in a a collection scrollytelling stories, the graphics team of the Neue Zurcher Zeitung identified 5 types:

- Scrollytelling technique: <span class='internal-link'>[graphic sequence](scrollytelling-graphic-sequence)</span>
- Scrollytelling technique: <span class='internal-link'>[animated transition](scrollytelling-animated-transition)</span>
- Scrollytelling technique: <span class='internal-link'>[pan and zoom](scrollytelling-pan-and-zoom)</span>
- Scrollytelling technique: <span class='internal-link'>[moviescroller](scrollytelling-moviescroller)</span>
- Scrollytelling technique: <span class='internal-link'>[show-and-play](scrollytelling-show-and-play)</span>