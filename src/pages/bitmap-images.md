Bitmap graphics encode images as a two dimensional grid of square pixels, each storing a colour value.

<p class='center'>
<img src='File%20formats,%20dimensions%20and%20units%20d1757276a4214716bd793dd0f19ac95c/942px-Rgb-raster-image.svg.png' alt='' class='max-600' />
</p>

An enlarged view on a bitmap image of a smily face, showing the RGB values stored in 3 pixels. Source: [Gringer](https://commons.wikimedia.org/wiki/File:Rgb-raster-image.svg), CC0

The size of a bitmap image is expressed as the number of rows and columns of the image. So a 1.200 by 800 pixel image contains 1.200 rows of 800 pixels each, totalling 960.000 pixels. The resolution of the output of digital cameras, which produce bitmap images, is often expressed in megapixels, equal to 1 million pixels. So a camera that produces 2.560  x 1.920 pixels =  4.915.200 pixels = 4.9 megapixels.

For working with bitmap images, you need bitmap editing software, like Adobe Photoshop, GIMP or Microsoft Paint.

Bitmap images have clear advantages: they are ubiquitous in the online world, most software know how to deal with them, and they are easy to understand and work with.

They are also easily compressed. When an image contains large areas of the same or similar colours, compression algorithms can pack that information into a smaller file. Compression can be lossless, so that the original image can be reproduced, or compression can be lossy, in which the original image cannot be fully reconstructed from the compressed file.

## Bitmap file formats

PNG images make use of lossless compression. PNG stands for Portable Network Graphics, as the file format was developed for transferring images over the internet. PNG files are able to encode transparency.

JPG (Joint Photographic Group) images use lossy compression. The level of compression can be set when saving images from bitmap image editing software. Typically, a compression rate of 60 to 70 percent will lead to big reductions in file size, while the image quality remains relatively high and is hardly perceptible for most use cases. JPG images cannot encode transparency.

![ ](File%20formats,%20dimensions%20and%20units%20d1757276a4214716bd793dd0f19ac95c/Felis_silvestris_silvestris_small_gradual_decrease_of_quality.png)

A jpg image of a European wild cat, with decreasing compression from left to right. Source: [Michael Gäblerderivative, AzaToth](https://commons.wikimedia.org/wiki/File:Felis_silvestris_silvestris_small_gradual_decrease_of_quality.png), CC BY 3.0

GIF (Graphic Interchange Format) images are an older raster image file format. It supports only 256 colours, but does implement transparency. Online, it is a popular file format because it supports animation.

![A GIF version of the rainbow cat meme showing a cat flying through stars and generating a rainbow](File%20formats,%20dimensions%20and%20units%20d1757276a4214716bd793dd0f19ac95c/spacecat.gif)

Source: [giphy.com](https://giphy.com/gifs/stars-rainbows-poptart-BSx6mzbW1ew7K)

WebP is a newer image file format developed by Google. It supports both lossy and lossless compression, has smaller file sizes for the same quality as jpeg and png, and also supports transparency and animation. WebP is currently supported in most modern browsers.

Other bitmap image formats include RAW (the raw output of image sensors of digital cameras, or the digital negative, comparable to the ‘negative’ of a photo shot with a traditional camera) and TIFF (Tag Image File Format, sometimes used for storing the output of scanners, and a popular format for geographical raster images, like satellite imagery).