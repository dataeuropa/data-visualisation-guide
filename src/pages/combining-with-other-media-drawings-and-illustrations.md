Some data visualisation designers have developed a very recognisable style by making hand drawn visualisations. One of these is [Mona Chalabi](https://monachalabi.com/).

![A drawn line chart, with a seat belt representing the seat belt use rate over time, and a bar chart representing the fatalities per 100 million vehicular miles travelled](Chart%20junk%20and%20data%20ink%2045cd2a8ea3454bffa82b78d53ca414dc/mona-chalabi-cars.jpg)

Source: [@MonaChalabi](https://twitter.com/MonaChalabi/status/1462845944857350151)

![Drawings of teeth in mouths, with the number of white teeth proportional to the number of dentist consultations per year in different countries](Chart%20junk%20and%20data%20ink%2045cd2a8ea3454bffa82b78d53ca414dc/mona-chalabi-dentist.jpg)

Source: [@MonaChalabi](https://twitter.com/MonaChalabi/status/1452642962769383425)

Data illustrator Gabrielle Merite is adding to the same genre, with collage-like data visualisations.

![3D renderings of different sources and sinks of carbon (like forestry, electricity and buildings), with the height of the drawings proportional to the amount of CO2 emitted or sequestrated in Costa Rica in 2018 and 2050](Chart%20junk%20and%20data%20ink%2045cd2a8ea3454bffa82b78d53ca414dc/gabrielle-merite-costa-rica-carbon.jpg)

Source: [@Data_Soul](https://twitter.com/Data_Soul/status/1498688948314406916)

![A bar chart titled 'Hold me longer' in which each bar is drawn being hugged and its height is proportional to the pleasure rating tied to the duration of the hug](Chart%20junk%20and%20data%20ink%2045cd2a8ea3454bffa82b78d53ca414dc/hugs-gabrielle-merite.jpg)

Source: [@Data_Soul](https://twitter.com/Data_Soul/status/1465365173040914435)

![A collage like visualisation of the top 10 countries with the largest cumulative carbon emissions](Chart%20junk%20and%20data%20ink%2045cd2a8ea3454bffa82b78d53ca414dc/gabrielle-merite-emissions.jpg)

Source: [@Data_Soul](https://twitter.com/Data_Soul/status/1457770156189315075)

In a talk at the Outlier 2022 Conference, Robert Kosara (the researcher who coined the term “harmless junk”), highlighted this chart:

![A bar chart titled 'If cows were a country, they would be among the top greenhouse-gas emitters. The bar representing Cattle & Dairy is replaced by a cow](Chart%20junk%20and%20data%20ink%2045cd2a8ea3454bffa82b78d53ca414dc/mckinsey-cow.jpg)

Source: [@McKinsey](https://twitter.com/mckinsey/status/1457365985971908616)
