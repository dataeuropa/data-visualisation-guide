If you read the pages on <span class="internal-link">[the grid](the-grid)</span> and <span class="internal-link">[arrangement](arrangement-and-reading-direction)</span>, you already know that you can give more prominence to some elements in publication or in a visualisation, simply by putting it in the top left corner of the design. This is an example of creating **visual hierarchy**: telling the reader what to look at first, what to look at after that, and what to look at in the end.

Many ways of creating visual hierarchy in a publication and in a visualisation design exist:

- position element in the <span class="internal-link">[x and y dimensions](visual-hierarchy-x-y-positioning)</span>
- layering elements on top of each other <span class="internal-link">[in the z dimension](visual-hierarchy-z-positioning)</span>
- using <span class="internal-link">[the size](visual-hierarchy-sizing)</span> of elements
- using <span class="internal-link">[colour](visual-hierarchy-colour)</span>
- highlight elements by <span class="internal-link">[enclosing them](visual-hierarchy-enclosure)</span>
