In animated transitions, a fixed graphic animates to a new state when a new text block is scrolled into view next to it. While graphic sequences simply fade graphics in and out, animated transitions show the intermediate, interpolated states between graphics.

![A four panel illustration of how animated transitions work, with the text 'Scrolling... triggers the animation morphing into a new graphic'](Scrollytelling%200ae4533947224ed3b08305e4c650ce0d/scrollytelling-animated-transitions.png)

Source: Oesch, Jonas, Manuel Roth, and Adina Renner. “Scrolling into the Newsroom: A Vocabulary for Scrollytelling Techniques in Visual Online Articles.” Information Design Journal. Forthcoming 2022

Animated transitions are most used for more abstract graphics like data visualisations. They are excellent devices to show different facets of a data set and to show evolution of processes over time.

In animated transitions, the animations can be fully controlled by the scroll position or can only be triggered by the scroll position, after which the animation runs independently from the scroll.

An example of the former, with animations fully tied to the scroll position, is “A visual introduction to machine learning”

![Screenshot of A visual introduction to machine learning](scrollytelling-machine-learning.png)

Source: [A visual introduction to machine learning](http://www.r2d3.us/visual-intro-to-machine-learning-part-1/)

An example that uses triggers to start animations is "Why Budapest, Warsaw and Lithuania split themselves in two"

![A map of Europe and some explanation screenshotted form Why Budapest, Warsaw and Lithuania split themselves in two](scrollytelling-eu-regions.jpg)

Source: [Why EU Regions are Redrawing Their Borders](https://pudding.cool/2019/04/eu-regions/)

Another example of this technique is applied in "East-West Exodus: The Millions Who Left"

![Screenshot of an animated chart from East-West Exodus: The Millions Who Left](scrollytelling-zeit.jpg)

Source: [East-West Exodus: The Millions Who Left](https://www.zeit.de/politik/deutschland/2019-05/east-west-exodus-migration-east-germany-demography)