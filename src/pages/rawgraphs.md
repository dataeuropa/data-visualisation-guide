Free, web app

[rawgraphs.io](https://rawgraphs.io/)

RAWGraphs is chart template based web app with an easy drag and drop interface. It was designed to make many less common charts accessible to non coders, and as a result it has a chart template gallery unmatched by other tools.

![ ](Data%20visualisation%20design%20in%20practice%202%20tools%20208f06b06b0f4b21ad8ecf3047f02ce0/rawgraphs-gallery-new.png)

The RAWGraphs chart template gallery. Source: Maarten Lambrechts, CC BY SA 4.0

Output formats are static (PNG and SVG), and styling options in RAWGraphs are limited. The exported SVG files are meant to be finished in vector editing software like Illustrator. The RAWGraphs website includes a [gallery of charts made with the tool](https://rawgraphs.io/gallery), and a [list of tutorials](https://rawgraphs.io/learning).