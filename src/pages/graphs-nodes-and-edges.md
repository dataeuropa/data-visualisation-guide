You and your friends on Facebook or any other social network constitute a network (also called a graph): you are connected to your friends, and they themselves are connected to other people in the network.

In graph data language, you are a “**node**” in the social media graph. The things that connect you to other nodes in the graph (like friend requests, likes, …) are called **edges**. So graph data is a collection of nodes which are connected to each other through edges. 

Graph data has become more and more important, and the techniques to visualise this kind of data have also grown. So it is worthwhile to dig a little deeper into chart types that you can use to visualise this special type of data.