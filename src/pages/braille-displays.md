The most popular screen readers can do more than reading text out loud: they also offer braille output. Braille is a tactile writing system developed for blind and visually impaired people. A braille letter consist of 6 dots arranged in two columns and three rows (called a braille cell), in which each dot can have 2 states: embossed or not embossed. Letters and punctuation are different combinations of embossed and non-embossed dots.

The following braille characters form the word “braille”:

## ⠃⠗⠁⠊⠇⠇⠑

This is an example of uncontracted braille, in which each letter is mapped one on one to a braille character. In contracted braille, single braille characters can represent multiple letters. The mapping from letters to braille characters is also different from language to language. English braille is different from French braille, for example. Braille also has an extended version that uses 8 instead of 6 dots, in which each cell can represent all printable ASCII characters (lower case a-z, upper case A-Z, numbers 0-9 and punctuation).

Screen readers traversing a web page or a computer operating system translate this input to speech but also to braille. In order to read the braille, it needs to be displayed on a **refreshable braille display**. This is a device that receives the output of a screen reader and displays the received text as braille through little pins raised through holes on a flat surface.

![ ](Braille,%20data%20sonification%20and%20data%20physicalisatio%203da60749f91a44b48ddbe910563ea247/brailliant_bi_40x_closedup_-_dsc06735-lr.jpg)

A refreshable braille display, with 40 8-dot braille cells. The 8 keys at the top of the display are used to type in braille characters. Source: [humanware.com](https://store.humanware.com/hus/brailliant-bi-40x-braille-display.html)

All information accessible to screen readers is also accessible through a braille display, and the keys on the display are used to navigate web pages and the operating system.