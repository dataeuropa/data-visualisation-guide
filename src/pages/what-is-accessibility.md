Designers conceive and construct the world we live in: architects draw buildings, product designers draw chairs, bikes and other tools we use every day, and web designers determine how websites should look and function. These designers, in the broad sense, design the interface of how we interact with the world and the society around us.

This is a big challenge, because people are very diverse, and designing an interface that works for everyone is difficult. **Accessibility** (often shortened to a11y: “a”, then 11 letters, and then a “y”) is the discipline in design that tries to match those interfaces to all, or at least to as many people as possible, and to maximise the match of people’s interactions with the world around them.

Accessibility has long been viewed from the perspective of disabilities as a personal attribute: experiences and interactions were designed so that people with a lack of a physical ability resulting from an impairment could still perform an activity in the manner of, or within the range considered for, a human being. Buildings are made accessible to wheel chairs, movies are subtitled so that deaf people can watch them and websites are designed so that they can be used by the blind.

A more modern look on accessibility goes beyond the aspect of impairments, and takes into account the whole **context** in which humans interact with the world around them. A mismatch between a persons abilities and an interaction interface can not only be a result of a **physical** impairment, but also of their **cognitive** abilities, some **temporary** disabilities (like injuries), or through characteristics of the **environment** people find themselves in.

Microsoft’s [Inclusive Design](https://www.microsoft.com/design/inclusive/) methodology embraces this wider approach to disabilities and accessibility. The following diagram illustrates perfectly how disabilities are not personal attributes, but can also be temporary and situational.

<p class='center'>
<img src='a11y%208dc3351bebd74fdcbe0d2dd06f4d4024/persona-spectrum-microsoft.png' alt='Line drawings of different kinds of disabilities' class='max-600' />
</p>

Source: [Microsoft Inclusive Design Toolkit](https://download.microsoft.com/download/b/0/d/b0d4bf87-09ce-4417-8f28-d60703d672ed/inclusive_toolkit_manual_final.pdf)

This approach shows that accessibility is not a niche discipline, and that it is not just a matter of  how to design things so that people with permanent impairments can also use a product or service. A tool that can be used single handedly does not only benefit people with amputated upper extremities, but also benefits people with temporary arm or hand injuries, and parents carrying sleeping newborns in their arm.

The US Census Bureau estimates that 26.000 people in the US suffer a permanent loss of upper extremities each year, while the yearly number of people with temporary injuries of hands and arms was estimated at 13 million. On top of that, the number of people with situational impairments amounts to 8 million. So the number of people benefitting from a design that allows single hand use of a tool does not run in the thousands, but in the millions.

Similarly, designing with the constraint of making screens usable for people with visual impairments for example, resulted in high-contrast screen settings. These have proved to be very handy for anyone trying to read something from a screen in bright sunlight.