Not all data points on a chart are as important as the other ones. Adding data labels to only the data points that have the highest relevance to that story can make these points stand out. On top of that, there might not be enough space to label every data point on a visualisation.

In time series visualisations, the most important values are often the most recent ones, so it make sense to label those.

![A line chart titled 'European Union slowly on its way to greener heating and cooling', with the most recent data points for the highest (Sweden) and lowest (Ireland) values labelled, as well as the value of the EU average](Text%20annotations%204d77570c409249378ca558ae45eb0d67/Chart-no_annotations2x.png)

Source: Maarten Lambrechts, CC-BY-SA 4.0

Notice that in the chart above, the category labels (the names of the countries) are also added sparingly: only the countries mentioned in the chart description above the chart are added. The lines of all other countries are in light grey and their category and data labels are not displayed.

Additional text on an axis can also help to explain what it means when data points are situated more to the top, right, left or right of a chart. 

![Beeswarm plots showing how risky it is to live in different countries](Text%20annotations%204d77570c409249378ca558ae45eb0d67/axis-labels-guardian.png)

Source: [theguardian.com](https://www.theguardian.com/global-development/datablog/2016/apr/25/where-is-the-riskiest-place-to-live-floods-storms)

Instead of labelling the x axis with numerical values, “less risk” and “more risk” labels are used, with arrows pointing to the axis ends. Notice how the connector lines for the annotations are arrows that point to the notes rather then to the subjects of the annotations.

![A scatter plot showing family income on the y axis, and the net impact of new tax rules on family income on th x axis. Text annotations explain some patterns visible in the data](Text%20annotations%204d77570c409249378ca558ae45eb0d67/nyt-scatterplot-reveal-details.png)

Notice the labels added to both sides of the vertical zero line. Source: [What the Tax Bill Would Look Like for 25,000 Middle-Class Families](https://www.nytimes.com/interactive/2017/11/28/upshot/what-the-tax-bill-would-look-like-for-25000-middle-class-families.html), nytimes.com

The same technique can be used on colour legends too. In the example below, text labels are added above the legend, in addition to the numerical values below it.

![2 world maps showing the impact on droughts under 2 emission scenarios. The labels above the legend read 'More drought-prone' (left) and 'Less drought-prone' right](Text%20annotations%204d77570c409249378ca558ae45eb0d67/legend-text-labels.png)

Source: [Floods, droughts and heat waves herald a changing climate](https://datatopics.worldbank.org/sdgatlas/goal-13-climate-action/), Sustainable Development Goals Atlas 2020, World Bank