Below are some examples of videos that use data visualisation in different ways.

<iframe width="100%" height="450" src="https://www.youtube.com/embed/sv0dQfRRrEQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

_Source: [How American conservatives turned against the vaccine](https://www.vox.com/22947498/partisan-pandemic-polarization-coronavirus-vaccine), vox.com_

In the series of videos called “[Data Crunch](https://www.ft.com/ft-data-crunch)”, 2 data journalists of the Financial Times have a conversation and try to answer questions with hand drawn data visualisations.

![Source: [FT Data Crunch](https://www.ft.com/ft-data-crunch), ft.com](Combining%20visualisations%20with%20other%20media%203195c842e70245d394d6c9e019d7165e/ft-data-crunch.png)

_Source: [FT Data Crunch](https://www.ft.com/ft-data-crunch), ft.com_

Film maker [Neil Halloran](https://www.youtube.com/c/neilhalloran) creates data driven documentaries of cinematographic quality.

<iframe width="100%" height="450" src="https://www.youtube.com/embed/bIAF7kBbGKk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

_Source: [Estimating Deaths in a Nuclear War](https://www.youtube.com/watch?v=bIAF7kBbGKk), Neil Halloran_

Information designer [Duncan Geere](https://www.duncangeere.com/) experiments with a plotter to tell the story of a dataset.

<iframe width="100%" height="450" src="https://www.youtube.com/embed/TzVEGaFW74I" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

_Source: Boom & Bust: A History of Oil Prices and Consumption, [Duncan Geere](https://blog.duncangeere.com/introducing-dataplotter/)_