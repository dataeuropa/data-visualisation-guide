Storytelling is all about making a connection with the reader. And there are some patterns that you can use to make that connection directly.

The first of these patterns is to translate big and abstract numbers and amounts to things that touch upon the daily lives of people. By **concretising** these big numbers, you make them more relatable to people.

The Reuters graphics team excels at this technique and has published some very nice examples. For example, in [The messy business of sand mining explained](https://graphics.reuters.com/GLOBAL-ENVIRONMENT/SAND/ygdpzekyavw/) they compare the amount of cement used over 10 years with the Great Pyramid of Giza:

![The amount of cement used in China in 10 years and in the US in 115 years visualised as a pyramid and compared with the Great Pyramid of Giza](Patterns%20for%20data%20driven%20stories%2034fe0220a7d64297ae6ccf534303e18f/cement-concretising-reuters-1.png)

Source: [The messy business of sand mining](https://graphics.reuters.com/GLOBAL-ENVIRONMENT/SAND/ygdpzekyavw/), graphics.reuters.com

Of course, this works less for readers who never saw the Great Pyramid in real life. But the article contains another example of concretising, and this time the comparison is made to something everyone is familiar with: the size of a human body.

![The global daily and yearly per capita demand for sand visualised as a heap of sand and compared to a human body](Patterns%20for%20data%20driven%20stories%2034fe0220a7d64297ae6ccf534303e18f/sand-percapita-concretise-reuters.png)

Source: [The messy business of sand mining](https://graphics.reuters.com/GLOBAL-ENVIRONMENT/SAND/ygdpzekyavw/), graphics.reuters.com

In another piece they use a comparison with islands to make the size of giant iceberg A68a more relatable.

![The silhouette of Iceberg A68a compared to the silhouettes of various islands](Patterns%20for%20data%20driven%20stories%2034fe0220a7d64297ae6ccf534303e18f/iceberg-islands-reuters.png)

Source: [World’s biggest iceberg heads for disaster](https://graphics.reuters.com/CLIMATE-CHANGE/ICEBERG/yzdvxjrbzvx/index.html), reuters.graphics.com

Concretising can also be applied to black boxes of models. Take for example this spreadsheet containing formulas to model Covid-19 aerosol transmission.

![Screenshot of a Google sheet showing the parameters of a model for Covid-19 aerosol transmission](Patterns%20for%20data%20driven%20stories%2034fe0220a7d64297ae6ccf534303e18f/elpais-model.png)

Source: [COVID Airborne Transmission Estimator](https://cires.colorado.edu/news/covid-19-airborne-transmission-tool-available), Cooperative Institute for Research in Environmental Sciences, University of Colorado Boulder

The spreadsheet calculates the probability of people getting infected taking into account room characteristics (dimensions, amount of ventilation, ...) and people behaviour (masking, crowding, movement). But for most people a spreadsheet like that is impossible to get any useful information out.

That is why Spanish newspaper El Pais turned the model into something concrete people can relate to: they drew the rooms and the people in it to show how covid-19 spreads under different circumstances.

![A drawing of a room with masked people in it, and ventilation bringing in fresh air](Patterns%20for%20data%20driven%20stories%2034fe0220a7d64297ae6ccf534303e18f/elpais-covid-spread.png)

Source: [A room, a bar and a classroom: how the coronavirus is spread through the air](https://elpais.com/especiales/coronavirus-covid-19/a-room-a-bar-and-a-class-how-the-coronavirus-is-spread-through-the-air/), elpais.com

This approach paid off for the newspaper. The article, originally written in Spanish but later translated into English, went viral and became one of the most visited pages of the paper’s website.

Another engaging pattern is, instead of showing the data directly, to **ask the reader what they think the data looks like** first. The first article to use this pattern was [You Draw It: How Family Income Predicts Children’s College Chances](https://www.nytimes.com/interactive/2015/05/28/upshot/you-draw-it-how-family-income-affects-childrens-college-chances.html). The article asks you to draw what you think the relationship between parents’ income and the share of children attending college looks like.

![An empty chart canvas with the title 'Draw your line on the chart below'](Patterns%20for%20data%20driven%20stories%2034fe0220a7d64297ae6ccf534303e18f/youdrawit-nyt-start.png)

Source: [You Draw It: How Family Income Predicts Children’s College Chances](https://www.nytimes.com/interactive/2015/05/28/upshot/you-draw-it-how-family-income-affects-childrens-college-chances.html), nytimes.com

After drawing a curve and submitting it, you are presented with the real data and with a visualisation of how other people have drawn the curve.

![The same chart as above, but with a drawn line and a comparison with the real numbers and with how other people drew the line](Patterns%20for%20data%20driven%20stories%2034fe0220a7d64297ae6ccf534303e18f/youdrawit-nyt-result.png)

Source: [You Draw It: How Family Income Predicts Children’s College Chances](https://www.nytimes.com/interactive/2015/05/28/upshot/you-draw-it-how-family-income-affects-childrens-college-chances.html), nytimes.com

This “You draw it” pattern has become quite popular, and has been used by the Office for National Statistics from the United Kingdom too. In the chart below, you can draw a curve for how you think the average house price evolved over time in the UK.

<iframe src="https://www.ons.gov.uk/visualisations/dvc445/ukhpi/index.html" style="border: none;" width="600px" height="480px"></iframe>

Embedded from [You draw the charts: 60 years of change](https://www.ons.gov.uk/peoplepopulationandcommunity/healthandsocialcare/healthandlifeexpectancies/articles/youdrawthecharts60yearsofchange/2017-10-24), ons.gov.uk

Finally, a very powerful pattern is to **show the humans behind the dots**. In this pattern, visual marks representing humans reveal some kind of information about the person(s) they represent. It is much easier to empathise with a person than with a dot on a chart, and revealing some personal details reminds the reader of the fact that the visualised data represents real people like themselves.

This technique is closely related to the use of <span class='internal-link'>[anthropographics](anthropographics)</span>.

![ ](Patterns%20for%20data%20driven%20stories%2034fe0220a7d64297ae6ccf534303e18f/stories-behind-a-line.png)

Stories Behind a Line tells the story of how seven migrants reached Europe after travelling long distances. The migrants are anonymous, but revealing their initials, their age and city and country of origin (see the top left), makes the stories a lot more personal. Source: [storiesbehindaline.com](http://www.storiesbehindaline.com/)

In May 2020, the Covid-19 death toll in the US reached 100.000 people. The graphics desk of the New York Times wanted to represent the number in a way that conveyed both the vastness of this number and the variety of the lives lost. They decided to use a text only approach, listing some of the deceased people, with their age, the place they lived and a little detail about their lives and what they meant for their community.

![A front page of the New York Times, with the names and obituaries of people who died from Covid-19](Patterns%20for%20data%20driven%20stories%2034fe0220a7d64297ae6ccf534303e18f/NYT-front-page-05-24-20-superJumbo-v2.jpg)

Source: [The Project Behind a Front Page Full of Names](https://www.nytimes.com/2020/05/23/reader-center/coronavirus-new-york-times-front-page.html), nytimes.com

Online, you can scroll through the names of the people, and the use of little silhouettes give you a sense of what was lost,

![Screenshot of the online version of the article, showing lots of silhouettes, with some of them labelled with a name and a description of the person](Patterns%20for%20data%20driven%20stories%2034fe0220a7d64297ae6ccf534303e18f/incalculable-loss-nyt.png)

Source: [An Incalculable Loss](https://www.nytimes.com/interactive/2020/05/24/us/us-coronavirus-deaths-100000.html), nytimes.com

The scatterplot in [What the Tax Bill Would Look Like for 25,000 Middle-Class Families](https://www.nytimes.com/interactive/2017/11/28/upshot/what-the-tax-bill-would-look-like-for-25000-middle-class-families.html) reveals data about the household and financial situation of the families represented on it. 

![A screenshot of a scatter plot, with a popup revealing the details of one of the many plotted families](Patterns%20for%20data%20driven%20stories%2034fe0220a7d64297ae6ccf534303e18f/household-incomes-nyt.png)

Source: [What the Tax Bill Would Look Like for 25,000 Middle-Class Families](https://www.nytimes.com/interactive/2017/11/28/upshot/what-the-tax-bill-would-look-like-for-25000-middle-class-families.html), nytimes.com

A similar approach was used in the waffle charts in [Beyond hunger: ensuring food security for all](https://datatopics.worldbank.org/sdgatlas/goal-2-zero-hunger/): hovering over one of the squares reveals details about the household composition, education level, level of income and risk of food insecurity of all households that participated in a survey:

![A waffle chart with a popup revealing the details about a specific family hovered over by the user](Patterns%20for%20data%20driven%20stories%2034fe0220a7d64297ae6ccf534303e18f/waffle-humans-behind-the-dots.png)

Source: [Beyond hunger: ensuring food security for all](https://datatopics.worldbank.org/sdgatlas/goal-2-zero-hunger/), Sustainable Development Goals Atlas 2020, World Bank