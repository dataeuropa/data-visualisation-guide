The fast pass Chartability audit involves 5 steps to quickly check a handful of access barriers quickly.

The first step is **visual testing**. This entails calculating the contrast ratios between text and data marks with their background colour, the use of colour blind safe colours and a check on the font sizes used.

The second step is **keyboard probing**. Interactive elements should be reachable and controllable using a keyboard alone, and this keyboard navigation and activation shouldn’t take too much time or effort.

Related to keyboard probing is **screen reader inspecting**. The key aspect of this third step is wether a text description of what is visible in a chart is provided and can be read out by the screen reader. But other elements in a visualisation should also be accessible to screen readers, like data values, category labels and axes titles. Furthermore, if a chart is interactive, screen readers should be able to recognise and announce the interactive features.

In the next step, **checking cognitive barriers**, the reading level and clarity of the text is evaluated. This step also involves checking wether or not the main message the chart is trying to convey is also present in written text (like in the chart title for example). On top of that, striking patterns in the data should be explained with callouts or annotations: it shouldn’t be up to the user to spot ant try to explain these patterns.

The fifth and last step is called **evaluating context**. This involves checking if the design and styling of a visualisation respects user settings like dark mode and high contrast mode, and checking whether alternative ways of accessing the data (with a simpler chart, or with a table for example) are provided.
