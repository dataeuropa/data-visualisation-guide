“Chart junk” is an infamous concept in data visualisation. Since the coining of the term in the early eighties, it has led to the creation of a school of minimalistic data visualisation design and the term has found its way into the language of both visualisation practitioners as well as researchers. In the past years, a <span class="internal-link">[counter movement](chart-junk-and-data-ink-counter-movement)</span>, trying to abolish the use of the word and rejecting the <span class="internal-link">[minimalistic visualisation design style](chart-junk-and-data-ink-minimalistic-vs-rich-design)</span>, has come to grow in popularity.

## Origins

The term chart junk was first used by [Edward Tufte](https://www.edwardtufte.com/) in his book “The Visual Display of Quantitative Information”.  Tufte, with a background in statistics and political science, criticised  some practices of data visualisation designers in that time, like adding patterns to the data elements in a chart, and making gridlines too visually present.

To overcome these issues, in the same book Tufte introduced the concept of “data-ink”. The data ink ratio is the proportion of the ink in a visualisation that is used to display the data divided by the total amount of ink used. According to Tufte, non-data-ink is chart junk and should be removed, within reason.

But Tufte also mentions another category of chart junk, which he compares to the “Big Duck”, a famous duck shaped building in New York, built by a duck farmer and used as a shop to sell ducks and duck eggs.

![Picture of the Big Duck building](Chart%20junk%20and%20data%20ink%2045cd2a8ea3454bffa82b78d53ca414dc/1280px-Big_Duck_2018_05.jpg)

Big Duck. Source: [Mike Peel](https://en.wikipedia.org/wiki/File:Big_Duck_2018_05.jpg), CC-BY-SA-4.0

For Tufte, the whole structure of the building is decoration, and some visualisations can be classified as Big Duck graphics:

> When a graphic is taken over by decorative forms or computer debris, when the data measures and structures become Design Elements, when the overall design purveys Graphical Style rather than quantitative information, then that graphic may be called a *duck* in honor of the duck-form store, “Big Duck”.
> 

In part, Tufte is criticising the emerging field of computer generated visualisations. Making graphics with computers was new, and as a result there was a lot of experimentation, which not always produced effective charts.

But he was also criticising designers being creative in adding effects and decorations to visualisations, like 3D perspective and other decoration. According to Tufte, “graphics do not become attractive and interesting through the addition of ornamental hatching and false perspective to a few bars”.