In [Inaugurate](http://inauguratespeeches.com/), the content of the speeches of 12 US presidents are compared and analysed for the occurrence of specific terms. Tooltips reveal parts of the speeches that contain the terms.

![A screenshot of Inaugurate, with images of past US presidents and outlines of their inauguration speeches below them](Visualising%20text%2073ea05fcce1b4ee5939cd29821ddd468/inaugurate.png)

Source: [Inaugurate](http://inauguratespeeches.com/)

When the focus is on topics and emotions in text, a technique that is sometimes used is to highlight parts of the analysed text on “little pages” and pull some quotes from the text for illustration.

![A visual comparison of speeches by Donald Trump and Hillary Clinton](Visualising%20text%2073ea05fcce1b4ee5939cd29821ddd468/compare-text-clinton-trump-nytimes.png)

Source: [‘Stronger Together’ and ‘I AmYour Voice’ — How the Nominees’Convention Speeches Compare](https://www.nytimes.com/interactive/2016/07/29/us/elections/trump-clinton-pence-kaine-speeches.html?_r=0)

More examples of examples of text visualisation techniques can be found in the [Text Visualization Browser](https://textvis.lnu.se/), and in the replies to [this tweet by Tiziana Alocci](https://twitter.com/Altiziana/status/1493534991954989057)