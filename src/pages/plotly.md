Free, open source

[plotly.com/graphing-libraries](https://plotly.com/graphing-libraries/)

Plotly is a library to make interactive charts and maps in wide range of programming languages, including Python, R and JavaScript. It is a chart template based library, with some less common and specialised chart types, including 3D visualisations.

![ ](Data%20visualisation%20design%20in%20practice%202%20tools%20208f06b06b0f4b21ad8ecf3047f02ce0/plotly-chart-types.png)

The available chart types in the [Plotly Chart Studio](https://chart-studio.plotly.com/), an online platform based on the Plotly Javascript library. Source: Maarten Lambrechts, CC BY SA 4.0

The interactivity of Plotly charts includes tooltips, filtering data, zooming and rotating (for 3D charts). Plotly can also have a button to download the chart as SVG, PNG, JPG and WebP files.