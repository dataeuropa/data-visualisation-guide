Just like analysing data driven stories can identify <span class='internal-link'>[story genres](tag/narrative-visualisation-genres)</span> and <span class='internal-link'>[interactive story structure](tag/interactive-story-structures)</span>, it can also reveal patterns that make stories engaging. These patterns are hooks into the message you want to convey with visualisation: they grab the attention of readers and keep them wanting to know more

These patterns are very diverse. Some **patterns inherent to the data** have a natural tendency to trigger curiosity and engagement. Specific **views on the data**, and changes in that view, are also potential atttention grabbers. Then there are **narrative patterns** to create tension and a story arc, and finally, probably the most powerful technique, is **directly appealing to readers**, or surprsing them.

The patterns are not “techniques” in the sense that you can just push a button in some software tool to use them: they have to be created. They are independent from the medium (web, print, video, ...) but some patterns may not apply to certain media.

## Story patterns in data

Some patterns in data have a natural storytelling capacity. If one of these patterns is present in your data, chances are that your story or message will be built around it. Visualisations should show and highlight these patterns in the clearest way possible.