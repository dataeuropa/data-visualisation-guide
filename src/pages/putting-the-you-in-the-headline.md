In online media, a powerful way to relate data to people and their is to **let readers find themselves in the data**. With search and filter functionality, the readers can find and visualise the data most relevant to them (like the local statistics for their municipality, the wages paid in the sector of the economy they are working, or key numbers for their age group).

This technique can be called “Putting the “you” in the headline” because that is what many of the titles of these interactives published in media have in common: they address readers directly, and invite them to start interacting to find their own data.

One example of this technique is [population.io](https://population.io/). Instead of putting “you” in the headline the site is putting “my” and “I” in the headline, but the interface is boldly inviting you to provide some data about yourself.

![Screenshot of the interface of population.io, with a headline saying What's my place in the world population? How long will I live?](Journalistic%20techniques%20for%20data%20storytelling%208bdd09bf88074238b1fe53b3a2116e1e/population-interface.png)

Source: [population.io](https://population.io/)

After entering your data, the site returns some personal statistics about yourself in the world population and the population of the country you live in.

![The output of population.io showing an area chart and some numbers](Journalistic%20techniques%20for%20data%20storytelling%208bdd09bf88074238b1fe53b3a2116e1e/population-result.png)

Source: [population.io](https://population.io/)

The Office for National Statistics of the UK put the “you” in the headline in [How health has changed in your local area](https://www.ons.gov.uk/peoplepopulationandcommunity/healthandsocialcare/healthandwellbeing/articles/howhealthhaschangedinyourlocalarea/2022-03-18). After selecting an area of interest, the text and the visualisations of the article are updated and center around the selected area.

![Screenshot of an interactive article with the headline 'Explore data in your area' above a big search field with dropdown](Journalistic%20techniques%20for%20data%20storytelling%208bdd09bf88074238b1fe53b3a2116e1e/ons-health-local-area.png)

Source: [How health has changed in your local area](https://www.ons.gov.uk/peoplepopulationandcommunity/healthandsocialcare/healthandwellbeing/articles/howhealthhaschangedinyourlocalarea/2022-03-18), ons.gov.uk

The New York Times managed to put the “you” in the headline twice in a single piece. The interactive in [How Much Hotter Is Your Hometown Than When You Were Born?](https://www.nytimes.com/interactive/2018/08/30/climate/how-much-hotter-is-your-hometown.html) asks you for your birth year and hometown to not only connect you to your age group, but also to a geographical place you care about.

![Screenshot of the interface of the 'How Much Hotter Is Your Hometown Than When You Were Born?' article. The interface has an input for looking up your hometown and an input for selecting your birth year.](Journalistic%20techniques%20for%20data%20storytelling%208bdd09bf88074238b1fe53b3a2116e1e/howmuchhotter-nyt-result.png)

Source: [How Much Hotter Is Your Hometown Than When You Were Born?](https://www.nytimes.com/interactive/2018/08/30/climate/how-much-hotter-is-your-hometown.html), nytimes.com

After providing this information, you are presented with a personalised visualisation.

![A line chart with the projected number of hot days for the selected town is shown](Journalistic%20techniques%20for%20data%20storytelling%208bdd09bf88074238b1fe53b3a2116e1e/howmuchhotter-nyt.png)

Source: [How Much Hotter Is Your Hometown Than When You Were Born?](https://www.nytimes.com/interactive/2018/08/30/climate/how-much-hotter-is-your-hometown.html), nytimes.com

These Put the “you” in the headline interactives can be really powerful, engaging and popular. But the amount of information the user needs to provide, and the ease with which this happens are critical for their success. If the amount of information to be entered is too large, or when the user interface is confusing or not easy to use, users will disengage very quickly.

For this reason, the article [The Best and Worst Places to Grow Up: How Your Area Compares](https://www.nytimes.com/interactive/2015/05/03/upshot/the-best-and-worst-places-to-grow-up-how-your-area-compares.html), tried a novel approach to make it easier for people to find the data for the area they live in. The main interface of the interactive article is a map, with a search box to find a particular county. The map is centered on New York (this is a New York Times article), but if users agrees to share their location through the browser, the article zooms in to the location of the user automatically.

![ ](Journalistic%20techniques%20for%20data%20storytelling%208bdd09bf88074238b1fe53b3a2116e1e/places-to-grow-up-nytimes.png)

The browser requesting to share the user’s location. Source: [The Best and Worst Places to Grow Up: How Your Area Compares](https://www.nytimes.com/interactive/2015/05/03/upshot/the-best-and-worst-places-to-grow-up-how-your-area-compares.html), nytimes.com

After sharing your location, the map zooms in to your county (if you are in the US), so you can consult the numbers for your county. But it doesn’t stop there: the text of the article adapts itself, and puts your county up front. The text mentions the numbers for your county and even compares it to neighbouring counties and other counties in the same state.

Interesting to note here is that on desktop screens, users can zoom out to see the whole map of the US. But on mobile phone, this option is disabled: users can only share their location or search for a county to let the map focus on a single county.

<p class='center'>
<img src='Journalistic%20techniques%20for%20data%20storytelling%208bdd09bf88074238b1fe53b3a2116e1e/places-to-grow-up-mobile.png' alt='A simulation of the article on a mobile phone. As a user you can only look up a county' class='max-400' />
</p>

On mobile screens, the interactivity of the map is limited to just searching for a county. Source: [The Best and Worst Places to Grow Up: How Your Area Compares](https://www.nytimes.com/interactive/2015/05/03/upshot/the-best-and-worst-places-to-grow-up-how-your-area-compares.html), nytimes.com

So on mobile phones, the article is clearly violating <span class='internal-link'>[the information-seeking mantra](the-information-seeking-mantra)</span>, as one of the authors of the article, Gregor Aisch, explained himself during a conference talk.

![A slide from a slide deck with the text 'Details first, no zoom and filter, overview on desktop only. Mobile News Visualization Reality'](Journalistic%20techniques%20for%20data%20storytelling%208bdd09bf88074238b1fe53b3a2116e1e/aisch-mantra.png)

Source: [Data Visualizations and the News](https://vimeo.com/182590214), Gregor Aisch at [Information+ Conference 2016](https://informationplusconference.com/2016/)