The title above the chart below reads “Nowhere in Europe more unemployed households than in Wallonia and Brussels”.

<p class='center'>
<img src='More%20pitfalls%20in%20statistics%20980beb010f984cd49c83ec8dab6dae6e/tijd-regios-landen.jpg' alt='A dot plot titled "Nergens in Europa meer werkloze gezinnen dan in Wallonië en Brussel" ("More unemployed families in Wallonia and Brussels than anywhere in Europe")' class='max-600' />
</p>

Source: [tijd.be](https://www.tijd.be/)

The chart compares region level data for Wallonia and Brussels to the country averages of EU member states. Here the issue is not so much the smaller population sizes of these regions compared to the population of countries (Wallonia has a higher population than several EU member states), but this chart is comparing apples to oranges.

The country averages are hiding a wide range of unemployment rates of their regions. When apples are compared to apples, and the numbers for Wallonia and Brussels are compared to the other EU regions, the claim in the title of the chart is no longer valid: many EU regions have a lower employment rate  than Brussels and Wallonia.

![A dot plot showing the unemployment rate in European regions](More%20pitfalls%20in%20statistics%20980beb010f984cd49c83ec8dab6dae6e/employment-rate-eurostat.png)

Source: [Regions and Cities Illustrated](https://ec.europa.eu/eurostat/cache/RCI/#?vis=nuts1.labourmarket&lang=en), Eurostat