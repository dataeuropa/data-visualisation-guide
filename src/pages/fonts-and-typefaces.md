The words "font" and "typeface" used to mean different things, but today the two terms are used for the same thing: a set of letters sharing the same design. But a font (or a typeface) can have many “variants”, like italic, small caps and bold.

Google Fonts calls variants “styles”. Below are the 12 available styles of the Roboto font.

<p class='center'>
<img src='Typography%20and%20the%20design%20of%20text%20elements%203d739b7b83f2405290637ce3d7a4a814/roboto.png' alt='The text "Almost before we knew it, we had left the ground" in the Roboto font with increasing font weights' class='max-600' />
</p>

Source: [fonts.google.com/specimen/Roboto](http://fonts.google.com/specimen/Roboto)

The numbers in the variant names in the image above refer to the weight of the variant. The higher the weight of a font variant the thicker the characters. A weight of 400 is the regular width, while 700 is the weight for bold fonts.

Some fonts will have many variants, with many weights and combinations of weights with italic variants. Other fonts have only one, or a few, variants. If you want to create <span class='internal-link'>[visual hierarchy](tag/visual-hierarchy)</span> with the weights of your font, pick a font with at least 2 and preferably 3 or more weights.