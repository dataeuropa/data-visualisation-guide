It can be a big challenge to make bigger, more complex and interactive visualisations work on small screens, even when considering all the above strategies. Instead of scaling the visualisation down and reduce the number of interactive controls, zoomed in parts of the visualisation in a certain state (with data highlighted or filtered) can be annotated to explain key parts of the bigger visualisation. The full visualisation can be removed, or can be shown with a mini map (a mini map is a small copy of the full visualisation, highlighting the part that is currently zoomed in on).

An example of this technique is used in The Evolution of Trade Agreements. It shows the web of trade agreements between countries in a radial chart with additional controls and smaller visualisations to the side. 

![Screenshot of the The Evolution of Trade Agreements, with a central radial visualisation, an intro text and legends on the left and a chart and a table on the right](Responsiveness%20and%20data%20visualisation%20for%20small%20sc%20bfcc7f2b3f63483d9213104e4137aec4/ftavis-full.png)

Source: [ftavis.com](http://ftavis.com)

The visualisation and the interactive controls are not suitable for smaller screens, and are not displayed on these screens. Instead, screenshots of parts of the visualisation are provided, accompanied by a little explanation, in a section called “Selection of findings and data stories”.

<p class='center'>
<img src='Responsiveness%20and%20data%20visualisation%20for%20small%20sc%20bfcc7f2b3f63483d9213104e4137aec4/ftavis-story1.png' alt='A zoomed in part of the visual, with a snippet of text with title Bloc Agreements to Protect Markets' class='max-400' />
</p>

<p class='center'>
<img src='Responsiveness%20and%20data%20visualisation%20for%20small%20sc%20bfcc7f2b3f63483d9213104e4137aec4/ftavis-story2.png' alt='A zoomed in part of the visual, with a snippet of text with title Trade Relations of the Third World with Developed Countries' class='max-400' />
</p>

Source: [ftavis.com](http://ftavis.com)

Reuters used a mini map to better show a world map on mobile screens.

<video src='Responsiveness%20and%20data%20visualisation%20for%20small%20sc%20bfcc7f2b3f63483d9213104e4137aec4/tracker.mp4' width='100%' height='450px' controls/>

_Source: [Creating responsive data visualisations](https://www.gurmanbhatia.com/talk/2020/11/28/responsive-data-viz-phone.html), Gurman Bhatia_