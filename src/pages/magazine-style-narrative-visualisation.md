The magazine style narrative visualisation genre is what can be thought of as the classical way of integrating visualisations into a story.

<p class='center'>
<img src='Data%20story%20genres%20and%20structures%20854bd72307ad4dbda8a777a86347f3df/magazinestyle.png' alt='A small illustration of a page in a magazine' class='max-200' />
</p>

Source: [Narrative Visualization: Telling Stories with Data](https://cpb-us-e1.wpmucdn.com/sites.northwestern.edu/dist/3/3481/files/2015/02/Narrative_Visualization.pdf)

The bulk of the story is written text, in which the visualisations are embedded so support or complement the written narrative. This format is very popular both in print and online, and mainly uses static visualisations, or visualisations with only limited interactivity.