![A line chart showing a line for total weekly deaths and weekly deaths by cholera during the summer of 1849 in New York](How%20to%20introduce%20less%20common%20chart%20types%2054daaa496c1540519f56f72f5ba88962/propublica-cholera-graphic-630.webp)

Source: [Infographics in the Time of Cholera](https://www.propublica.org/nerds/infographics-in-the-time-of-cholera), propublica.org

The line chart above appeared on the [front page of the New York Daily Tribune on 29 September 1849](https://chroniclingamerica.loc.gov/lccn/sn83030213/1849-09-29/ed-1/seq-1/). It shows the total weekly deaths and the weekly deaths attributed to cholera during an outbreak of the disease in New York City.

At that time, data visualisations were very rare in newspapers (and also outside of it), partly due to the technical challenges of analogue printing (printing illustrations and charts like the one above required engraving). It is safe to assume that many Daily Tribune readers had never seen anything like this chart before and still needed to learn how to read it.

The designers of the charts knew this, and made some design decisions to help the reader:

- the lines on the chart are labelled directly: what each line represents on the chart is clearly labelled, in a bold and in capitals
- the numbers for each week are added as a data label for each data point. Readers were more familiar with seeing these numbers in a tabular format, so the authors made sure to include all the numbers (in the description below the chart, they call the visualisation a “Graphic Table”
- a vertical grid connects the data points with the date labels on the x axis

But more importantly, the chart is complemented with a very detailed description of how to read the chart and what to make of it:

> “The zig-zag lines, which join the ends of these lines, show, by their upward or downward slopes, whether the deaths during those weeks have increased or decreased, rapidly or slowly.”
> 

One lesson to take away from this chart and the way it was presented and introduced in 1849 is that there is no such thing as an “intuitive chart”. We are not born with the capacity to read line charts: it is a learned skill. If you have good arguments to use more novel, innovative and non-standard charts, you can decide they are worth trying. But just like the journalists of the New York Daily Tribune in 1849, you can’t assume your audience will just understand them.

As Scott Klein, managing editor at the data journalism website ProPublica, writes in [the essay that identified the cholera line chart as one of the first data visualisations used in a newspaper](https://www.propublica.org/nerds/infographics-in-the-time-of-cholera), we shouldn’t shy away from using new chart types, as long as we help readers how to make sense of them.

> “First, we must always keep our readers in mind. They have the potential to understand our graphics, but we must never assume our graphics are intuitive, without the need for explanations or directions. Visual and narrative clues about how to read them are vital and mandatory.”
> 

> “On the other hand, this also means we are not trapped into using simple forms. There is no pure set of visual types that conform to human nature and are thus intrinsically better than the others. We are free to experiment. If we keep our readers in mind, we can pursue new forms that delight them and help shed light on complex subjects in ways that have never been tried before. For the right story, our readers will put in the time necessary to understand even strange new graphical forms — like our line chart must have seemed in 1849 — if we only take the time to help them do so.”