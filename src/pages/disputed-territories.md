Territorial disputes might be more common than you think. The [List of territorial disputes page](https://en.wikipedia.org/wiki/List_of_territorial_disputes) on Wikipedia contains no less than 188 disputed areas, distributed over all continents.

![ ](Pitfalls%20in%20mapping%20d062d31d59714b4183eff65fe1492566/world-disputed.png)

A world map overlaying the [disputed areas](http://www.naturalearthdata.com/downloads/10m-cultural-vectors/10m-admin-0-breakaway-disputed-areas/) of the [Natural Earth](http://www.naturalearthdata.com/) dataset (red) on the official country boundaries [as published by the European Commision](https://ec.europa.eu/eurostat/web/gisco) (grey). Source: Maarten Lambrechts, CC BY SA 4.0

This means that all world maps are political: by showing an area as belonging to one or the other country involved in a territorial conflict, the map is taking a side.

Maps published by entities like the EU and its organisations have an official status, and so they should reflect the official position on the territories displayed on the map. This is the reason many governments and intergovernmental organisations publish their own official boundary data. Maps made and published by these organisations should use the official boundaries, because boundaries from other sources (for example from commercial providers like Google Maps) might be different from the official ones.

For the EU, the [Geographic Information System of the COmmission (GISCO)](https://ec.europa.eu/eurostat/web/gisco) manages and publishes the EU official boundary datasets. This data should be used to produce maps instead of boundary data from other sources.