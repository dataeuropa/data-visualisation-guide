<p class='center'>
<img src='Data%20story%20genres%20and%20structures%20854bd72307ad4dbda8a777a86347f3df/interactive-slides.png' alt='A small illustration of interactive slides' class='max-400' />
</p>

Source: [Narrative Visualization: Telling Stories with Data](https://cpb-us-e1.wpmucdn.com/sites.northwestern.edu/dist/3/3481/files/2015/02/Narrative_Visualization.pdf)

In the **interactive slide show structure**, the author determines the overall structure of the story, and lays the story out in blocks of content (using a slide show or another genre). Within each content block, the users are free to explore the data on their own.

In contrast to the Martini glass structure, an interactive slide show has a reader-driven component in the middle of the story.

Below you can find an embedded Flourish story. [Flourish](https://flourish.studio/) is a data visualisation tool with which you can make so called “stories”: interactive slideshows with interactive visualisations on each slide. Use the arrows to navigate the slides and explore the interactivity within each slide.

<iframe style="border: none;" width="100%" height="600px" src="https://flo.uri.sh/story/186356/embed?auto=1"></iframe>

_Source: [Creating a story](https://help.flourish.studio/article/13-creating-a-story), Flourish_