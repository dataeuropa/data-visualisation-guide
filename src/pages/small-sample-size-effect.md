Based on the following chart, you might conclude that Liechtenstein is a dangerous place to live: in 2018 it had the third highest homicide rate in Europe.

![A horizontal bar chart showing homicide rates in European countries. Liechtenstein has the third highest value with 2,62, behind Latvia and Lithuania](More%20pitfalls%20in%20statistics%20980beb010f984cd49c83ec8dab6dae6e/homicide-rate-countries.png)

Source: [Recorded offences by offence category - police data](https://ec.europa.eu/eurostat/databrowser/bookmark/940b75f0-56a0-4dd7-9f7e-afbd58b589a5?lang=en), Eurostat

But in order to interpret the homicide rate correctly, you also need to consider the denominator. The homicide rate is expressed as the number of homicides per 100.000 inhabitants, but in 2018 Liechtenstein only had 38.378 inhabitants. So its homicide rate of 2,62 only represents a single murder.

A year later, Liechtenstein didn’t register any murders, so its homicide rate was 0.

![A vertical bar chart showing the homicide rate of European countries in 2018 and 2019.](More%20pitfalls%20in%20statistics%20980beb010f984cd49c83ec8dab6dae6e/Intentional_homicide_2018_and_2019_police-recorded_offences_per_100_000_inhabitants.png)

Source: [Crime statistics](https://ec.europa.eu/eurostat/statistics-explained/index.php?title=Crime_statistics), Eurostat

Pay attention to the first note at the bottom of the chart above. As a warning for the values of small countries, Eurostat mentions that these can show big fluctuations. This effect is seen in many fields: cities, regions and countries with small populations tend to show more extreme (very high and very high) numbers. Rare events, or the lack of them, can swing numbers significantly up or down because of the small population.