When data are collected and communicated carelessly, data analysis and data visualisations have a large capacity to mislead, misrepresent, and harm communities that already experience inequity and discrimination.

The US based nonprofit research organisation Urban Institute conducted interviews with data visualisation practitioners to see how they deal with this issue. They compiled their results in the [Do No Harm Guide](https://www.urban.org/research/publication/do-no-harm-guide-applying-equity-awareness-data-visualization) and extracted 11 recommendations regarding diversity, equity and inclusion in data visualisation. You can find them below.

**1. Critically examine your data**

Understand where your data comes from, who is included and excluded from the data, how the data was collected, why it was collected, and who benefits or is harmed by it.

**2. Use people-first language**

Start with the person, not the characteristic. So say “people with disabilities” instead of “disabled people” and “communities of colour” instead of “coloured communities”

**3. Label people, not skin colour**

Use full labels such as “Black people” rather than “Black.” And remember that language continues to evolve. Certain labels that may have been acceptable years ago may no longer be.

**4. Order labels purposefully**

Don’t simply order data in tables, graphs, and charts as they are ordered in the data, which may reflect historical biases. Consider alternative sorting parameters such as study focus, specific story or argument, quantitative relationship (i.e., magnitude of the results), alphabetical, or sample size (weighted or unweighted).

**5. Consider missing groups**

What groups are not included in your data? Consider adding notes to highlight how the data are not inclusive or representative.

**6. Consider alternatives to labelling the “other” catch-all category**

Use any of the following descriptions below

- Another
- Another race
- Additional groups
- All other self-descriptions
- People identifying as other or multiple races
- Identity not listed
- Identity not listed in the survey

**7. Carefully consider colours**

Avoid reinforcing gender or racial stereotypes, such as by using baby pink and baby blue to represent women and men or colours associated with skin tones or racial stereotypes. Avoid using incremental colour palettes (e.g., light to dark) to represent different demographic groups.

**8. Consider icons and shapes**

Recognise how readers might be better able to connect with the data by using small shapes or icons, but use them carefully. Avoid stereotypical, discriminatory, and racist imagery. Use images that show people as empowered and dignified, and avoid images that depict people as
helpless victims.

**9. Communicate with people and communities of focus**

Reach out to the people and communities you are focusing on in your work and hoping to connect with through the final product. Build teams and connections with outside groups to
build and maintain these relationships. This kind of work takes time and effort.

**10. Reflect lived experiences**

Not everyone has the same experiences, especially when it comes to characteristics such as race, ethnicity, and gender. Consider what your work may be missing and seek out colleagues and communities to help identify them.

**11. Consider the needs of your audiences**

Make sure results are presented in a format that is useful to the audience. Make sure the language used is written in a way that is easily understandable by your readers. Consider translating your products into the languages used by your audiences.