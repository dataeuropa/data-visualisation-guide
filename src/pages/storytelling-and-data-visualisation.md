If you ask 20 people what their favourite novel or favourite movie is, chances are you will get 20 different answers. But those 20 people will probably agree that their favourite novels and movies are all a good stories. But what makes a good story?

A good story needs to be **engaging**. A good book makes you want to grab it on any moment you have some time to read, and a good movie can absorb you completely and make you forget your daily worries.

Stories try to engage you by using a wide range of storytelling techniques. They trigger your **curiosity**, be revealing a glimpse of the story, but not the most important parts of it to understand what happened, and so they “hook” you into reading more or keep watching. They make you see what happened through the eyes of people experiencing events, so you **empathise** with them: you see what they see and you feel the emotiones they are feeling. And good stories have a **rythm**, varying between high intensity story parts and low intensity ones, and between joyful events full of happiness and dreadful events full of misery.

<iframe width="100%" height="460" src="https://www.youtube.com/embed/S5BhQiny7O0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

But the really good stories **teach you something about the world** we are living in.  A story might have a **moral**, hidden beneath the surface of the small and big events described in the story, and readers might start to question their norms and values: what would they do in the difficult situation the protagonist of the story finds herself in? And a story can **connect the dots**, and teach the reader how things they thought were unrelated turn out the be related after all.

So, good stories are a mechanism to communicate knowledge packed up in an engaging format in a way that most people would not recognise that someone is trying to teach them something.

## Storytelling and data visualisation

So can data visualisation learn anything from storytelling? The answer is, of course, “yes”, otherwise it would not make sense to follow this training.

Creating data visualisations and stories based on data that are so engaging that readers or viewers become completely immersed and engaged like if they were reading a great novel or watching a blockbuster movie, is really hard. People empathise with people (characters and protagonists), not with numbers. Numbers are abstract, cold and clear, while people are real, ambiguous and filled with emotions.

So how can data visualisation teach the viewer something about the data and the (part of the) world it represents, without the viewer feeling she is taught a lesson? The answer is to make the viewer’s life **as easy as possible**. Make “getting a chart” and understanding the message hidden in the data a smooth experience. That is what the first part of this training is about: making all elements in a chart (titles, annotations, chart types, ...) work together, so viewers need to the least amount of brain processing possible to take away the message of the visualisation your are trying to communicate.

And because numbers are hard to empathise with, does not mean that all storytelling techniques are invalid when a story is based on data. A lot of **data refers to people**, and other data **relates to the daily lives of people**, so making an engaging connection with the reader is still very much possible. Many techniques exist to deepen this connection and hook readers into your data driven story and visualisations. On top of that, **story genres, structures and patterns** have been defined that elevate data driven stories above the level of simply listing facts and numbers one after the other. That is what the second part of the training is about.