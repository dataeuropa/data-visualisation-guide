<p><span class='internal-link'><a href='highcharts'>Highcharts</a></span> is a JavaScript library to make a wide variety of charts and maps. To make Highcharts more accessible, Highcharts offers an <a href='https://www.highcharts.com/docs/accessibility/accessibility-module'>Accessibility module</a> (there are also additional modules to offer a link to download the data shown on a Highcharts visualisation).</p>

Some of the accessibility features in Highcharts are:

- add a text description of a chart and make it available to screen readers
- makes the data points in a visualisation and the chart menu navigable and interactable with a keyboard

<iframe width="100%" height="500" src="https://www.youtube.com/embed/HNS6PBpVqDo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Demo of a Highcharts map that is navigable with the keyboard and accessible to screen readers. Source: [highcharts.com/accessibility](https://www.highcharts.com/accessibility/)

- <span class='internal-link'><a href='double-encoding'>double encoding</a></span> to not only rely on colours. This is implented through the use of different fill and dot patterns
    
![Examples of Highchart visualisations with pattern fills with different colours, applying the concept of double encoding](Accessibility%20in%20data%20visualisation%20tools%206c7b042656f64445977564c856e9a8b9/highcharts-patterns.png)
    
Source: [highcharts.com/accessibility](https://www.highcharts.com/accessibility/)
    
- export of an SVG version of charts, that can be turned into a tactile graphic
- <span class='internal-link'><a href='data-sonification'>sonification of data</a></span>, that turns data into sound