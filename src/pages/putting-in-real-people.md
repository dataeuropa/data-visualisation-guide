Research has shown that the role of anthropographics in provoking empathy [might be limited](https://medium.com/@FILWD/can-visualization-elicit-empathy-our-experiments-with-anthropographics-7e13590be204). But a technique that has proven its effectiveness is to tell a data driven story by complementing it with the traditional journalistic technique of personalising events through the eyes of those experiencing it.

An excellent example of putting in real people is [Dollar Street](https://www.gapminder.org/dollar-street). Dollar Street visited 264 families in 50 countries and collected 30.000 photographs to show these families and how their income relates to their living conditions. The project manages to humanise dry statistics by putting a face to the families, it makes the experience of consuming the numbers much more visceral.

![A web browser showing the Dollar Street project, with pictures of 8 families](Journalistic%20techniques%20for%20data%20storytelling%208bdd09bf88074238b1fe53b3a2116e1e/dollar-street-overview.png)

Source: [gapminder.org/dollar-street](https://www.gapminder.org/dollar-street)

![A web browser showing the pictures of the inside of the home of a family in Jordann](Journalistic%20techniques%20for%20data%20storytelling%208bdd09bf88074238b1fe53b3a2116e1e/dollar-street-detail.png)

Source: [gapminder.org/dollar-street](https://www.gapminder.org/dollar-street)

In [The Last Person You’d Expect to Die in Childbirth](https://www.propublica.org/article/die-in-childbirth-maternal-death-rate-health-care-system), the issue of maternal deaths in the United States is introduced by telling the story of neonatal intensive car nurse Laura Bloomstein. Laura died soon after giving birth to a baby girl in 2011.

![A collage of picutres of woman on the beach, with a newborn and on a party](Journalistic%20techniques%20for%20data%20storytelling%208bdd09bf88074238b1fe53b3a2116e1e/maternal-mortality-lead-pro-publica.jpg)

Source: [The Last Person You’d Expect to Die in Childbirth](https://www.propublica.org/article/die-in-childbirth-maternal-death-rate-health-care-system), propublica.org

Laura’s story is a gripping story, and it is impossible not to go through many emotions when reading it. It is an illustration of a wider problem in the American health care system, that suffers from high maternal death rates, even though spending in the sector is high.

Only after Laura’s story, the statistics showing this problem are presented through data visualisation.

![A line chart showing increasing maternal mortality in the US, while the lines for other countries are going down](Journalistic%20techniques%20for%20data%20storytelling%208bdd09bf88074238b1fe53b3a2116e1e/propublica-maternal-deaths.png)

Source: [The Last Person You’d Expect to Die in Childbirth](https://www.propublica.org/article/die-in-childbirth-maternal-death-rate-health-care-system), propublica.org

People empathise with people, not with numbers. So when dealing with statistics related to people, like life and death, health, income and other topics, picking real people and telling their story can be an entry point into the broader issue made visible with data visualisation.