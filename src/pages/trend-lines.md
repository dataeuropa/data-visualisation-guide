The scatterplot shown below contains a regression line showing the trend in the data. To help the understanding, this **trend line should be explicitly annotated**. The line could be annotated with a generic annotation, like “Trend line”, but better would be to annotate it with text reinforcing the take away message of the chart, like “Higher sugar consumption is associated with higher numbers of decayed teeth”.

<p class='center'>
<img src='A%20deep%20dive%20into%20scatter%20plots%20447afd31ef0d4b0a887b000d2b360f95/pew-scatterplot.webp' alt='A scatter plot of countries with their average consumption of sugar on the x axis and the average number of decayed teeth per person on the y axis' class='max-600' />
</p>

Source: [The art and science of the scatterplot](https://www.pewresearch.org/fact-tank/2015/09/16/the-art-and-science-of-the-scatterplot/), pewresearch.net

The trend line in the Pew scatterplot divides the scatter plot into two regions: the region above the line contains the countries with people having relatively high numbers of decayed teeth, and in the countries below the line people have low numbers of decayed teeth. If the countries below the line would have some shared characteristic relevant to the story (like high per capita numbers of dentists, good government policies on dental care, ...) the **area below the and above the trend line** can also be explicitly labelled.

![A scatter plot titled 'Republican-leaning counties saw lower turnout', with margin in presidential race on the x axis and change in turnout in states on the y axis](A%20deep%20dive%20into%20scatter%20plots%20447afd31ef0d4b0a887b000d2b360f95/538-trendline.png)

An explicitly labelled trendline. Source: [fivethirtyeight.com](https://fivethirtyeight.com/features/our-51-best-and-weirdest-charts-of-2021/)

When the observed correlation in the data is not a linear one, you could use other regressions techniques. See <span class='internal-link'>[Smoothing lines](smoothing-lines)</span> for some examples.