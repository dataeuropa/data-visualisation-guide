A visualisation is composed out of different building blocks (see also <span class="internal-link">[Chart anatomy](chart-anatomy)</span>): there is text (like a title, labels and annotations), there is the data itself (represented by bars, lines, points or other geometrical elements) and there are guides for interpreting and reading of data (like axes, grid lines and colour legends).

The design of text elements is covered in the <span class="internal-link">[pages on typography](tag/typography)</span>. The design of data elements is a very broad topic and depends to a large degree on the chart type. This is covered in the <span class="internal-link">[pages on chart types](tag/chart-types)</span>. The design of other chart elements are covered in

- the page on <span class="internal-link">[the chart canvas](chart-canvas)</span>
- the page on <span class="internal-link">[axis, grids and legends](axes-grids-and-legends)</span>