Next to patterns in the data itself, storytelling in data visualisation can also be achieved by presenting data using a specific view on it.

A common view pattern is putting 2 **contrasting views** next to each other, to highlight differences.

![A chart showing 10.109 companies as dots. The dots are ordered according to the pay gap between men and women, with the companies paying women more on the left and companies paying men more on the right. The dots belonging to companies that pay women more are green, the companies that pay men more are orange](Patterns%20for%20data%20driven%20stories%2034fe0220a7d64297ae6ccf534303e18f/paygap-guardian.png)

Source: [Gender pay gap: what we learned and how to fix it](https://www.theguardian.com/news/ng-interactive/2018/apr/05/women-are-paid-less-than-men-heres-how-to-fix-it), theguardian.com

![A graphic comparing the Russian army on the left and the Ukrainian army on the right. The armies are compared using icons representing the number of tanks, fighting vehicles and artillery pieces](Patterns%20for%20data%20driven%20stories%2034fe0220a7d64297ae6ccf534303e18f/weapons-gap-guardian.png)

Source: [Russia’s war in Ukraine: complete guide in maps, video and pictures](https://www.theguardian.com/world/2022/mar/07/russia-war-ukraine-complete-guide-maps-video-and-pictures), theguardian.com

A common pattern when small details are important in a data driven story, is to start with a general, high level overview on the data to give context, and then zoom in on smaller details. This technique of **zooming in** can be applied both in static and interactive media.

Below is an example using a “lens” to magnify a small but significant detail in the visualisation.

![A chart showing countries' GDP and the cumulative impact of bailout since 2007 on Gross Public Debt and other support. A part of the graphic in the middle left is magnified to reveal a small detail](Patterns%20for%20data%20driven%20stories%2034fe0220a7d64297ae6ccf534303e18f/crisis-annotation-chart-FR.webp)

Source: [FT.com](https://www.ft.com/content/b823371a-76e6-11e7-90c0-90a9d1bc9691)

In the pie charts below, a small portion of the top 2 charts is magnified in the pie chart just below it.

![Three 3D pie charts showing the composition of all water on earth, divided between saltwater, freshwater, ice caps and glaciers, groundwater, water in lakes, water in soil, water vapour in the atmosphere, water in living organisms and water in rivers](Patterns%20for%20data%20driven%20stories%2034fe0220a7d64297ae6ccf534303e18f/water-stats-pie21.jpg)

Source: [thiswoo.wordpress.com](https://thiswoo.wordpress.com/2008/10/21/i-give-you-statistical-pie-charts/), graphic by [Andrew Davies](https://www.andrewdavies.com.au/)

Zooming in does not have to involve very elaborate designs. Below the same data as above is used, but this time with more traditional chart types.

![The same data set about the composition of the water on earth used in the graphic above, but this time visualised with 3 bar charts, of which 2 charts are a magnification of another one](Patterns%20for%20data%20driven%20stories%2034fe0220a7d64297ae6ccf534303e18f/data-as-bar-of-bar-bar-chart.png)

Source: [Chandoo.org](https://chandoo.org/wp/pie-of-pie-of-pie-chart/)

The reverse of zooming in can also work well: start with a detail and then **zoom out** to show the bigger picture. The video midway [Living in China’s Expanding Deserts](https://www.nytimes.com/interactive/2016/10/24/world/asia/living-in-chinas-expanding-deserts.html) transitions the story of a little boy and his family living in a Chinese desert to a map of China showing the issue of desertification in the country.

<video src="https://int.nyt.com/data/videotape/finished/2016/09/13/china-desertification/zoomer-1254.mp4" width="100%" height="450px" controls />

The same dataset can be visualised in many different ways. And sometimes a data driven story can benefit from having multiple views on the same data, to **switch the context** and to have a look at the data from a different angle. A map can show geographical patterns in data, but a different representation of the entities on the map can show other patterns, like ranges and distributions. In online media, a context switch can be animated so that it is more obvious that data in the 2 different visualisations is actually the same.

<video src="Patterns%20for%20data%20driven%20stories%2034fe0220a7d64297ae6ccf534303e18f/animation-798x.mov" width="100%" height="450px" controls />

_A map transitions into a dot plot. Source: [Why Budapest, Warsaw, and Lithuania split themselves in two](https://pudding.cool/2019/04/eu-regions/), pudding.cool_

A last specific view pattern are **small multiples**. When a multi-series visualisation contains many series (like a line chart with many lines) and the series are of equal importance, the visualisation can become crowded quickly. Small multiples help to overcome this problem by showing repeated mini versions of the chart for each series, which can then be arranged next to or above and below each other. Applying a logical ordering to the small multiples helps to compare them, and helps to quickly find a small multiple of interest.

![A series of area charts showing the trend in undernourishment rates in 7 regions of the world](Patterns%20for%20data%20driven%20stories%2034fe0220a7d64297ae6ccf534303e18f/small-multiples.png)

Source: [Beyond hunger: ensuring food security for all,](https://datatopics.worldbank.org/sdgatlas/goal-2-zero-hunger/) Sustainable Development Goals Atlas 2020, World Bank

![A grid of line charts titled 'My eyes are up here'. The charts show trends in what is considered sexual harassment in five countries](Patterns%20for%20data%20driven%20stories%2034fe0220a7d64297ae6ccf534303e18f/economist-small-multiples.png)

Source: [Over-friendly, or sexual harassment? It depends partly on whom you ask](https://www.economist.com/graphic-detail/2017/11/17/over-friendly-or-sexual-harassment-it-depends-partly-on-whom-you-ask),  Economist.com

A view pattern specifically designed to grab attention is to **break existing conventions in data visualisation**. This make people look twice and it generate curiosity. But because breaking conventions comes at a cost (in legibility and efficiency in communicating data) this technique might lead to confusion.

A classic example of this is Iraq’s bloody toll by Simon Scarr. It breaks the convention of the y axis running from bottom to top, in order to mimic blood dripping down the chart.

![A bar chart with red bars running down, mimicking blood. The title of the chart is 'Iraq's bloody toll', the y axis shows monthly fatalities of the war in Iraq, and the x axis runs from 2003 up to 2011](Patterns%20for%20data%20driven%20stories%2034fe0220a7d64297ae6ccf534303e18f/iraqs-bloody-toll.png)

Source: [simonscarr.com](http://www.simonscarr.com/iraqs-bloody-toll), for South China Morning Post

Letting the x axis spiral instead of letting it run straight, is a very clear example of breaking a convention. This spiral chart showing new covid-19 cases in the US sparked a lively debate over its design (people arguing against the spiral chart forgot or didn’t know that it accompanied an opinion article, and was not designed as a visualisation to read exact values from).

![A chart in the form of a spiral, titled 'New Covid-19 cases, United States'. The spiral start small in 2020 and ends wide in January 2022](Patterns%20for%20data%20driven%20stories%2034fe0220a7d64297ae6ccf534303e18f/spiral-chart.png)

Source: [Here’s When We Expect Omicron to Peak](https://www.nytimes.com/2022/01/06/opinion/omicron-covid-us.html), nytimes.com

An illustration of how strong conventions are and how strange things can look when they are not respected, is this rotated map of the Mediterannean. We are so used to seeing maps with the North up that this North-to-the-right map feels strange. Suddenly Europe seems much closer to Africa than we think it is.

![A map of the Mediterranean, with the North pointing to the right](Patterns%20for%20data%20driven%20stories%2034fe0220a7d64297ae6ccf534303e18f/mediterranean-rotated.jpg)

Source: [Sabine Réthoré](http://www.sabine-rethore.net/engl/artistic%20maps/mediterraneanwit.html), [Free Art License](https://en.wikipedia.org/wiki/Free_Art_License)