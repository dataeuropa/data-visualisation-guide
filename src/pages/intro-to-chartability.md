Because visualisation researcher [Frank Elavsky](https://www.frank.computer/) noticed a big gap when it came to guidelines on accessibility of data visualisations, he developed [Chartability](https://chartability.fizz.studio/). As Chartability describes itself:

> Chartability is a set of heuristics (testable questions) for ensuring that data visualizations, systems, and interfaces are accessible.
> 

From this definition it is clear that Chartability goes beyond the accessibility of just simple charts. It can evaluate any kind of data experience, including charts, graphs, bespoke and custom graphics based on data, models, algorithms and other data driven interfaces or systems.

Chartability is based on WCAG and its principles, and is not meant to replace these internationally recognised guidelines. But the WCAG were not developed with data visualisation in mind, so even WCAG compliant data visualisations and interactive charts can still be inaccessible to a lot of users.