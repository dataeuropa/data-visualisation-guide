Sometimes the shape a visualisation takes makes it suitable to be integrated into the layout of an article.

Here is an example by The Washington Post. The shape of the bar chart is “wrapped around” the article itself instead of being embedded in it.

![A bar chart integrated into the top of an article on washingtonpost.com. The highest value on the bar chart runs next to the text of the article for several paragraphs](Combining%20visualisations%20with%20other%20media%203195c842e70245d394d6c9e019d7165e/wapo-chart-layout.png)

Source: [The hidden billion-dollar cost of repeated police misconduct](https://www.washingtonpost.com/investigations/interactive/2022/police-misconduct-repeated-settlements/), washingtonpost.com

Here is a similar approach, but this time from Les Echos and in print. The chart showing the Chinese gdp growth is “running of the page”:

![A spread of newspaper Le Monde, with a line chart running over the text and onto the next page](Combining%20visualisations%20with%20other%20media%203195c842e70245d394d6c9e019d7165e/spread-lemonde-grandin.jpg)

Source: [@JulesGrandin](https://twitter.com/JulesGrandin/status/1075325066240552961) 