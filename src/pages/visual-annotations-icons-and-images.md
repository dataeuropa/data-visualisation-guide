Visual annotations can also take the form of icons, images and illustrations. Consider the following chart, taken from a study describing the behaviour of dogs when their owner paid attention to stuffed dogs and to 2 other objects:

![A grouped bar chart in black and white showing the proportion of dogs exhibiting certain behaviour](Visual%20annotations%20589ebcc4e0024634956566d2e144385f/jealous-dogs-original.png)

Source: [Jealousy in Dogs](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0094597), PLOS ONE

The behaviour of the jealous dogs on the x axis is described in words. As a reader, especially as a dog-owning reader, you might have an idea about how this dog behaviour looks like in reality. But now compare the above chart with the one below, where the dog behaviour is illustrated with drawings.

![The same chart as above, but with little drawings of dog exhibiting certain behaviours](Visual%20annotations%20589ebcc4e0024634956566d2e144385f/jealous-dogs-nytimes.png)

Source: [Inside Man’s Best Friend, Study Says, May Lurk a Green-Eyed Monster](https://www.nytimes.com/2014/07/24/science/entering-gray-area-study-says-dogs-can-be-green-with-envy.html), nytimes.com

This reworked chart is not only more fun to look at, chances are that you will also remember the chart and its message better.

The same [research](https://vcg.seas.harvard.edu/files/pfister/files/infovis_submission251-camera.pdf) that discovered <span class='internal-link'>[how important visualisation titles are](the-importance-of-visualisation-titles)</span>, also looked at the effect of adding pictograms to visualisations.

This is one of the conclusions of the study:

> Pictograms do not hinder the memory or understanding of a visualization. Visualizations that contain pictograms tend to be better recognized and described. Pictograms can often serve as visual hooks into memory, allowing a visualization to be retrieved from memory
more effectively. If designed well, pictograms can help convey the message of the visualization, as an alternative, and addition to text.
> 

Adding pictograms to text descriptions creates redundancy: the same feature is described both with an image and with text. This also can help the understanding of a visualisation:

> Redundancy helps with visualization recall and understanding. When redundancy is present, to communicate quantitative values (data redundancy) or the main trends or concepts of a visualization (message redundancy), the data is presented more clearly as measured through better-quality descriptions and a better understanding of the message of the visualization at recall.
> 

One of the most common icons used in data visualisation, are country flags. They can give a visualisation a more attractive look and help readers quickly recognise and find countries of their interest. But country flags shouldn’t be the only cue to identify countries. Country names, or at least country codes, should be provided as labels or tooltips too, because not everyone is familiar with all country flags.

![ ](Visual%20annotations%20589ebcc4e0024634956566d2e144385f/country-flags-eurosearch.png)

A visualisastion using country flags. Each flag is labelled with the country name, so people not familiar with country flags can also easily identify countries. Notice the diagonal dotted line, which is a visual annotation too (text annotations label the areas above and below this line, and the diagonal text explains the meaning of the line). Source: [The Eurosearch Song Contest 2019](https://googletrends.github.io/eurosearch-19/), Maarten Lambrechts

Having redundancy in pictograms and text labels is always a good idea: icons and pictograms should be additional to text descriptions. Visual representations of features and concepts can be ambiguous, or your audience can be unfamiliar with the visual representation, with unrecognised or misunderstood icons as a result. On top of that, textual representation is also important for reasons of accessbility: these are used by assistive technology like screen readers for example.

![ ](Visual%20annotations%20589ebcc4e0024634956566d2e144385f/human-icons-double-representation.webp)

This visualisation makes use of both numbers and icons for the x axis labels representing frequencies of elder people. Source: [‘The pandemic is gaining momentum’: Africa prepares for surge in infections](https://www.ft.com/content/1b3274ce-de3b-411d-8544-a024e64c3542), ft.com

A special case of using pictograms is the use of icons to represent people. This is a broad topic, that is covered by the page on <span class='internal-link'>[anthropographics](anthropographics)</span>.