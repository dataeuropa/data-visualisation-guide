Chart annotations are crucial in data-driven storytelling. They help chart authors to explain and highlight the core messages of their visualisations. As former graphics editor of the New York Times Amanda Cox stated in a now famous quote:

> “The annotation layer is the most important thing we do. . . otherwise it’s a case of here it is, you go figure it out”
> 

In the annotation layer, <span class='internal-link'>[text annotations](tag/text-annotations)</span> are very common, and they are important in explaining patterns in a visualisation. But non-text annotations also exist, and can help in getting the message of your visualisation to your audience. They fall apart into 3 categories:

- Visual annotations: <span class='internal-link'>[highlights](visual-annotations-highlights)</span>
- Visual annotations: <span class='internal-link'>[aggregation and distribution](visual-annotations-aggregation-and-distribution)</span>
- Visual annotations: <span class='internal-link'>[icons and images](visual-annotations-icons-and-images)</span>