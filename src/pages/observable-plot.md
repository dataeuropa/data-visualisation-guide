Free, open source

[observablehq.com/@observablehq/plot](https://observablehq.com/@observablehq/plot)

Observable Plot is a JavaScript visualisation library created by Mike Bostock, the creator of the <span class='internal-link'>[D3 library](d3js)</span>. It is based on the same concepts as <span class='internal-link'>[Vega](vega)</span>, and generates charts in SVG. It was developed to work well in the Observable interactive notebook environment, but is also available as a [standalone JavaScript library](https://github.com/observablehq/plot).

![A scatter plot of athletes' weigth (x axis) and height (y axis), made with Observable Plot](Data%20visualisation%20design%20in%20practice%202%20tools%20208f06b06b0f4b21ad8ecf3047f02ce0/observable-plot.png)

Source: [observablehq.com/@observablehq/plot](https://observablehq.com/@observablehq/plot)

See <span class='internal-link'>[Grammar of Graphics in practice: Observable Plot](grammar-of-graphics-in-practice-observable-plot)</span> for an introduction to Observable Plot.