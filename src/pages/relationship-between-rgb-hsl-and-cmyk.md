RGB and HSL colours are sharing the same space: they are just different ways of describing the coordinates in that space. Converting RGB to HSL and vice versa can be done by using relatively simple formulas.

Many tools exist to convert between the two modules. The easiest one to find is the one that is built into Google: simply Google a hexadecimal colour, like [#855ca1](https://www.google.com/search?q=%23855ca1), or an rgb colour, like [rgb(237, 148, 38)](https://www.google.com/search?q=rgb(237%2C148%2C38)). This will open the searched colour in a colour picker, in which you can adjust colours visually. Colours can be adjusted in RGB (both in decimal as in hexadecimal values), in HSL, in HSV (which stands for hue, saturation and value, and is similar to HSL) and CMYK.

![The Google colour picker, with a purple colour selected](Colour%20the%20basics%20a90e331756d2497aa8b2b3ce26b9e3de/google-colorpicker.png)

Source: [google.com](https://www.google.com/search?q=%23855ca1)

Notice how in HSL, the hue value moves between 0 and 360 when you move the colour slider from right to left. The saturation and lightness remain the same. The saturation increases when the white circle is moved from left to right, and the lightness increases when the white circle is moved from bottom to top.

Converting to and from CMYK is less straightforward, because this involves converting between an additive model for colours on screens and a subtractive model for colours printed on paper. The easiest way to convert colours in a document to CMYK is to use software that can do this for you, like GIMP, Adobe Photoshop or Adobe Illustrator.

And even after converting to CMYK, the colours in the document you designed will look different than when the document is printed. The safest bet is to use a colour chart with CMYK colours printed on the printer you will use for the final prints.

![ ](Colour%20the%20basics%20a90e331756d2497aa8b2b3ce26b9e3de/cmyk-color-chart.jpg)

A colour chart with CMYK color codes. Source: [Penoki98](https://commons.wikimedia.org/wiki/File:%EC%B9%BC%EB%9D%BC%EC%B0%A8%ED%8A%B8.jpg), CC BY-SA 4.0

When you pick the colours from the colour chart and use their codes in your design, you know what the colours will look like when printed (even though they might look different on screen).