Using data to generate sounds (datasonification) is a relatively new field.

In [Sonification: turning the yield curve into music](https://www.ft.com/content/80269930-40c3-11e9-b896-fe36ec32aece), Alan Smith of the Financial Times used sound to sonify financial data.

<video src='https://next-media-api.ft.com/renditions/15525620840330/1280x720.mp4' width='100%' height='450px' controls/>

_Source: [Sonification: turning the yield curve into music](https://www.ft.com/content/80269930-40c3-11e9-b896-fe36ec32aece), ft.com_

NASA has used data to [sonify data captured with different telescopes from a region in the center of the Milky Way](https://chandra.si.edu/photo/2020/sonify/animations.html) some 26.000 thousand light years away from us.

<video src='https://chandra.si.edu/photo/2020/sonify/sonify_galactic_all.mp4' width='100%' height='450px' controls/>

_Source: [Animations: Data Sonification: Sounds from Around the Milky Way](https://chandra.si.edu/photo/2020/sonify/animations.html), chandra.si.edu_

Another approach is to integrating sound and data visualisation is to add an audio layer to an animated data visualisation. Click the play button and listen to voice over. Note that this is not video: when the animation is playing or when you stop the animation, you can still access the interactive features of the visualisation, like the tooltips.

<iframe src='https://flo.uri.sh/story/25749/embed?auto=1' width='100%' height='700px' style='border: none;'></iframe>

_Source: [Why data visualization needs a play button](https://flourish.studio/blog/audio-talkie-visualisation-data-stories/), flourish.studio_