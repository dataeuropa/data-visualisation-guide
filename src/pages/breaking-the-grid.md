
Because the grid creates a regular pattern (see <span class="internal-link">[The grid](the-grid)</span>), with clean alignments and layout, any element that is not aligned to the grid of a publication, creates tension and dynamism. When breaking the grid is used well, it creates more interesting and engaging designs.

![A design that splits the page midways in two, with the title running over the separation between both parts](Grid%20and%20arrangement%204ba513397bf6413d900e7c2944888770/squarespace-breaking-grid.png)

The title is breaking the grid, and connects the left and right part of the design. Source: [Andre Ribeiro](https://andreribeiro.co/)

![A page design in which the text in the two columns runs around a central illustration of a leaf](Grid%20and%20arrangement%204ba513397bf6413d900e7c2944888770/breaking-grid-layout.jpg)

Wrapping the text in the 2 columns around a central object creates a dynamic design. Source: [Ted LoCascio](https://creativepro.com/converting-glyph-characters-into-graphic-elements/)

The technique of breaking the grid can be used to draw attention to a data visualisation, or to specific data points in a visualisation. It is a very efficient technique to draw attention to outliers (data points with extreme values) in data sets.

![](Grid%20and%20arrangement%204ba513397bf6413d900e7c2944888770/nytimes-front-2020-04-08-detail.jpg)

A spike map of covid-19 deaths in the United States, on the front page of the New York Times. The New York spike breaks out of the module of the map, and even spills over the heading. Source: [nytimes.com](https://www.nytimes.com/issue/todayspaper/2020/04/08/todays-new-york-times)

![A series of bar charts, of which the highest value in one chart 'spills over' to the chart above](Grid%20and%20arrangement%204ba513397bf6413d900e7c2944888770/break-grid-historical.jpg)

Source: [davidrumsey.com](http://www.davidrumsey.com/luna/servlet/workspace/handleMediaPlayer?lunaMediaId=RUMSEY~8~1~265455~5524620)

The Financial Times used this technique to show that some values were literally “off the charts”:

![A series of area charts titled 'Omicron is sending cases soaring to record highs across the world'. The highest values on the charts, for Australia and Denmark, spill over over the title and even of the edge of the image](Grid%20and%20arrangement%204ba513397bf6413d900e7c2944888770/Omicron-breaking-grid-FT.jpg)

Source: [twitter.com/jburnmurdoch/status/1478339853930618885](https://twitter.com/jburnmurdoch/status/1478339853930618885)

![The same technique applied in a chart titled 'In many US states, ICY patient numbers are already appraoching record levels as Omicron takes off'](Visual%20hierarchy%2032d60a2016ea4334ae0d7e2395559439/FIQez-BXoAAqXxg.jpg)

Source: [twitter.com/jburnmurdoch/status/1478340575053197313](https://twitter.com/jburnmurdoch/status/1478340575053197313)