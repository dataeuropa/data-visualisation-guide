Braille textbooks sometimes contain tactile graphics: embossed maps, charts, schematic diagrams and geometric figures that can be sensed through touch. 

![ ](Braille,%20data%20sonification%20and%20data%20physicalisatio%203da60749f91a44b48ddbe910563ea247/5-06001-00_World_Maps.jpg)

A tactile map of the Middle East. Source: [aph.org](https://www.aph.org/product/world-maps/)

Tactile maps are important for wayfinding in public transport infrastructure for visually impaired people.

![ ](Braille,%20data%20sonification%20and%20data%20physicalisatio%203da60749f91a44b48ddbe910563ea247/tactile-map-rail-station-warsau.png)

A tactile map of the Warsaw East station, Poland. Source: [Tactile Graphics at Railway Stations – an Important Source of Information for Blind and Visually Impaired Travellers](https://yadda.icm.edu.pl/baztech/element/bwmeta1.element.baztech-32af10ed-fc6f-49ed-a768-d6d3ee8147be/c/175_9.pdf)

Making tactile graphics is an art in itself, and usually involves manual craft. The American Printing House for the Blind has published [a library of templates for good tactile designs](https://imagelibrary.aph.org/portals/aphb/#page/welcome), which also includes some examples of charts.

![ ](Braille,%20data%20sonification%20and%20data%20physicalisatio%203da60749f91a44b48ddbe910563ea247/decibel-barchart-tactile.png)

A tactile bar chart showing the loudness of different sounds in the decibel scale, from a whisper at the top to a jet engine at the bottom. Source: [imagelibrary.aph.org](https://imagelibrary.aph.org/)

![ ](Braille,%20data%20sonification%20and%20data%20physicalisatio%203da60749f91a44b48ddbe910563ea247/effect-social-distancing-tactile.png)

A tactile graph showing the effect of social distancing on the evolution of an infectious disease. Source: [imagelibrary.aph.org](https://imagelibrary.aph.org/)

![ ](Braille,%20data%20sonification%20and%20data%20physicalisatio%203da60749f91a44b48ddbe910563ea247/coronavirus-comparison-tactile.png)

A tactile scatter plot showing the fatality rate (y axis) and contagiousness (x axis) of selected diseases, inspired by a chart by [The New York Times](https://www.nytimes.com/interactive/2020/world/asia/china-coronavirus-contain.html). Source: [imagelibrary.aph.org](https://imagelibrary.aph.org/)

An attempt to make time series data accessible on refreshable braille displays is [SparkBraille](https://fizzstudio.github.io/sparkbraille/). It uses the cells of the braille display to show a simplified version of a line chart, to get the gist of the trends in the data.

![A graphic explaining how SparkBraille is constructed from a traditional line chart](Braille,%20data%20sonification%20and%20data%20physicalisatio%203da60749f91a44b48ddbe910563ea247/sparkbraille.png)

The numbers in the time series are binned into vertical buckets, and each braille cell is used to show 2 binned values. Source: [fizzstudio.github.io/sparkbraille](https://fizzstudio.github.io/sparkbraille/)

SparkBraille is currently only a proof of concept.