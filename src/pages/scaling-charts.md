For larger charts data marks and font sizes should be scaled up to some degree: bigger charts need thicker lines, larger dots and bigger letters. Otherwise the design can look empty. The reverse is also true: these elements need scaling down for smaller charts.

![A line chart showing trends for all EU member states, with the lines for Sweden, Ireland and the EU average labelled and highlighted with colours](Size%20and%20aspect%20ratios%204b748df4893b4718a66577e684f03486/linechart-normal-size2x.png)

A chart with normal proportions... Source: Maarten Lambrechts, CC BY 4.0

<p class='center'>
<img src='Size%20and%20aspect%20ratios%204b748df4893b4718a66577e684f03486/linechart-small-size2x.png' alt='...and the same chart in smaller size, but with the same font sizes and stroke widths. Source: Maarten Lambrechts, CC BY 4.0' class='max-400' />
</p>

...and the same chart in smaller size, but with the same font sizes and stroke widths. Source: Maarten Lambrechts, CC BY 4.0

<p class='center'>
<img src='Size%20and%20aspect%20ratios%204b748df4893b4718a66577e684f03486/linechart-small-scaled2x.png' alt='' class='max-400' />
</p>

On this chart, font sizes, stroke widths and dot radii have been adapted to the chart dimensions. Source: Maarten Lambrechts, CC BY 4.0

But designing the same chart in different dimensions is not simply a matter of scaling all chart elements up or down. There is more to it.

Bigger charts can hold more data than smaller charts. So one obvious adaptation to chart size is including and excluding chart data.

<p class='center'>
<img src='Size%20and%20aspect%20ratios%204b748df4893b4718a66577e684f03486/linechart-small-scaled-lessdata_12x.png' alt='The grey line representing the other EU member states are removed from this chart' class='max-400' />
</p>

The grey line representing the other EU member states are removed from this chart. Source: Maarten Lambrechts, CC BY 4.0

To avoid overcrowded axis, smaller charts should have less axis labels. Labels on smaller charts could also have a more compact formatting.

<p class='center'>
<img src='Size%20and%20aspect%20ratios%204b748df4893b4718a66577e684f03486/linechart-small-less-ticks2x.png' alt='The same chart as above, but with less axis labels and and less grid lines' class='max-400' />
</p>

Source: Maarten Lambrechts, CC BY 4.0

Bigger charts also offer more space for annotations. When the annotations are important for the message the chart is trying to communicate, they can not simply be left out of smaller versions of the chart. One solution is to move them out of the chart, to a separate space below the chart for example.

<iframe src='https://datawrapper.dwcdn.net/iUPVd/5/' width='100%' height='750px' style='border: none;'></iframe>

Annotations can be integrated into a visualisation when its dimensions allow it. Source: [Datawrapper](https://blog.datawrapper.de/better-more-responsive-annotations-in-datawrapper-data-visualizations/)

<p class="center">
<iframe src='https://datawrapper.dwcdn.net/iUPVd/5/' width='300px' height='550px' style='border: none;'></iframe>
</p>

When space is limited, the annotations can be listed below the visualisation. Source: [Datawrapper](https://blog.datawrapper.de/better-more-responsive-annotations-in-datawrapper-data-visualizations/)

A more radical adaptation to a reduction in size is to change the orientation of a visualisation. When a chart has ample space in the horizontal direction, it could have a wide layout. But when the same chart needs to be designed for smaller widths, one solution is to rotate it 90 degrees, so that the layout changes to high instead of wide.

Some online media serve charts in different layouts depending on screen sizes. Below are two examples.

![An animation showing how a chart changes its orientation when the browser window is made smaller](Size%20and%20aspect%20ratios%204b748df4893b4718a66577e684f03486/verticalcharts.gif)

A visualisation with a wide layout is rotated on narrower screens. Source: [Italy elections 2018 polls: who is running and why it matters](https://ig.ft.com/italy-poll-tracker/), ft.com

![ ](Size%20and%20aspect%20ratios%204b748df4893b4718a66577e684f03486/northleft_nyt.gif)

A map showing a long feature running east-west is rotated to have north to the left to fit on mobile screens. Source: [London’s New Subway Symbolized the Future. Then Came Brexit](https://www.nytimes.com/2017/07/31/world/europe/london-crossrail-uk-brexit.html), nytimes.com

And finally, the most radical adaptation to different constraints in dimensions is to change the chart type altogether. On mobile phones, a simple ranking or bar chart can make more sense than a choropleth map or a line chart for example.