Humans use words to describe colours. We have words like “red”, “orange”, “purple” and “pink” to describe colours. And we even have words to describe different tints of the same color, like “light pink”, “hot pink”, “deep pink” and “salmon”. But these words do not map one on one to exact tints and hues: people use these words to describe slightly different colours.

On the following pages, different systems to exactly describe colours are outlined, together with their application:

- RGB: <span class='internal-link'>[RGB](describing-colours-rgb)</span>
- HSL: <span class='internal-link'>[HSL](describing-colours-hsl)</span>
- CMYK: <span class='internal-link'>[CMYK](describing-colours-cmyk)</span>
- Relationship between different colour models: <span class='internal-link'>[Relationship between RGB, HSL and CMYK](relationship-between-rgb-hsl-and-cmyk)</span>