The next series of patterns for data driven storytelling are very similar to patterns used in traditional storytelling.

One such technique is **question and answers**: start with a question, and then answer by showing a visualisation or by adding new data to an existing visualisation up until a resolution is found.

The video below highlights the structure and the question and answers pattern in [What’s Really Warming the World?](https://www.bloomberg.com/graphics/2015-whats-warming-the-world/), a scrollytelling article by Bloomberg published in 2015.

<iframe width="100%" height="450" src="https://www.youtube.com/embed/Bjrb_ErvvPs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Note that the question and answers technique is similar to the small multiples pattern: it uses **repetition** to break down a multi-series visualisation into single series chunks.

To make it easier for people to understand how a chart works, and for building up tension and curiosity, you can choose to **build up a visualisation** instead of showing the the whole chart at once. In a scatterplot for example, you can explain the x axis first, then explain the y axis, then add a single dot and explain what it represents and finally add the rest of the dots to the visualisation.

This technique is especially handy for chart types many people are not very familiar with. Take for example the chart below. There is a lot going on on this chart, and without help people might have difficulties to understand how how the chart works.

![A chart with stacked, variable width bars, showing the number of learning poor and not learning poor children in different regions of the world](Patterns%20for%20data%20driven%20stories%2034fe0220a7d64297ae6ccf534303e18f/shifted-marimekko-4.png)

Source: [Learning poverty: children's education in crisis](https://datatopics.worldbank.org/sdgatlas/goal-4-quality-education/), Sustainable Development Goals Atlas 2020 

This chart is part of a <span class='internal-link'>[scrollytelling](tag/scrollytelling)</span> module, and before the chart takes the state above, it is preceded with the states below.

First the y axis is explicitly labelled and explained, showing the numbers for the whole world, and only 2 colors are used.

![Annotations show what colour represent learning poor and what colour not learning poor represents](Patterns%20for%20data%20driven%20stories%2034fe0220a7d64297ae6ccf534303e18f/shifted-marimekko-1.png)

Source: [Learning poverty: children's education in crisis](https://datatopics.worldbank.org/sdgatlas/goal-4-quality-education/), Sustainable Development Goals Atlas 2020 

Then a third category and colour is introduced:

![The Learning poor category is split up into children out of school and students below minimum proficiency](Patterns%20for%20data%20driven%20stories%2034fe0220a7d64297ae6ccf534303e18f/shifted-marimekko-2.png)

Source: [Learning poverty: children's education in crisis](https://datatopics.worldbank.org/sdgatlas/goal-4-quality-education/), Sustainable Development Goals Atlas 2020

And then the bars representing the whole world is split up to show the data by region.

![The bar representing the whole world is split up into the regional data](Patterns%20for%20data%20driven%20stories%2034fe0220a7d64297ae6ccf534303e18f/shifted-marimekko-3.png)

Source: [Learning poverty: children's education in crisis](https://datatopics.worldbank.org/sdgatlas/goal-4-quality-education/), Sustainable Development Goals Atlas 2020

**Gradual visual reveal** is a similar technique: instead of showing the whole dataset at once, the data is revealed in steps. On each step the newly added data can be explained.

Below are 4 steps from the scrollytelling article [How Trump’s Trade War Went From 18 Products to 10,000](https://www.nytimes.com/interactive/2018/07/11/business/trade-war.html), revealing new data on each step.

![The first step in the animation starts on January 22 and shows a very limited amount of circles representing trade barriers on certain products](Patterns%20for%20data%20driven%20stories%2034fe0220a7d64297ae6ccf534303e18f/trump-trade-war-1.png)

![On May 18 the visualisation shows a lot of barriers for goods coming into the US, but also a lot of barriers for US products going abroad](Patterns%20for%20data%20driven%20stories%2034fe0220a7d64297ae6ccf534303e18f/trump-trade-war-2.png)

![By June 15, the number of barriers has grown even further](Patterns%20for%20data%20driven%20stories%2034fe0220a7d64297ae6ccf534303e18f/trump-trade-war-3.png)

Source: [How Trump’s Trade War Went From 18 Products to 10,000](https://www.nytimes.com/interactive/2018/07/11/business/trade-war.html), nytimes.com

![Finally the number of barriers does not fit the visualisation canvas anymore](Patterns%20for%20data%20driven%20stories%2034fe0220a7d64297ae6ccf534303e18f/trump-trade-war-4.png)

Source: [How Trump’s Trade War Went From 18 Products to 10,000](https://www.nytimes.com/interactive/2018/07/11/business/trade-war.html)

The gradual visual reveal is also used in the racing bar chart about city populations (see <span class='internal-link'>[Story patterns in data](story-patterns-in-data)</span>): the data is only shown up until the year the animation is currently at. This triggers curiosity: readers will want to know what the chart looks like in the years after that and at the end of the animation, showing the most recent available data.