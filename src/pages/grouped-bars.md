When data contains a secondary categorical breakdown, a bar chart can be turned into a **grouped bar chart**.

![A bar chart with numbers for men and women, showing the pay gap for different eduction levels](A%20deep%20dive%20into%20bar%20charts%20047791ead2e848bdb3d0afcd1bf2bd4a/grouped-bars2x.png)

Pay gap between man and women according to education level. Source: Maarten Lambrechts, CC BY SA 4.0

When the differences between the two groups are more important than the absolute values, it makes more sense to replace the grouped bar chart with a **dumbbell chart**. In a dumbbell chart, the values for the 2 groups are shown with a data mark (a circle or any other symbol), and the values are visually connected to highlight the differences between them.

![The same data as in the bar chart above, but represented with a dumbbell chart](A%20deep%20dive%20into%20bar%20charts%20047791ead2e848bdb3d0afcd1bf2bd4a/dumbbell_12x.png)

Source: Maarten Lambrechts

In the case the 2 groups represent moments in time, the dumbbell can be replaced by an arrow showing the direction and magnitude of the trend. This results in an **arrow chart**.

![The same data as in the bar chart above, with represented with an arrow chart](A%20deep%20dive%20into%20bar%20charts%20047791ead2e848bdb3d0afcd1bf2bd4a/arrow-chart2x.png)

This arrow chart shows the same data as the dumbbell chart above, but in this case it would show the evolution in the wages of people with different education levels between two moments in time. Source: Maarten Lambrechts, CC BY SA 4.0

When the number of groups is limited, grouped bar charts can work well. But when the number of groups grows, they become a lot harder to read.

![A grouped bar chart with 5 bars for each of 6 categories](A%20deep%20dive%20into%20bar%20charts%20047791ead2e848bdb3d0afcd1bf2bd4a/many-grouped-bars2x.png)

Source: Maarten Lambrechts, CC BY SA 4.0

In those cases, dot plots can provide a nice alternative.

![The same data as in the grouped bar chart above, but shown with 5 dots for each of the categories](A%20deep%20dive%20into%20bar%20charts%20047791ead2e848bdb3d0afcd1bf2bd4a/dot-plot2x.png)

Source: Maarten Lambrechts, CC BY SA 4.0