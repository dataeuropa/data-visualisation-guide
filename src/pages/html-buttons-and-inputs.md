The `<button>` element is used to specify actions that should be performed when the button is activated. Activating a button can be done by clicking it with a mouse pointer, but also by touching it on touch devices, hitting enter or the space bar when it is focussed with the keyboard, with a voice command or with other assistive technology. The actions to be performed are usually written in JavaScript.

```html
<!DOCTYPE html>
<html>
  <head>
    <title>Webdev 101</title>
  </head>
  <body>
    <h1>Buttons</h1>
		<p>This is a html button.</p>
        <button>Nothing will happen if you click me</button>
  </body>
</html>
```

<aside>
🔗 <a href='https://officepublicationseu.github.io/accessible-html-dataviz/buttons.html'>See this <code>buttons.html</code> page live</a>, or <a href='https://github.com/officepublicationseu/accessible-html-dataviz/blob/master/buttons.html'>inspect the source HTML</a>.

</aside>

A `<button>` can be associated with a `<form>` element, to submit the information entered in the `<input>` elements of the form. `<input>` elements have a `type` attribute to specify what type of input they are. The type of an `<input>` can be “radio” for radio buttons, “checkbox” for checkboxes, “text” for a text input, “number” for numerical inputs and “date” for date pickers, amongst other types.

<aside>
🔗 <a href='https://officepublicationseu.github.io/accessible-html-dataviz/inputs.html'>See the <code>inputs.html</code> page for some examples of HTML inputs</a>, or <a href='https://github.com/officepublicationseu/accessible-html-dataviz/blob/master/inputs.html'>inspect its source HTML here</a>.

</aside>