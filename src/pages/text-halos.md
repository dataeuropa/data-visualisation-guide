Text elements like data labels and annotations might be placed on top of parts of a visualisation that carry some colour.  This also happens a lot with labels on maps. To assure that text is always readable on any background, mapping tools usually have an option to apply a halo to text. A halo is an outline of text that has a different colour than the text itself. By making the text dark and the halo light (or vice versa), you can ensure that text elements always have enough contrast with their background.

![A map showing the flow of refugees from Ukraine to other European countries with arrows](Data%20visualisation%20design%20in%20practice%201%20design%20tri%201d0d3c62419c4546846d9a92f783836c/bbc-map-refugees.jpg)

In this map, most country names have a white halo applied to them. This makes them stand out and makes sure they are easily legible, even when they run over country boundaries. Source: [Ukrainians on way to UK hit paperwork dead-end in Calais](https://www.bbc.com/news/uk-60652914), bbc.com

If the text elements of your chart are positioned on coloured elements in the background, you can consider to add a halo to it to guarantee legibility. The halo can be subtle...

![A visualisation showing the diversity of federal judges ba different US presidents. The visual uses subtle halos to make the numbers on the chart stand out a little from their background](Data%20visualisation%20design%20in%20practice%201%20design%20tri%201d0d3c62419c4546846d9a92f783836c/halo-wapo.png)

Source: [Biden, who pledged to diversify the Supreme Court, has already made progress on lower courts](https://www.washingtonpost.com/politics/2022/01/27/federal-judge-diversity-biden/), Washington Post

...or very strong.

![A visualisation titled 'The Americans who definitely do not want the vaccine' using a white, thick halo around the its data labels](Data%20visualisation%20design%20in%20practice%201%20design%20tri%201d0d3c62419c4546846d9a92f783836c/outline-538.png)

US based data journalism medium FiveThirtyEight has a very striking data visualisation style, which includes heavy text outlines. This makes annotations stand out and legible on all backgrounds. Source: [Unvaccinated America, In 5 Charts](https://fivethirtyeight.com/features/unvaccinated-america-in-5-charts/), fivethirtyeight.com