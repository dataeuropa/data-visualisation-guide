For visualisations using a cartesian coordinate system, with an x and y axis enclosing the plot area, a common pattern is to apply some margins around the plot area to place text elements, like the title, the axis titles and the axis labels.

![A line chart in which the plot area and the right, bottom and left margins are annotated](Data%20visualisation%20design%20in%20practice%201%20design%20tri%201d0d3c62419c4546846d9a92f783836c/anatomy-margins2x.png)

Source: Maarten Lambrechts, CC BY SA 4.0

Many visualisation tools apply this pattern, because it ensures that these text elements are not overlapping data marks and other elements in the plotting area. But sometimes the shape of the data is such that parts of the plot area do not contain. In those cases, you can deviate from the margins pattern, and move text elements into the chart. Gridlines are not useful for the parts of the plot area where there is no data visible. So they can be cropped without any issue.

![A line chart of which the title 'Why Peyton Manning's Record Will Be Hard To Beat' is moved inside the plot area](Data%20visualisation%20design%20in%20practice%201%20design%20tri%201d0d3c62419c4546846d9a92f783836c/cropped-gridlines.png)

Source: [Why Peyton Manning's Record Will Be Hard to Beat](https://www.nytimes.com/interactive/2014/10/19/upshot/peyton-manning-breaks-touchdown-passing-record.html?_r=0), nytimes.com