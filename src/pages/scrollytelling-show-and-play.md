In the Show-and-Play technique a video or animation is started when the user scrolls it into view.

![A four panel illustration of how show and play scrollytelling work, with the text 'Scrolling the video... plays it. And pauses it, when the reader scrolls away](Scrollytelling%200ae4533947224ed3b08305e4c650ce0d/scrollytelling-show-play.png)

Source: Oesch, Jonas, Manuel Roth, and Adina Renner. “Scrolling into the Newsroom: A Vocabulary for Scrollytelling Techniques in Visual Online Articles.” Information Design Journal. Forthcoming 2022

“Preserving biodiversity by protecting forests”, by the World Bank, uses show-and-play to start a video showing the deforestation in Mato Grosso, Brazil. After the video is played once, you can replay it.

![Screenshot of satellite images of an area in Brazil showing deforestation. In the top left is a Replay button](scrollytelling-deforestation-worldbank.jpg)

Source: [SDG Atlas 2020](https://datatopics.worldbank.org/sdgatlas/goal-15-life-on-land/)

Hawaii’s Beaches Are Disappearing, by ProPublica, uses graphic sequences and pan and zoom (both with maps as with panoramic photographs), but also uses show-and-play:  at certain moments in the story, some beach side real estate is highlighted by showing a video, with the waves hitting the protective walls in front of them.

![A screenshot showing a map, a snippet of text and a house close to the sea from Hawaii's Beaches Are Disappearing](scrollytelling-hawai-propublica.jpg)

[Hawaii's Beaches Are Disappearing](https://projects.propublica.org/hawaii-beach-loss/)