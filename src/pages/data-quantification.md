Knowing how data is classified and included in a dataset is crucial. The [Global Terrorism Database](https://www.start.umd.edu/gtd/) tracks terror attacks worldwide since 1970. When Russia helped separatists occupying the Luhansk and Donetsk Republics in the east of Ukraine in 2014, there was no official declaration of war and Russia denied its involvement. As a result, the fightings in the area were not considered to be acts of war by the Global Terrorism Database, and instead they were classified as terrorism. 

![A line chart with a single line, with the variable 'Attacks' on the y axis and years on the x axis](Ethics%20in%20data%20visualisation%201a9252053a714191a1f8cc31071467fa/terrorism-ukraine.png)

A spike in the number of terrorist attacks in Ukraine as recorded by the Global Terrorism Database. Source: [start.umc.edu/gtd](https://www.start.umd.edu/gtd/search/Results.aspx?chart=overtime&casualties_type=&casualties_max=&country=214)

Similarly, databases tracking ongoing wars might have excluded the fighting in eastern Ukraine in 2014 and 2015, which may have led to the underestimation of the war threat posed by Russia.

In this context, Goodhart’s law is also relevant. This law is named after British economist Charles Goodhart and states the following:

> When a measure becomes a target, it ceases to be a good measure.
> 

When a police department gets rewarded for the reduction the number of murders in their area, police officers can set up measures to prevent murders, or crack down hard on murderers. But the rewarding scheme also creates incentives for police officers to register a death under suspicious conditions as a natural death or as suicide rather than as a murder. Homicide rates might go down because of police officers stretching the definition of what constitutes a murder, or even hiding crime scene details to classify a death as not being a murder.