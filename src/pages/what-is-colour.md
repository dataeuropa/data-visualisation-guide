Colour might be the most discussed topic in data visualisation. This is due to colours being at the crossroads of aesthetics, cognitive science, culture and accessibility of data visualisation. Another reason for colour being a hot topic is that it is a hard topic to give advice about: coming up with “the best” color palette is simply not possible: too many elements play a role in the encoding and perception of colours.

What is possible, though, is trying to get a good understanding of what colours are, of what factors play a role in the perception of colours, and of what you should take into account when choosing colour palettes for data visualisation. What can also help is knowing about tools  that can help picking colours a little easier.

## What is colour?

Technically, colour is what our brains perceive when light of different wave lengths hits our retina, and the signal is sent through the optic nerve.

For a more practical and data visualisation focused approach to colour, you could say that colours are the proportions of different tints of ink that we print on paper, or the proportions of different kind of light that we let screens emit to represent data marks on a visualisation. The way to **describe** the proportions of “pure” colours needed to produce a certain color is what the <span class='internal-link'>[describing colours](describing-colours-introduction)</span> pages are about.

The second part of this module is about combining colours. Some color combinations feel much more harmonious than others, and you’ll learn why that is the case. Luckily many tools exist to help you pick these <span class='internal-link'>[colour harmonies](colour-harmonies)</span>.

Because colours are often used in data visualisation to discern between groups and categories, colour combinations also need to be “different enough” to be used effectively in data visualisation. And maybe most importantly: the colours used must be different enough from the background they are used on. This is the topic of the <span class='internal-link'>[colour contrast](colour-contrast)</span> page.

Finally you’ll learn that colours are **not neutral**: many colours are associated with objects, feelings and ideologies. These associations and <span class='internal-link'>[colour connotations](colour-connotations)</span> are not universal: they are culturally determined. Something important to keep in mind when designing visualisations for an international audience.