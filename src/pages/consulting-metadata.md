When you start looking at a new dataset your first reflex should be to look out for the metadata. Metadata, “data about the data”, explains who collected the data, when and how it was collected, and for what purpose it was collected

Good metadata will also explain what each column in the data means exactly and what the units of the numbers in the data are. This information is crucial for interpreting the data correctly and to make meaningful visualisations with it.

So never assume what the meaning of a column in the data is, or what the units of the numbers in a column are: make sure to consult the metadata, and ask your data provider to provide the metadata when it is missing.