Grid lines can be used to allow the reader read the values represented by the bars from the numerical axis.

<p class='center'>
<img src='A%20deep%20dive%20into%20bar%20charts%20047791ead2e848bdb3d0afcd1bf2bd4a/bars-axis-line.gif' alt='An horizontal bar chart with 8 bars and vertical gridlines for the values 20, 40, 60 and 80' class='max-400' />
</p>

Source: Maarten Lambrechts, CC BY 4.0

A better option is to put the data values as labels at the end of the bars. In that way the readers have direct access to the actual data, without having to read them from the grid lines. When data labels are present, the grid lines can be left out, because the data values can be read directly from the chart.

<p class='center'>
<img src='A%20deep%20dive%20into%20bar%20charts%20047791ead2e848bdb3d0afcd1bf2bd4a/bars-data-labels2x-100.jpg' alt='The same bar chart as above, but without grid lines and with data labels' class='max-400' />
</p>

Source: Maarten Lambrechts, CC BY 4.0

An “invisible grid” can be placed on top of the bars, which might make comparisons easier.

![An horizontal bar chart titled Inverclyde has shown the greatest improvement in affordability since 2007, applying the technique of the invisible grid lines](A%20deep%20dive%20into%20bar%20charts%20047791ead2e848bdb3d0afcd1bf2bd4a/invisible-gridlines.png)

Source: [The Guardian](https://www.theguardian.com/business/2017/sep/13/house-prices-uk-housing-affordability-london-birmingham-glasgow-leeds)