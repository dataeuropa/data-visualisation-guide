Simplifying a visualisation can also be done by leaving out data points through filtering or sampling. In the example below, the 2010 forecast for world GDP growth is left out of the visualisation on smaller screens.

![A lien chart showing forecasts for world GDP, with a title Forecasts for the world's gross domestic product, change from previous years. The chart has two annotations for the forecasts in 2010 and 2016](Responsiveness%20and%20data%20visualisation%20for%20small%20sc%20bfcc7f2b3f63483d9213104e4137aec4/wsj-bonds-desktop.png)

Source: [wsj.com](https://www.wsj.com/graphics/how-bond-yields-got-this-low/)

<p class='center'>
<img src='Responsiveness%20and%20data%20visualisation%20for%20small%20sc%20bfcc7f2b3f63483d9213104e4137aec4/wsj-bonds-mobile.png' alt='The same chart as above, but designed for small screens. Some of the forecasts are removed from the chart, and there is only one annotation' class='max-400' />
</p>

Source: [wsj.com](https://www.wsj.com/graphics/how-bond-yields-got-this-low/)

The map below shows the 20 biggest oil spills in 2018, with the size of the dots scaled to the size of the spill, and the most relevant ones labelled.

![A world map with bubbles showing the biggest oil spills in the world in 2018, with 4 of the spills labelled](Responsiveness%20and%20data%20visualisation%20for%20small%20sc%20bfcc7f2b3f63483d9213104e4137aec4/oil-spills-full.png)

Source: [Techniques for Flexible Responsive Visualization Design](https://dl.acm.org/doi/abs/10.1145/3313831.3376777), Hoffswell et al

For a thumbnail version of this map, some smaller oil spills can be filtered out. In this way, the map can be zoomed in, and exclude parts of the world that are less relevant for the story the map is trying to convey. Notice that the number of labels is also reduced, and the bubble size legend is removed.

<p class='center'>
<img src='Responsiveness%20and%20data%20visualisation%20for%20small%20sc%20bfcc7f2b3f63483d9213104e4137aec4/oil-spills-thumbnail.png' alt='A thumbnail version of the map above, zoomed in on northern Africa, Europe and Asia. Only one oil spill is labelled' class='max-400' />
</p>

Source: [Techniques for Flexible Responsive Visualization Design](https://dl.acm.org/doi/abs/10.1145/3313831.3376777), Hoffswell et al