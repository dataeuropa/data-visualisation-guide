**Coming full circle** is another storytelling technique that you can use in data driven stories. For example, 
[Why Budapest, Warsaw, and Lithuania split themselves in two](https://pudding.cool/2019/04/eu-regions/) shows a map of the regions of Hungary in the beginning of the article, which are shown again at the end. 

![2 maps of Hungary showing how the region around Budapest was split in two](Journalistic%20techniques%20for%20data%20storytelling%208bdd09bf88074238b1fe53b3a2116e1e/hungary-split-before.png)

Source: [Why Budapest, Warsaw, and Lithuania split themselves in two](https://pudding.cool/2019/04/eu-regions/), pudding.cool

![The same maps, but now with the regions coloured by their GDP per capita](Journalistic%20techniques%20for%20data%20storytelling%208bdd09bf88074238b1fe53b3a2116e1e/hungary-split-after.png)

Source: [Why Budapest, Warsaw, and Lithuania split themselves in two](https://pudding.cool/2019/04/eu-regions/), pudding.cool

The text and visualisations in between the maps explain the why of what is shown on them. But I am not revealing what that is here, because I want to create **a cliffhanger** to trigger your curiosity and make you click through to the original article 😉