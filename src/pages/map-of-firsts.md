[history.infowetrust.com](https://history.infowetrust.com/)

The Map of Firsts is a collection of visualisations, organised on a timeline based on when the visualisation was invented and published for the first time. The design of the timeline is based on the styling of historic visualisations and publications itself.

![A browser window showing the Map of Firsts homepage](Data%20visualisation%20galleries%2054f97b3d69b04dbe86cbf50ba86ab8c5/map-of-first.png)

Source: Maarten Lambrechts, CC BY SA 4.0

For every visualisation on the time line, its author and a link to the source are provided.

![A browser window showing a map with pie charts, an entry in the Map of Firsts](Data%20visualisation%20galleries%2054f97b3d69b04dbe86cbf50ba86ab8c5/map-of-firsts-minard.png)

Source: Maarten Lambrechts, CC BY SA 4.0