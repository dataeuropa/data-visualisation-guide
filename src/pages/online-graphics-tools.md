In the video below, you’ll see various tools for making visualisation and extracting graphics from webpages. The tools are:

- [RAWGraphs](https://rawgraphs.io/)
- [Datawrapper](https://www.datawrapper.de/)
- [Flourish](https://flourish.studio/)
- [SVG Crowbar](https://nytimes.github.io/svg-crowbar/)
- [Chrome Developer Tools Capture node screenshot](https://umaar.com/dev-tips/156-element-screenshot/)

<iframe width="100%" height="450" src="https://www.youtube.com/embed/BPHRymxF960" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>