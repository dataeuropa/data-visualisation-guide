Arranging elements on a page is what many people think about when they hear the term “graphic design”.  And it is true: arrangement and layout of elements are one of the most basic elements of graphic design.

Almost all graphic designers use a very simple tool to guide how they arrange elements on a page: the grid. By dividing a page into a simple grid of rows and colums, and by aligning the elements of the design to the borders of the resulting cells, a designer creates a document that is easy to read, and in which the reader is guided in a natural flow along the page. No wonder that all design and publishing tools, even basic ones, have an option to turn on the grid and let elements “snap” to the grid lines.

![ ](Grid%20and%20arrangement%204ba513397bf6413d900e7c2944888770/keynote-grid.png)

Rulers forming a grid in a Keynote presentation. Source: Maarten Lambrechts, CCBY 4.0

Although a grid with straight lines and right angles may sound not very exciting or even boring, grids offer a wide range of techniques to create engagement, tension and surprise.

Because visualisations are in many cases graphical elements on a page, and because a visualisation itself is a composition of graphical elements, it is worth spending a little time looking at grids and how they can help you in designing data visualisations.

## Page layout

The use of the grid started off with the layout of printed pages. Hand written books already were using basic alignment and layout, but with the invention of the printing press alignments became much more precise. Printers adopted it quickly, as you can see on the pages of the Gutenberg bible below.

![ ](Grid%20and%20arrangement%204ba513397bf6413d900e7c2944888770/Gutenberg_Bible_Lenox_Copy_New_York_Public_Library_2009._Pic_01.jpg)

A Gutenberg Bible on display, Lenox Copy, New York Public Library, 2009. [Picture by A GutNYC Wanderer (Kevin Eng)](https://commons.wikimedia.org/wiki/File:Gutenberg_Bible,_Lenox_Copy,_New_York_Public_Library,_2009._Pic_01.jpg), CC BY-SA 2.0

The Gutenberg bible uses a two column grid, with consistent margins and spacing between the columns (these are called the **gutters**).

![ ](Grid%20and%20arrangement%204ba513397bf6413d900e7c2944888770/1280px-Gutenberg_Bible_Proportions.svg.png)

The grid used in the design of the Gutenberg bible. [Image by Nik1986](https://upload.wikimedia.org/wikipedia/commons/archive/d/db/20221231082426%21Gutenberg_Bible_Proportions.svg), CC BY-SA 3.0. The outer and bottom margins are twice the size of the upper and inner margins, putting the text off center and making the pages a little bit dynamic, but well balanced.

In our todays digital world, it is hard to find any publication that is not using a grid in its design. Whole books have been written about the grid.

![Cover of a book titled 'Page Design' showing a grid in the background](Grid%20and%20arrangement%204ba513397bf6413d900e7c2944888770/page-design-309647601.jpg)

Source: [promopress.es](http://www.promopress.es/en/diseno-grafico-101580304/page-design-1120802-000-0001.html)

<p class="center">
<img src="Grid%20and%20arrangement%204ba513397bf6413d900e7c2944888770/grid-fade.gif" class="max-600" alt=""/>
</p>

A poster showing how the grid is used for its layout. Source: [GearedBull](https://commons.wikimedia.org/wiki/File:Grid2aib.svg), public domain

The grid has extended its use from the world of print into the one of digital design, and is also of very high importance in web and interface design.

![Animation of how a grid reacts to a shrinking and growing browser window](Grid%20and%20arrangement%204ba513397bf6413d900e7c2944888770/responsive-grid.gif)

Responsive grids are a common design pattern in modern web design. Source: [codepen.io/travishorn/full/RdPLwj](https://codepen.io/travishorn/full/RdPLwj)

## Invisible

The grid in a design is usually invisible, otherwise you get a “boxy” design.

<p class="center">
<img src="Grid%20and%20arrangement%204ba513397bf6413d900e7c2944888770/vermont-without-grid2x.png" class="max-600" alt="A poster anouncing the calendar of the Vermont Symphony Orchestra, with an invisible grid"/>
</p>

<p class="center">
<img src="Grid%20and%20arrangement%204ba513397bf6413d900e7c2944888770/Grid2aib.svg.png" class="max-600" alt="The same poster as above, but with the grid made visible"/>
</p>

Source: [GearedBull](https://commons.wikimedia.org/wiki/File:Grid2aib.svg), public domain 

This is the reason why you usually don’t want a frame around your visualisation when you embed it in a publication.

<p class="center">
<img src="Grid%20and%20arrangement%204ba513397bf6413d900e7c2944888770/chart-with-border2x.png" class="max-600" alt="A line chart enclosed in a black box"/>
</p>

Source: Maarten Lambrechts, CC-BY-SA 4.0

A visualisation without a border blends in better into your publication:

<p class="center">
<img src="Grid%20and%20arrangement%204ba513397bf6413d900e7c2944888770/chart-without-border2x.png" class="max-600" alt="The same line chart as above, but without the black enclosure"/>
</p>

Source: Maarten Lambrechts, CC-BY-SA 4.0

The same applies to the frame around the plotting area of a visualisation (see the “Anatomy of a chart” section further below).

## Using the grid

The most commonly used grids are the column grid and the modular grid.

The **column grid** consists of a number of columns separated by gutters and wrapped in some margins left and right.

![ ](Grid%20and%20arrangement%204ba513397bf6413d900e7c2944888770/material-design-grid.png)

An eight colun grid with margins in green and gutters in cyan from the Material Design design system. Source: [material.io](https://material.io/design/layout/responsive-layout-grid.html)

A **modular design** adds rows to a column grid, creating cells in the design. These cells are called **modules**. 

![ ](Grid%20and%20arrangement%204ba513397bf6413d900e7c2944888770/material-design-grid-2.png)

An 8 by 4 grid in the Material Design design system. This is an example of a modular use of the grid: some visual elements occupy only 1 modules, some 2 and some 4. Source: [material.io](https://material.io/design/layout/responsive-layout-grid.html)

12 is a popular number of columns in grid design. Because it is a multiple of 2, 3, 4 and 6, 12 columns allow for very flexible designs.

For the same reason of divisibility, the 960 pixel grid was popular in webdesign. Today, responsive web designs with fluid grid systems are more popular. Those make use of relative widths (percentages).

![ ](Grid%20and%20arrangement%204ba513397bf6413d900e7c2944888770/grid-layout-960.jpg)

A 940 pixel wide grid, which adds up to 960 pixels when a margin on the left and right side is taken into account. Source: [John Kuefler](https://johnkuefler.com/axure-template-download-960-grid/)

<aside>
🔎 As a little exercise, you can grab any publication (a book, a folder, a poster, a news paper, a web page, ...) and try to identify and describe the grid used for the layout:
- Is the publication using a column grid, a modular grid, or a less common type of grid?
- How many columns and rows can you identify?
- Can you find the margins and gutters?

</aside>