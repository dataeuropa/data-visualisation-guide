When the categories of a bar chart have no inherent ordering, the bars in a bar chart should be ordered based on the numerical values, so that the biggest bars are on one side of the chart and the smallest ones on the other. This increases the legibility of the bar chart, and creates a nice ranking and a more pleasing design.

<p class='center'>
<img src='A%20deep%20dive%20into%20bar%20charts%20047791ead2e848bdb3d0afcd1bf2bd4a/data-labels-inside2x%201.png' alt='A horizontal bar chart with 5 bars, which are not sorted' class='max-400' />
</p>

<p class='center'>
<img src='A%20deep%20dive%20into%20bar%20charts%20047791ead2e848bdb3d0afcd1bf2bd4a/sorted-bars2x.png' alt='The same bar chart as above, but with the bars sorted from highest to lowest value' class='max-400' />
</p>

Unsorted and sorted bars. Source: Maarten Lambrechts, CC BY SA 4.0

When the categories do have an inherent ordering (like age classes, or values like “very bad”, “bad”, “neutral”, “good”, “very good”), the bars should be ordered according to this inherent ordering.