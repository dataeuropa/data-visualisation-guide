The most popular data visualisation packages for the Python programming languages are [Matplotlib](https://matplotlib.org/), [Seaborn](https://seaborn.pydata.org/), [Bokeh](https://bokeh.org/) and [Plotly](https://plotly.com/python/) (see below). A non-chart-template library is [Altair](https://altair-viz.github.io/), which uses the same philosophy as ggplot2 in R to build up visualisations.

![ ](Data%20visualisation%20design%20in%20practice%202%20tools%20208f06b06b0f4b21ad8ecf3047f02ce0/altair.png)

Gallery of charts made with Altair in Python. Source: [altair-viz.github.io](https://altair-viz.github.io/)