Paying (free for personal use), JavaScript Library

[www.highcharts.com](https://www.highcharts.com/)

Highcharts is a JavaScript library for making SVG visualisations in the browser. It is chart template based and has extensive configuration options for interactivity, styling, annotations and accessibility.

![6 examples of line charts made with Highcharts](Data%20visualisation%20design%20in%20practice%202%20tools%20208f06b06b0f4b21ad8ecf3047f02ce0/highcharts.png)

Source: [highcharts.com/demo](https://www.highcharts.com/demo)

Highcharts also offers a library for maps, called Highchart Maps.