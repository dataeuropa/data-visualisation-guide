
<p class='center'>
<img src='Data%20story%20genres%20and%20structures%20854bd72307ad4dbda8a777a86347f3df/annotatedchart.png' alt='A small illustration showing a line chart with 3 annotiations' class='max-200' />
</p>

Source: [Narrative Visualization: Telling Stories with Data](https://cpb-us-e1.wpmucdn.com/sites.northwestern.edu/dist/3/3481/files/2015/02/Narrative_Visualization.pdf)

The narrative power of adding visual and textual annotations is described on the <span class='internal-link'>[visual annotations](tag/visual-annotations)</span> and <span class='internal-link'>[text annotations](tag/text-annotations)</span> pages. The researchers recognised this power by identifying the annotated chart as a separate genre.

Annotated charts will have only a limited amount of surrounding text and use a visualisation as the canvas for the annotations, which are the building blocks of the narrative.

![A Dutch newspaper page titled 'Zo verloopt de intelligente lockdown tot nu'. It shows how busy areas like shops, parks, residential areas and stations were for the period from 20 February up to 16 April with heatmaps. Text annotions explain the trends visible in the data](Data%20story%20genres%20and%20structures%20854bd72307ad4dbda8a777a86347f3df/annotations-sjoerdmouissie.jpg)

A page from the Nederlands Dagblad newspaper explaining the effects of lockdown measures on mobility in different parts of the Netherlands (shops, parks and beaches, residential areas, ...). The annotations on the chart tell the story, and the text below the graphic only explains how to read the chart and where the data comes from. Source: [@SjoerdMouissie](https://twitter.com/SjoerdMouissie/status/1253740351329898496) 

You can find more examples of the annotated chart genre on the <span class='internal-link'>[text annotation stories](text-annotation-stories)</span> page.