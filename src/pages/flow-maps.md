Flow maps, also known as origin-destination maps, are maps that show the direction and magnitude of flows between geographical areas or points. Many techniques exist to depict flows on a map.

French engineer Charles Joseph Minard was a pioneer in making flow maps in the nineteenth century. His [map of Napoleon’s Russian campaign](https://en.wikipedia.org/wiki/File:Minard.png) became an icon of the data visualisation world, but he made many other flow maps. The map below shows the kind and quantity of livestock that were transported to Paris from the surrounding regions in 1862. The width of each segment of the flows is proportional to the quantities.

![A vintage map titled 'Carte figurative et approximative des poids des bestiaux venus à Paris sur les chemins de fer en 1862. The map shows flows of increasing widths flowing to Paris, with yellow, red, grey and blue colours representing beef, veal, porc and mutton'](Maps%20e22d0627fc944d47be79a1d1a4f8acef/minard_paris.jpg)

Source: [École nationale des ponts et chaussées](https://heritage.ecoledesponts.fr/enpc/#bibnum)

The map below uses a technique akin to a <span class='internal-link'>[Sankey diagram](sankey-and-alluvial-diagrams)</span>: it groups the lines originating from the sources together, and splits it over the destination countries moving to the right of the map.

![A map of the northern part of Africa, Europe and Asia, with green arrows originating in Mali, Ivory Coast, Benin, Togo, Burkina Faso and Ghana and pointing to Bangladesh, China, Vietnam and some other countries. The map is titled 'Most West African cotton is exported to South and Southeast Asia'](Maps%20e22d0627fc944d47be79a1d1a4f8acef/cotton-flow-map.jpeg)

Source: [@atthar_mizra](https://twitter.com/atthar_mirza/status/1342646609012793347)

The flow map belows shows global migration patterns between countries and uses a technique called **edge bundling** to group connections between countries and continents together to decrease the clutter that complex networks can suffer from.

![A world map showing bundles of coloured line flowing between the continents. The maps is titled 'Global Migration (2005-2010)' and the colours represent the continent of origin](Maps%20e22d0627fc944d47be79a1d1a4f8acef/global-migration-edge-bundling.jpeg)

Source: [@schochastics](https://twitter.com/i/web/status/1455508236598132737)

More examples of flow maps can be found in [this slide deck](https://docs.google.com/presentation/d/1J2smDZOr664zUWrpScqSU8U2tFHeKD4qLhRaKXtPE2c/edit#slide=id.p4).