[A window into Delhi’s deadly pollution](https://graphics.reuters.com/INDIA-POLLUTION/01008173281/index.html), by Reuters, uses photographs to tell the story of the horrible air quality in Delhi.

![Screenshot of the opening of the article titled Delhi's deadly pollution, showing a composition of views on the city with increasing amount of smog from left to right](Combining%20visualisations%20with%20other%20media%203195c842e70245d394d6c9e019d7165e/delhi-air-quality.png)

Source: [A window into Delhi’s deadly pollution](https://graphics.reuters.com/INDIA-POLLUTION/01008173281/index.html), graphics.reuters.com

In [A plateful of plastic](https://graphics.reuters.com/ENVIRONMENT-PLASTIC/0100B4TF2MQ/index.html), Reuters also used photography to show the amount of microplastics we eat:

![A photo of a spoon full of plastic, the amount we consume of it every week](Combining%20visualisations%20with%20other%20media%203195c842e70245d394d6c9e019d7165e/plastic-reuters2.png)

Source: [A plateful of plastic](https://graphics.reuters.com/ENVIRONMENT-PLASTIC/0100B4TF2MQ/index.html), graphics.reuters.com

![A photo of a bowl full of plastic, the amount we consume of it every month](Combining%20visualisations%20with%20other%20media%203195c842e70245d394d6c9e019d7165e/plastic-reuters.png)

From [Use Only as Directed](https://www.propublica.org/article/tylenol-mcneil-fda-use-only-as-directed), by ProPublica:

![A photo of jars of pills showing the amount of pills of acetominophen you can buy in the USA compared to England, Germany and Mexico](Combining%20visualisations%20with%20other%20media%203195c842e70245d394d6c9e019d7165e/pills-propublica.png)

Source: [Use Only as Directed](https://www.propublica.org/article/tylenol-mcneil-fda-use-only-as-directed), propublica.org

Artist Dillon Marsh uses computer graphics to place metal spheres in abandoned mines to show the extracted amount of material.

![A rendering of a copper sphere behind a lake in a closed mine](Combining%20visualisations%20with%20other%20media%203195c842e70245d394d6c9e019d7165e/copper02.jpg)

Source: [Dillon Marsh](http://dillonmarsh.com/copper02.html)

And finally a classic example from a 1965 campaign by London Transport showing the difference in space needed for cars and a bus.

![3 photos showing 69 people in cars, on foot and in one bus. The text next to the photos says 'These vehicles are carrying... 69 people who could all... be on this one bus](Combining%20visualisations%20with%20other%20media%203195c842e70245d394d6c9e019d7165e/london-bus-cars.jpg)

Source: [Heinz Zinram at the London Transport Museum](https://www.ltmuseum.co.uk/collections/collections-online/posters/item/1983-4-7561)