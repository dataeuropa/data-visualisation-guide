A table containing numbers is closely related to a heatmap, in which each cell in the table is coloured according to the values it contains.

![A heatmap of monthly unemployment rates in 11 countries](Table%20design%20fc020c95cbd840efb61aa1fdc6f6cb09/heatmap.png)

Source: Datawrapper

This heatmap technique of colouring cells depending on their numerical value, can also be applied to individual columns in a table. This can help find the lower and higher values in columns with (unsorted) numbers or help in signalling the proportions between the numbers.

![A list of the most visited museums in the world, with the column with the number of visitors coloured according the the values](Table%20design%20fc020c95cbd840efb61aa1fdc6f6cb09/heatmap-single-column.png)

Another interesting technique for fusing visualisations and tables is to integrate bars into a column in which the bar length of the bar in a cell is proportional to the number it contains.

<iframe title="The most visited art museums in the world" aria-label="Table" id="datawrapper-chart-bboxO" src="https://datawrapper.dwcdn.net/bboxO/1/" scrolling="no" frameborder="0" style="border: none;" width="100%" height="605" data-external="1"></iframe>

And when time series data is available, a table column can integrate mini linecharts (called “sparklines”) indicating the trend in the data.

<iframe title="Life expectancy in all countries increased since 1960, but with a different pace" aria-label="Table" id="datawrapper-chart-NUn3X" src="https://datawrapper.dwcdn.net/NUn3X/1/" scrolling="no" frameborder="0" style="border: none;" width="100%" height="582" data-external="1"></iframe>