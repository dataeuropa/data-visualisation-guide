/*export let topicColors = {
    'Design principles': '#C73938',
    'Data storytelling': '#316DB0',
    'Pitfalls': '#661188',
    'Dataviz in practice': '#53793F',
    'Chart types': '#C14E7C',
    'Accessibility': '#707070',
    'Grammar of Graphics': '#7C4D13'
}*/

/*export let topicColors = {
    'Design principles': '#0D40B7',
    'Data storytelling': '#CCA300',
    'Pitfalls': '#BF0036',
    'Dataviz in practice': '#007957',
    'Chart types': '#CC4E00',
    'Accessibility': '#490CB9',
    'Grammar of Graphics': '#1B1E27'
}*/

export let topicColors = {
    'Design principles': '#4A4A4A',
    'Data storytelling': '#5CC1F8',
    'Pitfalls': '#2241AB',
    'Dataviz in practice': '#629869',
    'Chart types': '#654D8F',
    'Accessibility': '#E7363A',
    'Grammar of Graphics': '#EB9D39'
}